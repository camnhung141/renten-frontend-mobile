/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';

import React, {useEffect, useState} from 'react';
import {
    LogBox,
    StyleSheet, View,
} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';

import {NavigationContainer} from '@react-navigation/native';
import RootNavigationStack from './src/navigations/Root';
import {Provider, useDispatch} from 'react-redux';
import store from './src/redux/store';
import Toast from 'react-native-toast-message';
import MainSplashScreen from './src/components/splashScreen/mainSplashScreen';
import {wait} from './src/common/utils/utils';
import {getUserInfo} from './src/redux/features/customerSlice';

LogBox.ignoreAllLogs(true);

const App: () => React$Node = () => {
    const [isLoading, setIsloading] = useState(true);

    wait(1500).then(() => {
        setIsloading(false);
    });

    return (
        <SafeAreaProvider>
            <Provider store={store}>
                <NavigationContainer>
                    {
                        isLoading ? <MainSplashScreen/>
                            :
                            <RootNavigationStack/>
                    }
                    <Toast ref={(ref) => Toast.setRef(ref)}/>
                </NavigationContainer>
            </Provider>
        </SafeAreaProvider>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});

export default App;
