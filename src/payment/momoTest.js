import {Platform, DeviceEventEmitter, NativeModules, NativeEventEmitter, TouchableOpacity, Linking} from 'react-native';
import RNMomosdk from 'react-native-momosdk';
import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import CustomText from '../components/text/customText';
import {Colors, IconStyle} from '../common/styles/common';
import CustomButton from '../components/buttons/customButton';
import {doPaymentMomo, getPaymentMomo} from '../redux/features/checkoutSlice';
import Spinner from 'react-native-loading-spinner-overlay';

const MomoTest = ({style}) => {

    const RNMomosdkModule = NativeModules.RNMomosdk;
    const EventEmitter = new NativeEventEmitter(RNMomosdkModule);

    const enviroment = '0'; //"0": SANBOX , "1": PRODUCTION

    const [isGettingStatus, setGettingStatus] = useState();

    useEffect(() => {
        EventEmitter.addListener('RCTMoMoNoficationCenterRequestTokenReceived', (response) => {
            try {
                console.log('<MoMoPay>Listen.Event::' + JSON.stringify(response));
                if (response && response.status == 0) {
                    //SUCCESS: continue to submit momoToken,phonenumber to server
                    let fromapp = response.fromapp; //ALWAYS:: fromapp==momotransfer
                    let momoToken = response.data;
                    let phonenumber = response.phonenumber;
                    let message = response.message;
                    let orderId = response.refOrderId;
                } else {
                    //let message = response.message;
                    //Has Error: show message here
                }
            } catch (ex) {
            }
        });
        //OPTIONAL
        EventEmitter.addListener('RCTMoMoNoficationCenterRequestTokenState', (response) => {
            console.log('<MoMoPay>Listen.RequestTokenState:: ' + response.status);
            // status = 1: Parameters valid & ready to open MoMo app.
            // status = 2: canOpenURL failed for URL MoMo app
            // status = 3: Parameters invalid
        });
    });

    // TODO: Action to Request Payment MoMo App
    const onPress = async () => {
        let jsonData = {};
        getPaymentMomo().then(data => {
            jsonData.enviroment = enviroment; //SANBOX OR PRODUCTION
            jsonData.action = 'gettoken'; //DO NOT EDIT
            jsonData.merchantname = data.merchantname; //edit your merchantname here
            jsonData.merchantcode = data.merchantCode; //edit your merchantcode here
            jsonData.merchantnamelabel = data.merchantNameLabel;
            jsonData.description = data.description;
            jsonData.amount = data.amount;//order total amount
            jsonData.orderId = data.orderId;
            jsonData.orderLabel = data.orderLabel;
            jsonData.appScheme = 'momocgv20170101';// iOS App Only , match with Schemes Indentify from your  Info.plist > key URL types > URL Schemes

            if (Platform.OS === 'android') {
                RNMomosdk.requestPayment(jsonData).then(dataPayment => {
                    momoHandleResponse(dataPayment, data._id);
                });
            } else {
                // RNMomosdk.requestPayment(jsonData);
            }
        });
    };

    const momoHandleResponse = (response, momoMobileId) => {
        try {
            if (response && response.status == 0) {
                //SUCCESS continue to submit momoToken,phonenumber to server
                let fromapp = response.fromapp; //ALWAYS:: fromapp == momotransfer
                let momoToken = response.data;
                let phonenumber = response.phonenumber;
                let message = response.message;
                doPaymentMomo(momoMobileId, response.phonenumber, response.data).then(() => {
                        setGettingStatus(true);
                    },
                );
            } else {
                //let message = response.message;
                //Has Error: show message here
            }
        } catch (ex) {
        }
    };


    return (
        <View style={style}>
            <CustomButton title='Thanh toán (Momo)' backgroundColor={Colors.orange}
                          isNotRound={true}
                          onChooseOption={onPress}
            />
            <Spinner
                visible={isGettingStatus}
                textContent={'Đang xử lý thanh toán...'}
                textStyle={{color: Colors.white}}
                cancelable={true}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {},
});

export default MomoTest;
