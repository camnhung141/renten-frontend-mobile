import React from 'react';
import {View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, FontSize, IconStyle, PaymentStatus} from '../common/styles/common';
import CustomText from '../components/text/customText';
import CustomButton from '../components/buttons/customButton';
import {useNavigation} from '@react-navigation/core';
import {StackRoute} from '../navigations/stackroute/stackRoute';

const PaymentResult = (props) => {
    const navigation = useNavigation();
    const status = props.route.params.status
    const message = props.route.params.message

    return (
        <View style={styles.container}>
            {
                status === PaymentStatus.success ?
                    <View style={styles.container}>
                        <View>
                            <Icon name='check-circle-outline' color={Colors.green} style={{fontSize: 80}}/>
                        </View>
                        <View>
                            <CustomText>{message} !</CustomText>
                        </View>
                    </View>
                    :
                    <View style={styles.container}>
                        <View>
                            <Icon name='alert-circle-outline' color={Colors.red} style={{fontSize: 80}}/>
                        </View>
                        <View>
                            <CustomText>{message} !</CustomText>
                        </View>
                    </View>
            }
            <CustomButton title='Trở về trang chủ' backgroundColor={Colors.orange} style={{bottom: 0, position: 'absolute'}} onChooseOption={() => {navigation.navigate(StackRoute.Home)}}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
});

export default PaymentResult;
