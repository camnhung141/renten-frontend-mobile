import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Image, Linking} from 'react-native';
import {Colors, IconStyle, PaymentStatus} from '../common/styles/common';
import CustomButton from '../components/buttons/customButton';
import Spinner from 'react-native-loading-spinner-overlay';
import {sendReturnUrlMomo} from '../redux/features/checkoutSlice';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/core';
import {StackRoute} from '../navigations/stackroute/stackRoute';
import {checkObjectValid, getParams} from '../common/utils/utils';

const DeepLinkMomo = ({style}) => {

    const navigation = useNavigation();

    const onPress = async () => {
        sendReturnUrlMomo('rententmobile://payment').then((rs) => {
            Linking.openURL(JSON.parse(rs.data.payload).deeplink);
        });
    };

    useEffect(() => {
        Linking.getInitialURL().then((url) => {
            if (url) {
                handleOpenURL(url);
            }
        }).catch(err => {
        });
        Linking.addEventListener('url', handleOpenURL);
    }, []);

    const handleOpenURL = (url) => {
        const url_params = url.url
        navigation.navigate(StackRoute.PaymentResult, {
            status: getParams('message', url_params),
            message: getParams('localMessage', url_params)
        });
    };

    return (
        <View style={style}>
            <CustomButton title='Thanh toán (Momo)' backgroundColor={Colors.orange}
                          isNotRound={true}
                          onChooseOption={onPress}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {},
});

export default DeepLinkMomo;
