import axios from 'axios';
import {getLocalRefreshToken} from '../common/utils/utils';
import {useSelector} from 'react-redux';
import {getAccessToken, getRefreshToken, storeAuthInfo} from '../storage/async-storage';
import {setLoginStatus} from '../redux/features/authSlice';
import Toast from 'react-native-toast-message';
import {err} from 'react-native-svg/lib/typescript/xml';

const instance = axios.create({
    // baseURL: 'http://192.168.1.14:4000',
    baseURL: 'https://renten-backend.herokuapp.com',
    // baseURL: 'http://192.168.137.1:4000',
    timeout: 3000,
});

let accessToken = null;

getAccessToken().then(ac => {
    accessToken = ac
})

export const setAccessToken = (value) => {
    accessToken = value;
};

instance.interceptors.request.use(function (config) {
    config.headers.Authorization = `Bearer ${accessToken}`;
    return config;
});

const api = {
    get: (url, params) => instance.get(`${url}`, params),
    put: (url, params) => instance.put(`${url}`, params),
    post: (url, data) => instance.post(`${url}`, data),
    delete: (url, params) => instance.delete(`${url}`, params),
};

export const postFormDataImages = (form, type) => {
    if (type) {
        form.append('path', type)
    }
    return instance.post('/images', form, {
        timeout: 15000,
    });
};


const refreshToken = async () => {
    return getRefreshToken().then(ref => {
        return instance.post('/auths/refresh-token', {
            refreshToken: ref,
        });
    })
};

instance.interceptors.response.use((response) => {
    return response;
}, (error) => {
    const statusCode = error.response.status;

    if (error.response.data.status !== 'jwt malformed' && error.response.data.status !== 'jwt must be provided') {
        Toast.show({
            type: 'error',
            text1: 'Đã có lỗi xảy ra',
            text2: error.response.data.status
        });
    }

    const originalRequest = error.config;
    if (statusCode === 401) {
        return refreshToken().then(rs => {
            setAccessToken(rs.data.payload.accessToken);
            storeAuthInfo(rs.data.payload);
            return instance.request(originalRequest);
        }).catch(error => {
                return Promise.reject(error);
            },
        );
    }
    return new Promise.reject(error);
});

export default api;
