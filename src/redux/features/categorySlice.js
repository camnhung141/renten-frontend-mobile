import {createSlice} from '@reduxjs/toolkit';
import api from '../../api/api';

export const CategorySlice = createSlice({
    name: 'category',
    initialState: {
        categories: []
    },
    reducers: {
        setCategories: (state, action) => {
            state.categories = action.payload;
        }
    },
});

export const getCategoriesService = () =>  dispatch =>  {
    api.get('/categories').then((response) => {
        dispatch(setCategories(response.data.payload));
    });
};



export const {setCategories} = CategorySlice.actions;

export default CategorySlice.reducer;

