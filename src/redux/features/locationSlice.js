import {createSlice} from '@reduxjs/toolkit';
import api from '../../api/api';

export const LocationSlice = createSlice({
    name: 'location',
    initialState: {
        cities: [],
        districts: [],
        wards: [],
        addressAdd: {},
        addressList: []
    },
    reducers: {
        loadCities: (state, action) => {
            state.cities = action.payload;
        },
        loadDistricts: (state, action) => {
            state.districts = action.payload;
        },
        loadWards: (state, action) => {
            state.wards = action.payload;
        },
        refreshAddress: (state, action) => {
            state.addressList = action.payload
        }
    },
});

export const loadCitiesService = () => dispatch => {
    api.get('/locations/cities').then((r) => {
        dispatch(loadCities(r.data.payload));
    });
};

export const loadDistrictsService = (cityCode) => dispatch => {
    if (cityCode) {
        api.get('/locations/cities/' + cityCode + '/districts').then((r) => {
            dispatch(loadDistricts(r.data.payload));
        });
    } else {
        dispatch(loadDistricts([]));
    }
};

export const loadWardService = (districtCode) => dispatch => {
    if (districtCode) {
        api.get('/locations/districts/' + districtCode + '/wards').then((r) => {
            dispatch(loadWards(r.data.payload));
        });
    } else {
        dispatch(loadWards([]));
    }
};

export const updateAddressInfo = (address) => dispatch =>  {
    api.post('/customers/delivery-addresses', address).then(() =>
        dispatch(getAddressInfoList())
    )
};

export const editAddressInfo = (address, _id) => dispatch => {
    api.put('/customers/delivery-addresses/' + _id, address).then(() =>
        dispatch(getAddressInfoList())
    )
};

export const deleteAddressInfo = (_id, accountId) => dispatch => {
    api.delete( accountId + '/customers/delivery-addresses/' + _id).then(() =>
        dispatch(getAddressInfoList())
    )
};

export const getAddressInfoList = () => dispatch => {
    return api.get('/customers/delivery-addresses').then((response) => {
        dispatch(refreshAddress(response.data.payload))
        // return response.data.payload;
    });
};

export const {loadCities, loadDistricts, loadWards, refreshAddress} = LocationSlice.actions;

export default LocationSlice.reducer;
