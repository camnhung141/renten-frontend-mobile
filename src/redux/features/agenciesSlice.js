import {createSlice} from '@reduxjs/toolkit';
import api from '../../api/api';

export const AgenciesSlice = createSlice({
    name: 'agencies',
    initialState: {
    },
    reducers: {
    },
});

export const getAgency = (id) => {
    return api.get('/agencies/' + id).then(result => {
        return result.data.payload;
    }).catch(() => {
        return null;
    })
}

export const getProductsByAgencyId = (id) => {
    return api.get('/product-templates?agencies=' + id).then(result => {
        return result.data.payload;
    }).catch(() => {
        return null;
    })
}

export const {} = AgenciesSlice.actions;

export default AgenciesSlice.reducer;
