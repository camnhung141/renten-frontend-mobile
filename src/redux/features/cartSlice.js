import {createSlice} from '@reduxjs/toolkit';
import api from '../../api/api';
import {dateToStringFormat, getHeaderAccessTokenConfig} from '../../common/utils/utils';

export const CartSlice = createSlice({
    name: 'cart',
    initialState: {
        carts: [],
        isUpdatedCart: 1,
    },
    reducers: {
        setIsUpdatedCart: (state) => {
            state.isUpdatedCart ^= state.isUpdatedCart;
        },
        getCarts: (state, action) => {
            state.carts = action.payload;
        },
        deleteCart: (state, action) => {
            state.isUpdatedCart ^= state.isUpdatedCart;
            state.carts = state.carts.filter(item => {
                return  item.productId !== action.payload
            })

        }
    },
});

export const getCartsService = () => dispatch => {
    // const headerAccessToken = await getHeaderAccessToken();
    // dispatch(setLoadingCarts())
    api.get('/carts').then((r) => {
        dispatch(getCarts(r.data.payload))
    })
};

export const addCartService = (cartDetail) => {
    return api.post('/carts', cartDetail.request)
};

export const updateCartItem = (cartInfo) => dispatch => {
    api.put('/carts', cartInfo).then(r => {
        dispatch(setIsUpdatedCart());
        if (cartInfo.quantity < 1) {
            dispatch(deleteCart(cartInfo.productId));
        }
    });
};

export const deleteCarts = () => {
    api.delete('/carts')
};

// export const addCartService = (cartDetail) => async dispatch => {
//     const headerAccessToken = await getHeaderAccessToken();
//     api.post('/carts', cartDetail.request, headerAccessToken).then((response) => {
//         if (response.data.message === 'OK') {
//             dispatch(addCart({...cartDetail.request, ... {product: cartDetail.item}}));
//         }
//     });
// };

export const {setIsUpdatedCart, getCarts, deleteCart} = CartSlice.actions;

export default CartSlice.reducer;

