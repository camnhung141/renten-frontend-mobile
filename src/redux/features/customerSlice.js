import {createSlice} from '@reduxjs/toolkit';
import api from '../../api/api';
import Toast from 'react-native-toast-message';

export const CustomerSlice = createSlice({
    name: 'customer',
    initialState: {
        userInfo: {},
        justChangeInfo: 0,
        updateInfo: {},
        point: {},
        accountStatus: {}
    },
    reducers: {
        setUserInfo: (state, action) => {
            const data = action.payload;
            delete data.__v
            const id = data.accountId._id
            state.accountStatus = data.accountId
            delete data.accountId
            state.point = data.point
            delete data.point
            const avatar = data.avatar

            state.userInfo = {...state.userInfo, ...data, accountId: id, avatar: avatar};
            state.justChangeInfo ^= 1;
            state.updateInfo = {};
        },
        addUpdateUserProperty: (state, action) => {
            state.updateInfo = ({... state.updateInfo, ...action.payload});
        },
        updateUserInfo: (state, action) => {
            state.userInfo = {...state.userInfo, ...state.updateInfo};
            state.justChangeInfo ^= 1;
            state.updateInfo = {};
        }
    },
});

export const getUserInfo = () => dispatch => {
    api.get('/customers/info').then(
        (responseInfoCus) => {
            // if (responseInfoCus.data.message === 'OK') {
                dispatch(setUserInfo(responseInfoCus.data.payload));
            // }
        }
    )
}

export const sendUpdateUserInfo = (dataUpdate) => dispatch => {
    api.put('/customers/info', dataUpdate).then(() => {
        Toast.show({
            type: 'success',
            text1: 'Cập nhật thông tin cá nhân thành công!',
        })
        dispatch(updateUserInfo());
    })
}

export const updatePropertyUserInfo = (userProperty) => dispatch => {
    dispatch(addUpdateUserProperty(userProperty));
}

export const {setUserInfo, addUpdateUserProperty, updateUserInfo} = CustomerSlice.actions;

export default CustomerSlice.reducer;
