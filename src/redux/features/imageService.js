import {postFormDataImages} from '../../api/api';
import {checkObjectValid} from '../../common/utils/utils';
import {reviewProduct} from './productSlice';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {TypeImage} from '../../common/styles/common';

export const getImageLink = (uri, name, type) => {
    const data = {
        uri: uri,
        type: type,
        name: name
    }
    const form = new FormData();
    form.append('images', data);
    form.append('path', TypeImage.IDENTITY_CARD)
    return postFormDataImages(form).then((response) => {
        if (response.data.payload) {
           return response.data.payload[0];
        }
        return null;
    }).catch((err) => {
        return null
    });
}
