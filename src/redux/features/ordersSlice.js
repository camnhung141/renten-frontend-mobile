import {createSlice} from '@reduxjs/toolkit';
import api from '../../api/api';
import {StatusOrder} from '../../common/styles/common';
import Toast from 'react-native-toast-message';

export const OrdersSlice = createSlice({
    name: 'order',
    initialState: {
        justUpdateOrder: 1,
    },
    reducers: {
        setJustUpdateOrders: (state) => {
            state.justUpdateOrder ^= 1
        }
    },
});

export const getOrders = () => {
    return api.get('/orders').then((r) => {
        return r.data.payload;
    }).catch(() => {
        return null;
    });
};

export const getOrderDetail = (id) => {
    return api.get('/orders/' + id).then((r) => {
        return r.data.payload;
    }).catch(() => {
        return null;
    });
}

export const updateOrder = (idOrder, status) => dispatch => {
    return api.put('/orders/' + idOrder, {status: status}).then(() => {
            if (StatusOrder.cancel === status) {
                Toast.show({
                    type: 'success',
                    text1: 'Hủy đơn hàng thành công',
                });
            }
            dispatch(setJustUpdateOrders())
            return true;
        },
    ).catch(() => false);
};

export const {setJustUpdateOrders} = OrdersSlice.actions;

export default OrdersSlice.reducer;
