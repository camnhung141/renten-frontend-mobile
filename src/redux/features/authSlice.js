import {createSlice} from '@reduxjs/toolkit';
import api, {setAccessToken} from '../../api/api';
import {
    removeAuthInfo,
    storeAuthInfo
} from '../../storage/async-storage';
import {getUserInfo} from './customerSlice';
import {getLocalRefreshToken} from '../../common/utils/utils';

export const AuthSlice = createSlice({
    name: 'auth',
    initialState: {
        isLogged: false,
        signUpSuccessFlag: 1,
    },
    reducers: {
        setLoginStatus: (state, action) => {
            state.isLogged = action.payload;
        },
        setJustSuccessSignUp: (state) => {
            state.signUpSuccessFlag ^= 1;
        },
    },
});

export const loginService = loginInfo => dispatch => {
    api.post('/auths/signin', loginInfo).then((response) => {
        if (response.data.message === 'OK') {
            setAccessToken(response.data.payload.accessToken);
            storeAuthInfo(response.data.payload)
            dispatch(setLoginStatus(true));
            dispatch(getUserInfo());
        }
    });
};

export const signUpService = signUpInfo => dispatch => {
    api.post('/auths/customers/signup', signUpInfo).then((response) => {
        if (response.data.message === 'OK') {
            dispatch(setJustSuccessSignUp());
        }
    });
};

export const setStatusLoginService = (value) => dispatch => {
    dispatch(setLoginStatus(value));
};

export const logOutService = () => dispatch => {
    getLocalRefreshToken().then(refreshTok => {
        api.post('/auths/signout', {
            refreshToken: refreshTok,
        });
        removeAuthInfo().then(dispatch(setLoginStatus(false)));
    });
};

export const updatePassword = (oldPassword, newPassword, confirmPassword) => {
    return api.put('/auths/update-password', {oldPassword, newPassword, confirmPassword})
};

export const verify = (data) => {
    return api.put('auths/verify', data)
}

export const {setLoginStatus, setJustSuccessSignUp} = AuthSlice.actions;

export default AuthSlice.reducer;
