import {createSlice} from '@reduxjs/toolkit';
import api from '../../api/api';
import {getCartsService, setIsUpdatedCart} from './cartSlice';

export const CheckoutSlice = createSlice({
    name: 'checkout',
    initialState: {
        paymentUpdated: '',
        isJustPay: 1,
        payments: {},
    },
    reducers: {
        paymentUpdated: (state, action) => {
            state.paymentUpdated = action.payload
        },
        justPayUpdate: (state, action) => {
            state.isJustPay ^= 1
        },
    },
});

export const getPayments = () => {
    return api.get('/checkout/payment').then((response) => {
        return response.data.payload
    }).catch(() => {
        return null
    })
}

export const doPaymentMomo = (momoMobileId, customerNumber, token) => {
    return api.post('/checkout/mobile-payment', {momoMobileId, customerNumber, token}).then(() => {
        return true
    })
}

export const updatePaymentMethod = (method) => dispatch => {
    return api.put('/checkout/payment-methods', {type: method}).then(() => {
        dispatch(paymentUpdated(method))
    })
}

export const updateMethodPaymentState = (method) => dispatch => {
    dispatch(paymentUpdated(method))
}

export const doPayment = () => {
    return api.post('/checkout/payment')
}

export const sendReturnUrlMomo = (returnUrl) => {
    return api.post('/checkout/payment', {returnUrl})
}

export const getPaymentMomo = () => {
    return api.get('/checkout/mobile-payment').then((response) => {
        return response.data.payload;
    })
}

export const {paymentUpdated, justPayUpdate} = CheckoutSlice.actions;

export default CheckoutSlice.reducer;
