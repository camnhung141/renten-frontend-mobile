import {createSlice} from '@reduxjs/toolkit';
import api from '../../api/api';
import {checkObjectValid} from '../../common/utils/utils';
import Toast from 'react-native-toast-message';

export const ProductSlice = createSlice({
    name: 'product',
    initialState: {
        carts: [],
        isLoadingProductsSearch: false,
    },
    reducers: {
    },

});

export const searchProducts = async (searchInfo) => {
    const sortCriteria = checkObjectValid(searchInfo.sort) ? searchInfo.sort : ''
    const agenciesCriteria = checkObjectValid(searchInfo.agencyId) ? searchInfo.agencyId : ''
    return (await api.get('/product-templates', {params: {
            categoriesId: searchInfo.categoriesId,
            subCategoriesId: searchInfo.subCategoriesId,
            keyword: searchInfo.keyword,
            page: searchInfo.page,
            sort: sortCriteria,
            agencyId: agenciesCriteria,
            limit: 10
        }})).data.payload;
};

export const getProductDetail = async (productId) => {
    return (await api.get('/product-templates/' + productId)).data.payload;
}

export const getAllProductsByProductTempId = async (productId) => {
    return (await api.get('/product-templates/' + productId + '/products')).data.payload;
}

export const getProductComentsService = async (productID) => {
    return (await api.get('/products/' + productID + '/reviews')).data.payload.reviews;
};

export const getProductTempComentsService = async (productTempId) => {
    return (await api.get('/product-templates/' + productTempId + '/reviews')).data.payload;
}

export const getProductAppearancesService = async (productId) => {
    return (await api.get('/products/'+ productId + '/appearances')).data.payload;
}

export const reviewProduct = (reviewInfo, productId) => {
    return api.post(`/products/${productId}/reviews`, reviewInfo).then((r) => {
        Toast.show({
            type: 'success',
            text1: 'Đánh giá sản phẩm thành công!',
        });
        return true
    }).catch((e) => {
        return false;
    })
}

export default ProductSlice.reducer;

