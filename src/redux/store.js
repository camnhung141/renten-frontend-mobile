import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit';
import authReducer from './features/authSlice';
import productReducer from './features/productSlice';
import cartReducer from './features/cartSlice'
import categoryReducer from './features/categorySlice'
import customerReducer from './features/customerSlice'
import locationReducer from './features/locationSlice'
import checkoutReducer from './features/checkoutSlice'
import agenciesReducer from './features/agenciesSlice'
import orderReducer from './features/ordersSlice'

export default configureStore({
    reducer: {
        auth: authReducer,
        product: productReducer,
        cart: cartReducer,
        category: categoryReducer,
        customer: customerReducer,
        location: locationReducer,
        checkout: checkoutReducer,
        agencies: agenciesReducer,
        order: orderReducer
    }
})

