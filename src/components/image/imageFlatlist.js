import React, {useState} from 'react';
import {View, StyleSheet, FlatList, SafeAreaView, TouchableOpacity, Image} from 'react-native';
import {Colors, Dimension} from '../../common/styles/common';
import TestImg from './testImg';

const ImageFlatlist = (props) => {
    const [visible, setVisible] = useState(false)

    const renderItem = (item) => {
        return <TouchableOpacity style={styles.imageContainer} onPress={() => {setVisible(true)}}>
            <Image source={{uri: item.item}} style={styles.image}
                   resizeMode='contain'
                   resizeMethod='resize'
            />
        </TouchableOpacity>;
    };

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={props.data}
                contentContainerStyle={props.contentContainerStyle}
                renderItem={renderItem}
                horizontal={true}
            />
            <TestImg data={props.data} setIsVisible={setVisible} visible={visible}/>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: Dimension.widthNormal,
        height: Dimension.heightNormal,
    },
    imageContainer: {
        marginVertical: Dimension.marginMedium,
        marginHorizontal: Dimension.marginSmall,
        borderWidth: 2,
        borderColor: Colors.orange,
        backgroundColor: Colors.lighterGray,
        borderStyle: 'dashed',
        borderRadius: 3,
        padding: Dimension.paddingSmall
    }
});

export default ImageFlatlist;
