import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import ImageView from 'react-native-image-viewing';

const TestImg = ({visible, setIsVisible, data}) => {

    const images = data.map(item => {
        return {
            uri: item,
        };
    });

    return (
        <View style={styles.container}>
            <ImageView
                images={images}
                imageIndex={0}
                visible={visible}
                onRequestClose={() => setIsVisible(false)}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default TestImg;
