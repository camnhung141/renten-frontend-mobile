import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import CustomText from '../text/customText';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {Colors, IconStyle} from '../../common/styles/common';
import {checkObjectEmpty, checkObjectValid, dateToStringFormat, dateToTimeFortmat} from '../../common/utils/utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';

const CustomDatePicker = ({date, setDate, style, pickDateTextStyle, mode, disable}) => {
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [contentText, setContentText] = useState('---Chọn thời gian---');
    const [contextHour, setContextHour] = useState('');
    Date.prototype.addHours = function (h) {
        this.setTime(this.getTime() + (h * 60 * 60 * 1000));
        return this;
    };

    useEffect(() => {
        // if (checkObjectEmpty(date)) {
        //     setDate(new Date());
        // }
        setContentTextWithDate(date);
    }, [date]);

    const setContentTextWithDate = (item) => {
        if (!checkObjectValid(item)) {
            setContentText('---Chọn ngày---');
            setContextHour('');
        } else {
            setContentText(dateToStringFormat(item));
            if (mode !== 'date') {
                setContextHour(dateToTimeFortmat(item));
            }
        }
    };

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const handleCancel = () => {
        hideDatePicker();
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        date.addHours(7); //Time zone
        setDate(date);
        hideDatePicker();
    };


    return (
        <TouchableOpacity onPress={showDatePicker} style={{...styles.container, ...style}} disabled={!setDate || disable}>
            <CustomText style={{...styles.pickDateText, ...pickDateTextStyle, color: disable ? Colors.gray : Colors.blue}}>{contentText} </CustomText>
            <CustomText style={{...styles.pickDateText, ...pickDateTextStyle}}>{contextHour} </CustomText>
            {
                setDate
                    ?
                    <Icon name='calendar-edit' style={{...IconStyle.tiny, marginHorizontal: 0, paddingRight: 0}}
                          color={disable ? Colors.gray : Colors.blue}/>
                    : null
            }
            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode={mode ? mode : 'datetime'}
                onConfirm={handleConfirm}
                onCancel={handleCancel}
                // date={date}
            />

        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    textStyle: {},
    pickDateText: {
        fontWeight: 'bold',
        color: Colors.blue,
    },
});

export default CustomDatePicker;
