import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import CustomText from '../text/customText';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {Colors, Dimension, IconStyle} from '../../common/styles/common';
import { checkObjectValid, dateToStringFormat, dateToTimeFortmat} from '../../common/utils/utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Calendar} from 'react-native-calendars';
import {Overlay} from 'react-native-elements';

const DatePickerDisableAble = ({date, setDate, style, pickDateTextStyle, mode, disable, minDate, maxDate, orderedTime, isDefaultMinDate}) => {
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [contentText, setContentText] = useState('---Chọn thời gian---');
    const [contextHour, setContextHour] = useState('');
    const [contextHourPicker, setContextHourPicker] = useState('-Chọn giờ-');
    const [isSetTime, setIsSetTime] = useState(false);
    const [isTimeVisible, setTimeVisibility] = useState(false);
    const [isPickedDate, setIsPickedDate] = useState(false);
    const [maxDateDefault, setMaxDateDefault] = useState(null);
    Date.prototype.addHours = function (h) {
        this.setTime(this.getTime() + (h * 60 * 60 * 1000));
        return this;
    };

    Date.prototype.toDateCalendarString = function () {
        var d = this,
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }

        return [year, month, day].join('-');
    };

    const selectedStyle = {selected: true, selectedColor: Colors.orange};
    const disabledStyle = {marked: true, dotColor: Colors.red ,disabled: true, disableTouchEvent: true};

    const [disabledDates, setDisabledDates] = new useState({});
    const [markedDates, setMarkedDates] = new useState({});

    useEffect(() => {
        setContentTextWithDate(date);
        let event = {};
        if (date) {
            event[date.toDateCalendarString()] = selectedStyle;
        }
        setMarkedDates(JSON.parse(JSON.stringify({...disabledDates, ...event})))

    }, [date]);

    useEffect(() => {
        let minLimitMax = null
        if (orderedTime && !isDefaultMinDate) {
            for (const obj in orderedTime) {
                const minDateObj = new Date(orderedTime[obj].beginTime);
                if ((minLimitMax < minDateObj) && (minDateObj > minDate))
                {
                    minLimitMax = minDateObj
                }
            }
        }
        setMaxDateDefault(minLimitMax)
        if (!isDefaultMinDate && (date > minLimitMax) && minLimitMax != null) {
            setDate(minLimitMax)
        }
    },[minDate])

    useEffect(() => {

        if (checkObjectValid(orderedTime)) {
            let disabledDatesList = {}
            orderedTime.forEach(item => {
                const beginTime = new Date(item.beginTime);
                const endTime = new Date(item.endTime);
                for (let d = beginTime; d <= endTime; d.setDate(d.getDate() + 1)) {
                    if (d) {
                        let event = {}
                        event[d.toDateCalendarString()] = disabledStyle;
                        disabledDatesList = {...disabledDatesList, ...event};
                    }
                }
            })
            setDisabledDates({...disabledDatesList});
            setMarkedDates(JSON.parse(JSON.stringify({...disabledDatesList})))
        }
    },[orderedTime])

    const setContentTextWithDate = (item) => {
        if (!checkObjectValid(item)) {
            setContentText('---Chọn thời gian---');
            setContextHour('');
        } else {
            setContentText(dateToStringFormat(item));
            if (mode !== 'date') {
                setContextHour(dateToTimeFortmat(item));
                setContextHourPicker(dateToTimeFortmat(item));
            }
        }
    };

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const handleCancelTime = () => {
        setTimeVisibility(false);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (dateConfirm) => {
        const dateConfirmValue = new Date(dateConfirm.dateString);
        setIsPickedDate(true);
        setDate(dateConfirmValue);
        // let event = {};
        // if (dateConfirmValue) {
        //     event[dateConfirmValue.toDateCalendarString()] = selectedStyle;
        // }
        // setMarkedDates(JSON.parse(JSON.stringify({...disabledDates, ...event})))
        // hideDatePicker();
    };

    const handleConfirmTime = (timeConfirm) => {
        // setDate(date.setTime(timeConfirm.getTime()));
        setIsSetTime(true);
        timeConfirm.addHours(7); //Time zone

        timeConfirm.setUTCFullYear((date.getUTCFullYear()), date.getUTCMonth(), date.getUTCDate());

        setDate(timeConfirm);
        setTimeVisibility(false);
    };


    return (
        <TouchableOpacity onPress={showDatePicker}
                          style={{...styles.container, ...style, ...disable ? styles.disableContainer : styles.enableContainer}}
                          disabled={!setDate || disable}>
            <CustomText style={{
                ...styles.pickDateText, ...pickDateTextStyle,
                color: disable ? Colors.gray : Colors.blue,
            }}>{contentText} </CustomText>
            <CustomText style={{...styles.pickDateText, ...pickDateTextStyle}}>{contextHour} </CustomText>
            {
                setDate
                    ?
                    <Icon name='calendar-edit' style={{...IconStyle.tiny, marginHorizontal: 0, paddingRight: 0}}
                          color={disable ? Colors.gray : Colors.blue}/>
                    : null
            }
            <Overlay isVisible={isDatePickerVisible} onBackdropPress={() => {
                setDatePickerVisibility(!isDatePickerVisible);
            }}>
                <View>
                    <Calendar
                        onDayPress={(day) => {
                            handleConfirm(day);
                        }}
                        minDate={isDefaultMinDate ? (new Date()).toDateCalendarString() : minDate ? minDate.toDateCalendarString() : null}
                        maxDate={maxDate ? maxDate.toDateCalendarString() : maxDateDefault ? maxDateDefault.toDateCalendarString() : null}
                        markedDates={markedDates}
                    />
                    {
                        mode !== 'date' ?
                            <TouchableOpacity onPress={() => setTimeVisibility(true)} disabled={!isPickedDate} style={{
                                padding: Dimension.paddingSmall, alignItems: 'center',...!isPickedDate ? styles.disableContainer : styles.enableContainer,
                            }}>
                                <CustomText
                                    style={{...styles.pickDateText,... isPickedDate ? styles.enableText : styles.disableText}}>{contextHourPicker} </CustomText>
                            </TouchableOpacity> : null

                    }
                </View>
                <DateTimePickerModal
                    isVisible={isTimeVisible}
                    mode='time'
                    onConfirm={handleConfirmTime}
                    onCancel={handleCancelTime}
                />
            </Overlay>


        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: Dimension.paddingSmall,
    },
    textStyle: {},
    pickDateText: {
        fontWeight: 'bold',
        color: Colors.blue,
    },
    disableContainer: {
        borderWidth: 0.8,
        borderColor: Colors.gray,
        borderRadius: 25,
    },
    enableContainer: {
        borderWidth: 0.8,
        borderColor: Colors.blue,
        borderRadius: 25,
    },
    enableText: {
        color: Colors.blue,
    },
    disableText: {
        color: Colors.gray,
    },
});

export default DatePickerDisableAble;
