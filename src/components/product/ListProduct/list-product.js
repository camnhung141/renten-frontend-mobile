import React from 'react';
import {SafeAreaView, View, StyleSheet, TouchableOpacity} from 'react-native';
import ListProductHorizontal from './list-product-horizontal';
import ListProductVertical from './list-product-vertical';
import CustomText from '../../text/customText';
import {Colors, Dimension, FontSize} from '../../../common/styles/common';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {StackRoute} from '../../../navigations/stackroute/stackRoute';
import SearchBar from 'react-native-search-bar';
import Categories from '../../category/categories';
import {useNavigation} from '@react-navigation/core';

const ListProduct = ({title, horizontal, criteria, refreshing, header, footer, refreshControl}) => {
    const navigation = useNavigation();

    const onSeeMore = () => {
        navigation.navigate(StackRoute.SearchDetail, {
            criteria,
        });
    };

    const renderHeader = () => {
        return (
            <View>
                {header}
                <View style={styles.header}>
                    <CustomText style={styles.title}>{title}</CustomText>
                    <TouchableOpacity style={styles.watchMoreContainer} onPress={onSeeMore}>
                        <CustomText style={styles.watchMoreText}>xem tất cả</CustomText>
                        <Icon name="chevron-right" solid color={Colors.orange}
                              style={{marginBottom: Dimension.marginSmall}}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    return (
        <SafeAreaView style={styles.container}>
            <View>
                {
                    horizontal ?
                        <ListProductHorizontal navigation={navigation} criteria={criteria} refreshing={refreshing} header={renderHeader()} footer={footer}/> :
                        <ListProductVertical navigation={navigation} criteria={criteria} refreshing={refreshing}
                                             header={renderHeader()} footer={footer} refreshControl={refreshControl}/>
                }
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
    },
    title: {
        fontSize: FontSize.xmedium,
    },
    header: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        margin: Dimension.marginMedium,
    },
    watchMoreText: {
        fontSize: FontSize.medium,
        color: Colors.red,
        marginRight: Dimension.marginSmall,
        marginBottom: Dimension.marginSmall,
    },
    watchMoreContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
});

export default ListProduct;
