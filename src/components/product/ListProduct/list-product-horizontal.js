import React, {useEffect, useState} from 'react';
import {FlatList, SafeAreaView, StyleSheet, View} from 'react-native';
import ProductCardItem from '../productCardItem';
import {searchProducts} from '../../../redux/features/productSlice';
import LoaderProductComponent from '../../contentLoader/contentLoader';
import {useNavigation} from '@react-navigation/core';

const ListProductHorizontal = ({criteria, refreshing, header, footer}) => {
    const navigation = useNavigation();
    const [data, setData] = useState();
    const [page, setPage] = useState(2);
    const [isLoaded, setIsloaded] = useState(false);

    useEffect(() => {
        searchProducts(criteria).then(data => {
            setData(data.products);
            setIsloaded(true);
        });
    }, [refreshing]);

    const loadMore = () => {
        searchProducts({...criteria, page: page}).then(dataResult => {
            if (dataResult) {
                setData([...data, ...dataResult.products]);
                setPage(page + 1);
            }
        });
    };

    const renderItem = ({item}) => {
        return (<ProductCardItem item={item}
                                 navigation={navigation}
        />);
    };

    return (<SafeAreaView style={styles.container}>
        {header}
        <FlatList data={data}
                  renderItem={renderItem}
                  keyExtractor={item => item._id}
                  horizontal={true}
                  ListEmptyComponent={
                      isLoaded ? null
                          :
                          LoaderProductComponent()
                  }
                  onEndReached={({distanceFromEnd}) => {
                      if (distanceFromEnd < 0) {
                          return;
                      }
                      loadMore();
                  }
                  }
        />
        {footer}
    </SafeAreaView>);
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default ListProductHorizontal;
