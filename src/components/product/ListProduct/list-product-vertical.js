import React, {useCallback, useEffect, useState} from 'react';
import {FlatList, SafeAreaView, StyleSheet, View} from 'react-native';
import ProductCardItem from '../productCardItem';
import {searchProducts} from '../../../redux/features/productSlice';
import LoaderProductComponent from '../../contentLoader/contentLoader';
import {Dimension} from '../../../common/styles/common';
import {useNavigation} from '@react-navigation/core';
import {checkObjectValid} from '../../../common/utils/utils';

const ListProductVertical = ({criteria, header, footer, dataProducts, refreshing, refreshControl}) => {
    const navigation = useNavigation();
    const [data, setData] = useState();
    const [page, setPage] = useState(2);
    const [isLoaded, setIsloaded] = useState(false);

    useEffect(() => {
        if (!checkObjectValid(dataProducts)) {
            searchProducts(criteria).then(data => {
                setData(data.products);
                setIsloaded(true);
            });
        } else {
            setData(dataProducts);
            setIsloaded(true);
        }
    }, [refreshing]);

    const loadMore = () => {
        searchProducts({...criteria, page: page}).then(dataResult => {
            if (dataResult) {
                setData([...data, ...dataResult.products]);
                setPage(page + 1);
            }
        });
    };

    const renderItem = ({item}) => {
        return (<ProductCardItem item={item}
                                 navigation={navigation}
        />);
    };

    return (<SafeAreaView style={styles.container}>
        <FlatList data={data}
                  ListHeaderComponent={header}
                  ListFooterComponent={footer}
                  renderItem={renderItem}
                  numColumns={2}
                  keyExtractor={item => item._id}
                  ListEmptyComponent={
                      isLoaded ? null
                          :
                          LoaderProductComponent()
                  }
                  onEndReached={({distanceFromEnd}) => {
                      if (distanceFromEnd < 0) {
                          return;
                      }
                      loadMore();
                  }
                  }
                  onEndReachedThreshold={0.5}
                  refreshControl={refreshControl}
        />
    </SafeAreaView>);
};

const styles = StyleSheet.create({
    container: {
        paddingBottom: Dimension.paddingMedium,
    },
});

export default ListProductVertical;
