import React, {useState} from 'react';
import {Card} from 'react-native-elements';
import {Dimensions, StyleSheet, TouchableOpacity, View} from 'react-native';
import CustomText from '../text/customText';
import {Colors, Dimension, FontSize, FontStyle} from '../../common/styles/common';
import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HTML from 'react-native-render-html';

const ProductDescription = ({data}) => {

    const [isCollapsed, setCollapsed] = useState(true)
    const [watchMoreText, setWatchMoreText] = useState('Xem thêm')
    const [iconWatchMore, setIconWatchMore] = useState('chevron-down')

    const onPressWatchMore = () => {
        setCollapsed(!isCollapsed)
        isCollapsed ? setWatchMoreText('Thu gọn') : setWatchMoreText('Xem thêm')
        isCollapsed ? setIconWatchMore('chevron-up') : setIconWatchMore('chevron-down')
    }
    const contentWidth = Dimensions.get('window').width

    return <View style={styles.container}>
        <Card wrapperStyle={styles.wrapperStyle} containerStyle={styles.containerStyle}>
            <CustomText style={styles.title}>Chi tiết sản phẩm</CustomText>
            <Collapsible collapsedHeight={60} collapsed={isCollapsed} style={styles.collapsible}>
                <Card.Divider/>
                <HTML source={{ html: data }} contentWidth={contentWidth} />
                {/*<CustomText style={styles.longDescribe}>{data}</CustomText> : null*/}
            </Collapsible>
            <Card.Divider/>
            <TouchableOpacity style={styles.watchMoreButton} onPress={onPressWatchMore}>
                <CustomText style={styles.watchMore}>{watchMoreText}</CustomText>
                <Icon name={iconWatchMore} color={Colors.orange}/>
            </TouchableOpacity>
        </Card>
    </View>;
};

const styles = StyleSheet.create({
    title: {
        fontSize: FontSize.xmedium,
        marginBottom: Dimension.marginMedium,
    },
    container: {
        backgroundColor: Colors.white,
    },
    wrapperStyle: {
        borderColor: Colors.white,
        margin: 0,
        padding: 0,
    },
    containerStyle: {
        borderColor: Colors.white,
        margin: 0,
    },
    collapsible: {
        marginBottom: Dimension.marginMedium
    },
    watchMoreButton: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    watchMore: {
        color: Colors.red,
        fontSize: FontSize.medium
    },
    longDescribe: {
        marginBottom: Dimension.marginMedium
    }
});

export default ProductDescription;
