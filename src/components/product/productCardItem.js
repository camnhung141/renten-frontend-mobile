import React, {useEffect, useState} from 'react';
import {TouchableOpacity, View, StyleSheet, Dimensions} from 'react-native';
import {Card, Image, Rating, Text} from 'react-native-elements';
import {Colors, Dimension, FontSize, FontStyle, IconStyle} from '../../common/styles/common';
import CustomText from '../text/customText';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {checkObjectValid, nFormatter, toConcurrency} from '../../common/utils/utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {backgroundColor} from 'react-native-tab-view/lib/typescript/example/src/CoverflowExample';

const ProductCardItem = ({item, navigation}) => {

    const [isNew, setIsNew] = useState(false);

    const openDetailProduct = () => {
        navigation.push(StackRoute.DetailProduct, {
            name: item.name,
            id: item._id,
        });
    };

    Date.prototype.addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    useEffect(() => {
        let d1 = new Date()
        let d2 = new Date(item.createdAt)
        let d3 = d1.addDays(30)
        d2 = d2.addDays(30) // 7 days
        if (d2 >= d1 && d2 <= d3) {
            setIsNew(true);
        }
    },[])

    const windowWidth = Dimensions.get('window').width;

    return (
        <TouchableOpacity style={{width: windowWidth / 2}} onPress={openDetailProduct}>
            <Card containerStyle={styles.containerCard}>
                {
                    isNew ? <View style={{backgroundColor: Colors.white, position: 'absolute', top: 0, right: 0, zIndex: 1, borderRadius: 24}}>
                        <Icon name='new-box' style={{...IconStyle.small}} color={Colors.orange}/>
                    </View> : null
                }
                <Card.Image
                    source={item.thumbnail ? {uri: item.thumbnail} : require('../../../assets/images/no-img.png')}
                    style={styles.image}
                    resizeMode='contain'
                    resizeMethod='resize'
                />
                <Card.Divider/>
                <View style={styles.mainContainer}>
                    <CustomText style={styles.title} numberOfLines={2}>{item.name}</CustomText>
                    <View style={styles.subContainer}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <CustomText style={{fontSize: FontSize.small}}>Điểm yêu cầu: </CustomText>
                            <CustomText style={{color: Colors.red}}>({item.rentDetail.requiredPoint})</CustomText>
                        </View>
                        <View style={styles.horizontalReview}>
                            <CustomText style={styles.rateCountText}>({item.rate.rateCount}) đánh giá</CustomText>
                            <Rating
                                style={styles.rating}
                                ratingBackgroundColor={Colors.transparent}
                                readonly
                                imageSize={10}
                                ratingCount={5}
                                startingValue={item.rate.rateAverage}
                            />
                        </View>
                        <View style={styles.infoContainer}>
                            <View style={styles.priceContainer}>
                                <CustomText style={styles.price}
                                            numberOfLines={1}>{toConcurrency(item.rentPrice)}</CustomText>
                                <CustomText style={styles.currency}>₫ /{item.rentDetail.unitRent === 'date' ? 'ngày' : 'giờ'}</CustomText>
                            </View>
                            <Text style={styles.sold}>Đã thuê {nFormatter(item.rentCount, 1)}</Text>
                        </View>
                    </View>
                </View>
            </Card>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    priceContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    containerCard: {
        margin: 5,
        padding: 5,
    },
    title: {
        fontWeight: FontStyle.bold,
        fontSize: FontSize.medium,
    },
    container: {
        flex: 1 / 2,
    },
    image: {},
    price: {
        marginRight: 5,
        color: Colors.red,
        fontSize: FontSize.medium,
    },
    currency: {
        color: Colors.red,
        fontSize: FontSize.small,
        marginRight: 20,
    },
    sold: {
        alignSelf: 'flex-end',
        fontSize: FontSize.small,
    },
    infoContainer: {
        marginTop: 5,
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    mainContainer: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        flex: 1,
        height: 100,
    },
    iconTicket: {
        fontSize: FontSize.xlarge,
        marginRight: Dimension.marginSmall,
    },
    discountAndReview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    subContainer: {
        flexDirection: 'column',
        flexWrap: 'wrap',
    },
    discount: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rating: {
        alignSelf: 'flex-end',
    },
    horizontalReview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rateCountText: {
        fontSize: FontSize.small
    }
});

export default ProductCardItem;
