import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {Colors, Dimension, FontSize, FontStyle} from '../../common/styles/common';
import CustomText from '../text/customText';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {checkObjectValid} from '../../common/utils/utils';

const CategoryItem = ({item, navigation, containerStyle}) => {
    const searchByCategories = () => {
        let criteria = {
            keyword: '',
            page: 1,
        }
        if (checkObjectValid(item.parentId)) {
            criteria.subCategoriesId = item._id
        } else {
            criteria.categoriesId = item._id
        }
        navigation.push(StackRoute.SearchDetail, {
            criteria,
            categoryItem: item,
            name: item.name
        });
    }
    return (
        <View>
            <TouchableOpacity style={{...styles.container, ...containerStyle}} onPress={searchByCategories}>
                <View style={styles.iconContainer}>
                    {/*<Icon name='headphones' style={styles.icon} color={Colors.textColor}/>*/}
                    <Image style={styles.image} source={{uri: item.icon}}/>
                </View>
                <CustomText style={styles.name} numberOfLines={1}>{item.name}</CustomText>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        // flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: Dimension.paddingMedium,
        paddingVertical: Dimension.paddingSmall,
        marginVertical: Dimension.marginSmall,
        borderRadius:10,
        maxWidth: Dimension.widthItemCategory,
        // margin: Dimension.marginSmall
        backgroundColor: Colors.white,
    },
    icon: {
        fontSize: FontSize.xxxxxlarge,
        alignSelf: 'center',
        marginHorizontal: Dimension.marginSmall,
    },
    iconContainer: {
        // borderRadius:10,
        alignItems: 'center',
        // paddingHorizontal: Dimension.paddingMedium,
        // paddingVertical: Dimension.paddingSmall
    },
    name: {
        marginLeft: Dimension.marginSmall,
        color: Colors.textColor,
        // fontWeight: FontStyle.bold,
        fontSize: FontSize.medium,
        // borderWidth: 1
    },
    image: {
        width: 55,
        height: 55,
    },
});

export default CategoryItem;
