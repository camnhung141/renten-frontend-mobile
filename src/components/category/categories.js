import React, {useEffect, useState} from 'react';
import {View, StyleSheet, SafeAreaView, FlatList, ScrollView, TouchableOpacity} from 'react-native';
import CategoryItem from './CategoryItem';
import {Colors, Dimension, FontSize} from '../../common/styles/common';
import {useDispatch, useSelector} from 'react-redux';
import CustomText from '../text/customText';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {StackRoute} from '../../navigations/stackroute/stackRoute';

const Categories = ({data, navigation}) => {
    let categories = data ? data : useSelector(state => state.category.categories);
    const [dataCategories2Column, setDataCategories2Column] = useState([]);
    // const dispatch = useDispatch();

    // useEffect(() => {
    //     if (!data) {
    //         dispatch(getCategoriesService());
    //     }
    // }, []);

    useEffect(() => {
        let categoriesCurrentData = categories;
        if (categories.length > 10) {
            categoriesCurrentData = categories.slice(1, 10);
        }

        let arrayTemp = [];
        let i;
        for (i = 0; i < categoriesCurrentData.length; i += 2) {
            let item = [];
            item.push(categoriesCurrentData[i]);
            if (i + 1 < categoriesCurrentData.length) {
                item.push(categoriesCurrentData[i + 1]);
            }
            // if (i + 2 < categories.length) {
            //     item.push(categories[i + 2]);
            // }
            arrayTemp.push(item);
        }
        setDataCategories2Column(arrayTemp);
    }, [categories]);


    const renderItem = ({item}) => {
        // return <CategoryItem item={item}/>;
        return <View style={{margin: 5}}>
            <CategoryItem item={item[0]} navigation={navigation}/>

            {
                item.length > 1 ?
                    <CategoryItem item={item[1]} navigation={navigation}/>
                    : null
            }
            {/*{*/}
            {/*    item.length > 2 ?*/}
            {/*        <CategoryItem item={item[2]} navigation={navigation}/>*/}
            {/*        : null*/}
            {/*}*/}
        </View>;
    };

    const openCategoriesAll = () => {
        navigation.navigate(StackRoute.Categories, {
            data: categories
        })
    }

    return (<View style={styles.container}>
        <View style={styles.categoryHeaderContainer}>
            <CustomText style={styles.categoryHeaderTitle}>Danh mục</CustomText>
            <TouchableOpacity style={styles.watchMoreContainer} onPress={openCategoriesAll}>
                <CustomText style={{
                    color: Colors.white,
                    marginLeft: Dimension.marginMedium,
                    fontSize: FontSize.xmedium,}}>xem tất cả</CustomText>
                <Icon name="chevron-right" solid color={Colors.white} size={FontSize.large}
                      style={{marginTop: Dimension.marginSmall}}/>
            </TouchableOpacity>
        </View>
        <ScrollView horizontal={true} showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={true}>
            <FlatList data={dataCategories2Column}
                      contentContainerStyle={{alignSelf: 'flex-start'}}
                      renderItem={renderItem}
                      keyExtractor={item => item[0]._id}
                      horizontal={true}
                // numColumns={Math.ceil(categories.length / 2)}
                      showsVerticalScrollIndicator={false}
                      showsHorizontalScrollIndicator={false}
                      scrollEnabled={false}
            />
        </ScrollView>
    </View>);
};

const styles = StyleSheet.create({
    container: {
        // paddingVertical: Dimension.paddingMedium,
        backgroundColor: Colors.orange,
    },
    categoryHeaderTitle: {
        color: Colors.white,
        fontWeight: 'bold',
        marginLeft: Dimension.marginMedium,
        fontSize: FontSize.xmedium,
    },
    categoryHeaderContainer: {
        // backgroundColor: Colors.orange,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: Dimension.paddingMedium,
        paddingVertical: Dimension.paddingMedium,
    },
    watchMoreContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
});

export default Categories;
