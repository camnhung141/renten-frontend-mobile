import React, {useEffect} from 'react';
import {View, StyleSheet, ImageBackground, Image} from 'react-native';
import {Card} from 'react-native-elements';
import {getCategoriesService} from '../../redux/features/categorySlice';
import {getCartsService} from '../../redux/features/cartSlice';
import {useDispatch} from 'react-redux';
import {getRefreshToken} from '../../storage/async-storage';
import {getUserInfo} from '../../redux/features/customerSlice';
import {setStatusLoginService} from '../../redux/features/authSlice';

const MainSplashScreen = (props) => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getCategoriesService());
        dispatch(getCartsService());

        getRefreshToken().then(result => {
            if (result) {
                dispatch(getUserInfo())
                dispatch(setStatusLoginService(true))
            }
        })
    })

    return (
      <View style={styles.container}>
        <Image style={{width: '100%'}}
               source={require('../../../assets/images/logo.png')}
               resizeMode='contain'
               resizeMethod='resize'
        />
      </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center'
    }
})

export default MainSplashScreen;
