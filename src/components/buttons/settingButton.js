import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import {Colors, Dimension, FontSize, IconStyle} from '../../common/styles/common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SettingButton = ({title, onChooseOption, style, icon, rightTitle, disabled, colorIcon}) => {

    return (
        <View style={{...styles.buttonContainer, ...style}}>
            <TouchableOpacity
                onPress={onChooseOption}
                style={styles.touchContainer}
                disabled={disabled}
            >
                <View style={styles.horizontalContainer}>
                    {icon ?
                        <View>
                            <Icon name={icon} style={IconStyle.small} color={colorIcon || Colors.gray}/>
                        </View> : null}
                    <Text style={styles.textTitle}>{title}</Text>
                </View>
                <View style={styles.horizontalContainer}>
                    {
                        rightTitle ?
                            <Text style={styles.textTitle}>{rightTitle}</Text> : null
                    }
                    {
                        disabled ?
                            null :
                            <View>
                                <Icon name='menu-right' style={{...IconStyle.small, marginHorizontal: 0, paddingRight: 0}} color={Colors.gray}/>
                            </View>
                    }
                </View>
            </TouchableOpacity>
        </View>
    );
};


const styles = StyleSheet.create({
    buttonContainer: {
        width: '100%',
        height: 40,
        backgroundColor: Colors.white,
    },
    textTitle: {
        color: Colors.black,
        // textAlign: 'center',
    },
    rounded: {
        borderRadius: 5,
    },
    notRounded: {},
    touchContainer: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: Dimension.paddingXMedium,
    },
    horizontalContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: Dimension.marginSmall,
        paddingRight: Dimension.paddingMedium
    },
});

export default SettingButton;
