import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import propTypes from 'prop-types';
import {Colors, Dimension, FontSize, IconStyle} from '../../common/styles/common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const CustomButton = ({title, backgroundColor, onChooseOption, style, isNotRound, disabled, textColor, isTextBold, icon, border, vGrow, iconColor}) => {
    let colorOfText = textColor ? textColor : Colors.white;
    const textBold = isTextBold ? 'bold' : '600';
    colorOfText = border ? Colors.orange : colorOfText;

    return (
        <View style={{
            ...styles.buttonContainer,
            backgroundColor, ...style, ...isNotRound ? {} : styles.rounded, ...disabled ? styles.disabled : {},
            ...border ? styles.border : {},
            ...vGrow ? {height: '100%'} : {}
            , paddingHorizontal: Dimension.paddingSmall
        }}>
            <TouchableOpacity
                onPress={onChooseOption}
                disabled={disabled}
                style={{flexDirection: 'row', justifyContent: 'center'}}
            >
                {icon ?
                    <View>
                        <Icon name={icon}
                              style={{...IconStyle.small, ...styles.icon, ...border ? styles.colorOrange : {}}}
                              color={iconColor ? iconColor : Colors.gray}/>
                    </View> : null}
                <Text style={{...styles.textTitle, color: colorOfText, fontWeight: textBold}}>{title}</Text>
            </TouchableOpacity>
        </View>
    );
};


const styles = StyleSheet.create({
    buttonContainer: {
        width: '100%',
        minHeight: 40,
        justifyContent: 'center',
        padding: Dimension.paddingSmall
    },
    textTitle: {
        color: Colors.white,
        fontSize: FontSize.large,
        // fontWeight: 'bold',
        textAlign: 'center',
    },
    rounded: {
        borderRadius: 5,
    },
    disabled: {
        backgroundColor: Colors.disabledBtn,
    },
    notRounded: {},
    horizontalContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        // marginTop: Dimension.marginSmall,
    },
    colorOrange: {
        color: Colors.orange,
    },
    border: {
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Colors.orange,
    },
});
CustomButton.propTypes = {
    title: propTypes.string.isRequired,
    backgroundColor: propTypes.string,
    onChooseOption: propTypes.func.isRequired,
    width: propTypes.string,
};
CustomButton.defaultProps = {
    backgroundColor: Colors.transparent,
};

export default CustomButton;
