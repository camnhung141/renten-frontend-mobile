import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import propTypes from 'prop-types';
import {Colors, FontSize} from '../../common/styles/common';

const OutlineButton = ({ title, onChooseOption, isTextBold }) => {

    const textBold = isTextBold ? 'bold' : '600'

    return <View style={{...styles.buttonContainer}}>
        <TouchableOpacity
            onPress={onChooseOption}
        >
            <Text style={{... styles.textTitle, fontWeight: textBold}}>{title}</Text>
        </TouchableOpacity>
    </View>;
}

const styles = StyleSheet.create({
    buttonContainer: {
        width: '100%',
        height: 40,
        borderRadius: 5,
        borderWidth: 1,
        // backgroundColor: Colors.transparent,
        borderColor: Colors.secondaryColor,
        justifyContent: 'center',
    },
    textTitle: {
        color: Colors.blue,
        fontSize: FontSize.xmedium,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});
OutlineButton.propTypes = {
    title: propTypes.string.isRequired,
    onChooseOption: propTypes.func.isRequired,
};
export default OutlineButton;
