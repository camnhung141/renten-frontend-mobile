import React, {useState} from 'react';
import propTypes from 'prop-types';
import {TextInput, View, StyleSheet} from 'react-native';
import {Colors, Dimension, FontSize, IconStyle, Input, InputStyle} from '../../common/styles/common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomText from '../text/customText';

const CustomInput = ({
                         textAlignVertical,
                         placeholder,
                         defaultValue,
                         onChangeText,
                         isSecure,
                         keyboardType,
                         style,
                         multiline,
                         isValid,
                         iconName,
                         errorMessage,
                         isUpperCaseName,
                     }) => {
    const [height, setHeight] = useState(40);

    if (isValid === undefined) {
        isValid = true;
    }

    function capitalizeTheFirstLetterOfEachWord(words) {
        var separateWord = words.toLowerCase().split(' ');
        for (var i = 0; i < separateWord.length; i++) {
            separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
                separateWord[i].substring(1);
        }
        return separateWord.join(' ');
    }

    const innerOnchangeText = (text) => {
        if (isUpperCaseName) {
            onChangeText(capitalizeTheFirstLetterOfEachWord(text));
        } else {
            onChangeText(text);
        }
    };

    const updateSize = (newHeight) => {
        setHeight(newHeight);
    };

    return (
        <View style={{width: '100%', marginBottom: Dimension.marginSmall}}>
            <TextInput
                textAlignVertical={textAlignVertical}
                defaultValue={defaultValue}
                onChangeText={innerOnchangeText}
                placeholder={placeholder}
                placeholderTextColor={Colors.subTextColor}
                style={{...styles.input, height: height, ...style, ...isValid ? Input.validInput : Input.invalidInput}}
                secureTextEntry={isSecure}
                keyboardType={keyboardType}
                multiline={multiline}
                onContentSizeChange={(e) => updateSize(e.nativeEvent.contentSize.height)}
            />
            {
                !isValid ?
                    <CustomText style={styles.errorMsg}>
                        {errorMessage}
                    </CustomText> : null
            }
        </View>
    );
};


const styles = StyleSheet.create({
    input: {
        borderRadius: 7,
        height: 44,
        padding: 10,
        borderColor: Colors.blue,
        borderWidth: 1,
        backgroundColor: Colors.white,
    },
    invalidInput: {
        borderColor: Colors.red,
    },
    validInput: {
        borderColor: Colors.blue,
    },
    errorMsg: {
        color: Colors.red,
        fontSize: FontSize.small,
    },
});
CustomInput.propTypes = {
    placeholder: propTypes.string,
    defaultValue: propTypes.string,
    onChangeText: propTypes.func,
    isSecure: propTypes.bool,
    keyboardType: propTypes.string,
};
CustomInput.defaultProps = {
    placeholder: '',
    defaultValue: '',
    onChangeText: () => {
    },
    isSecure: false,
    keyboardType: 'default',
};

export default CustomInput;
