import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import CustomText from '../text/customText';
import {Colors, Dimension, FontSize} from '../../common/styles/common';
import {Divider, Rating} from 'react-native-elements';
import CommentContent from './commentContent';
import ImageFlatlist from '../image/imageFlatlist';
import {dateToStringFormat, dateToTimeFortmat} from '../../common/utils/utils';
import AppearanceItem from '../../containers/appearances/appearanceItem';

const CommentItem = (props) => {
    const data = props.data;

    return (
        <View style={styles.container}>
            <View style={styles.imageContainer}>
                <Image style={styles.image} source={data.avatar ? {uri: data.avatar} : require('../../../assets/images/no-img.png')}/>
            </View>
                <View style={{marginLeft: Dimension.marginSmall, borderLeftWidth: 0.5, borderColor: Colors.orange, paddingVertical: Dimension.paddingSmall}}/>
            {/*<View style={styles.headerComment}>*/}
            <View style={styles.content}>
                <CustomText style={styles.name}>{data.name}</CustomText>
                <Rating
                    ratingBackgroundColor={Colors.transparent}
                    readonly
                    imageSize={10}
                    ratingCount={5}
                    startingValue={data.rating}
                    style={styles.rating}
                />
                <CommentContent data={data.content}/>
                <View>
                    <ImageFlatlist data={data.images}/>
                </View>
                <CustomText style={styles.time}>{dateToStringFormat(data.timeCreated)} : {dateToTimeFortmat(data.timeCreated)}</CustomText>

                <Divider style={{marginTop: Dimension.marginMedium}}/>
            </View>
            {/*</View>*/}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        // marginRight: Dimension.marginMedium
    },
    image: {
        width: 44,
        height: 44,
        borderRadius: 44 / 2,
        marginRight: Dimension.marginSmall,
    },
    headerComment: {
        flexDirection: 'row',
    },
    name: {
        // fontWeight: '500'
        // color: Colors.gray,
    },
    content: {
        // flexDirection: 'column',
        marginTop: Dimension.marginSmall,
        marginBottom: Dimension.marginSmall,
        // paddingRight: Dimension.paddingSmall
        width: '85%',
        marginLeft: Dimension.marginSmall
    },
    rating: {
        alignSelf: 'flex-start',
        marginBottom: Dimension.marginMedium,
    },
    time: {
        color: Colors.gray,
        fontSize: FontSize.small
    },
    imageContainer: {
        marginTop: Dimension.marginMedium
    }
});

export default CommentItem;
