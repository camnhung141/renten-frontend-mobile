import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import CommentItem from './commentItem';
import {
    getProductTempComentsService
} from '../../redux/features/productSlice';
import {Colors, Dimension, DividerStyle, FontSize} from '../../common/styles/common';
import {Divider} from 'react-native-elements';
import CustomText from '../text/customText';

const CommentList = (props) => {

    const [data, setData] = useState();

    const productTempId = props.productTemplateId;
    useEffect(() => {
        // if (productId) {
        //     getProductComentsService(productId).then(data => {
        //         setData(data);
        //     });
        // } else {
        //     getProductTempComentsService(productTempId).then(data => {
        //         setData(data);
        //     })
        // }

        getProductTempComentsService(productTempId).then(data => {
            setData(data);
        })
    }, []);

    const renderItem = ({item}) => {
        return (<View>
                <CommentItem data={item}
                />
            </View>
        );
    };

    return (
        <View style={{...styles.container, ...props.style}}>
            <CustomText style={styles.title}>Đánh giá</CustomText>
            <Divider style={DividerStyle.horizontal}/>
            <FlatList data={data}
                      contentContainerStyle={{alignSelf: 'flex-start'}}
                      renderItem={renderItem}
                      keyExtractor={item => item._id}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1,
        padding: Dimension.marginMedium,
    },
    title: {
        fontSize: FontSize.xmedium,
        marginBottom: Dimension.marginMedium,
        // backgroundColor: Colors.orange
    },
});

export default CommentList;
