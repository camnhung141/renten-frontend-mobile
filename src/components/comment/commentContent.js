import React from 'react';
import {StyleSheet, View} from 'react-native';
import CustomText from '../text/customText';
import {Dimension} from '../../common/styles/common';

const CommentContent = ({data}) => {

    return <View style={styles.container}>
        <CustomText style={styles.longDescribe} >{data}</CustomText>
    </View>;
};

const styles = StyleSheet.create({
    container: {
        // backgroundColor: Colors.white,
        marginRight: Dimension.marginMedium,
    },
    longDescribe: {
        marginBottom: Dimension.marginMedium,
    },
});

export default CommentContent;
