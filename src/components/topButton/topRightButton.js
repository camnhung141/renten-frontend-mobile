import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import CustomText from '../text/customText';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, Dimension, IconStyle} from '../../common/styles/common';

const TopRightButton = ({onPress, title, icon, disabled}) => {
    return (
        <TouchableOpacity style={styles.container} disabled={disabled} onPress={onPress}>
            <CustomText style={styles.title}>{title}</CustomText>
            {icon ?
                <View>
                    <Icon name={icon}
                          style={{...IconStyle.small}}
                          color={disabled ? Colors.gray : Colors.white}/>
                </View> : null}
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginRight: Dimension.marginMedium,
        alignItems: 'center'
    },
    title: {
        color: Colors.orange,
    },
});

export default TopRightButton;
