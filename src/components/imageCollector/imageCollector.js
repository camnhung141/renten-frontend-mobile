import React, {useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {Divider} from 'react-native-elements';
import CustomButton from '../buttons/customButton';
import {CameraSetting, Dimension} from '../../common/styles/common';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImageItem from './imageItem/imageItem';

const ImageCollector = ({data, setData, limit}) => {
    const dataTest = [1, 2, 3, 4];
    const [id, setId] = useState(0);

    const takeImage = () => {
        launchCamera(CameraSetting.image, (response) => {
            if (!response.didCancel) {
                setData(oldArray => [...oldArray, {...response, id: id}]);
                setId(id + 1);
            }
        });
    };

    const openGallery = () => {
        launchImageLibrary(CameraSetting.image, (response) => {
            if (!response.didCancel) {
                setData(oldArray => [...oldArray, {...response, id: id}]);
                setId(id + 1);
            }
        });
    };

    const removeItem = (itemId) => {
        setData(data.filter(item => item.id !== itemId));
    };

    const renderImg = (item) => {
        return <ImageItem item={item.item} removeItem={removeItem}/>;
    };

    return (
        <View>
            <View style={styles.imageBtnArea}>
                <View style={{...styles.btnImage, width: '38%'}}>
                    <CustomButton title='Chụp ảnh' icon='camera' border={true} onChooseOption={takeImage}/>
                </View>
                <View style={{...styles.btnImage, width: '58%'}}>
                    <CustomButton title='Chọn ảnh từ thư viện' icon='folder-multiple-image' border={true}
                                  onChooseOption={openGallery}/>
                </View>
            </View>
            {
                dataTest.length > 0 ?
                    <FlatList
                        data={data}
                        renderItem={renderImg}
                        horizontal={true}
                        style={styles.flatlistImg}
                    />
                    : null
            }
        </View>
    );
};

const styles = StyleSheet.create({
    container: {},
    imageBtnArea: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    btnImage: {
        marginVertical: Dimension.marginSmall,
    },
    flatlistImg: {
        padding: Dimension.paddingSmall,
    },
});

export default ImageCollector;
