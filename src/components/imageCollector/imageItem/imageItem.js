import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image, ImageBackground, Text} from 'react-native';
import {Colors, Dimension, IconStyle} from '../../../common/styles/common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ImageItem = ({item, removeItem}) => {
  return (
      <View style={styles.imageContainer}>
              <ImageBackground style={styles.image}
                               source={{uri: item.uri}}>
                  <TouchableOpacity onPress={() => removeItem(item.id)}>
                      <Icon name='close'
                            style={{...IconStyle.small, ...styles.icon}}
                            color={Colors.gray}/>
                  </TouchableOpacity>
              </ImageBackground>
      </View>
  );
}

const styles = StyleSheet.create({
    imageContainer: {
        width: Dimension.widthNormal,
        height: Dimension.heightNormal,
        borderWidth: 1,
        borderColor: Colors.orange,
        borderStyle: 'dashed',
        borderRadius: 1,
        margin: Dimension.marginSmall
    },
    image: {
        width: '100%',
        height: '100%',
    },
    icon: {
        color: Colors.orange,
        backgroundColor: Colors.lightGray,
        borderRadius: 50,
        margin: Dimension.marginSmall,
        alignSelf: 'flex-end'
    },
})

export default ImageItem;
