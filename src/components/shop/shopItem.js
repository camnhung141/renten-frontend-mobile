import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image, Dimensions, PointPropType} from 'react-native';
import {Colors, Dimension, DividerStyle, FontSize, IconStyle} from '../../common/styles/common';
import CustomText from '../text/customText';
import {Divider} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ShopItem = ({onPress, info}) => {

    const widthContent = Dimensions.get('window').width / 3;

    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <CustomText style={styles.title}>Cửa hàng</CustomText>
                <Icon name='store'
                      style={{...IconStyle.small, marginHorizontal: 0, paddingRight: 10, paddingLeft: 10, marginBottom: Dimension.marginMedium}}
                      color={Colors.orange}/>
            </View>
            <Divider style={DividerStyle.horizontal}/>
            <TouchableOpacity onPress={onPress} disabled={!onPress}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{flex: 1}}>
                        <View style={styles.infoContainer}>
                            <View>
                                <Image style={styles.avatar}
                                       source={info.avatar ? {uri: info.avatar} : require('../../../assets/images/no-img.png')}/>
                            </View>
                            <View style={{flex: 1}}>
                                <View style={{flexDirection: 'row'}}>
                                    <CustomText style={styles.name} numberOfLines={2}>{info.agencyName}</CustomText>
                                </View>
                                <View style={{flex: 1}}>
                                    {
                                        info.about ? <CustomText numberOfLines={2}>{info.about}</CustomText> : null
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        padding: Dimension.paddingMedium,
        backgroundColor: Colors.white,
        flex: 1,
    },
    avatar: {
        width: 44,
        height: 44,
        borderRadius: 44 / 2,
        marginRight: Dimension.marginMedium,
    },
    name: {
        fontWeight: 'bold',
        fontSize: FontSize.medium,
        color: Colors.black,
    },
    infoContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        flex: 1,
        width: '100%',
        backgroundColor: Colors.white,
        paddingVertical: Dimension.paddingMedium,
        paddingLeft: Dimension.paddingMedium,
    },
    chatIconContainer: {
        borderWidth: 1,
        borderColor: Colors.orange,
        borderRadius: 22,
        paddingHorizontal: Dimension.paddingSmall
    },
    number: {
        color: Colors.orange,
        fontWeight: 'bold',
        fontSize: FontSize.medium,
    },
    title: {
        fontSize: FontSize.xmedium,
        marginBottom: Dimension.marginMedium
    }
});

export default ShopItem;
