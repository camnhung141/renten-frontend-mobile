import React from 'react';
import {Text, StyleSheet, View} from 'react-native';
import propTypes from 'prop-types';
import {Colors} from '../../common/styles/common';

const CustomText = ({style, numberOfLines, children, isRequired}) => {
    return (
        <View style={styles.container}>
            <Text numberOfLines={numberOfLines} style={{...styles.text,...style}}>
                {children}
                {
                    isRequired ?
                        <Text style={{color: Colors.red}}> *</Text>
                        : null
                }
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    text: {
        fontFamily: 'Roboto',
        flexWrap: 'wrap'
    },
    container: {
        flexDirection: 'row',
        alignContent: 'flex-start',
    },
});


export default CustomText;
