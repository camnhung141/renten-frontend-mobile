import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import CustomText from '../../../components/text/customText';
import CustomDatePicker from '../../../components/datePicker/customDatePicker';
import NumericInput from 'react-native-numeric-input';
import {Colors, Dimension, FontSize, FontStyle} from '../../../common/styles/common';
import {StackRoute} from '../../../navigations/stackroute/stackRoute';
import {useNavigation} from '@react-navigation/native';
import {checkObjectValid, toConcurrency} from '../../../common/utils/utils';

const PaymentItem = ({item}) => {

    const navigation = useNavigation();


    return (
        <View style={styles.container}>
            <View style={styles.mainContainer}>
                <View style={styles.imageContainer}>
                    <Image style={styles.image}
                           source={item.product.thumbnail ? {uri: item.product.thumbnail} : require('../../../../assets/images/no-img.png')}/>
                </View>
                <View style={styles.infoContainer}>
                    <CustomText numberOfLines={2} style={styles.title}>{item.product.name}</CustomText>
                    <CustomText style={styles.title}>{item.product.productCode}</CustomText>
                    <CustomText >{item.product.appearance}</CustomText>
                    <View style={styles.horizontalView}>
                        <CustomText style={styles.label}>Ngày thuê: </CustomText>
                        <CustomDatePicker date={item.product.beginTime} style={styles.pickDate}/>
                    </View>
                    <View style={styles.horizontalView}>
                        <CustomText style={styles.label}>Ngày trả: </CustomText>
                        <CustomDatePicker date={item.product.endTime} style={styles.pickDate}/>
                    </View>
                </View>
            </View>
            <View style={styles.totalPriceContainer}>
                <CustomText>Số tiền thuê: </CustomText>
                <CustomText style={styles.totalPrice}>{toConcurrency(item.totalPrice)} ₫</CustomText>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        // margin: Dimension.marginSmall,
        marginVertical: Dimension.marginSmall,
        backgroundColor: Colors.white,
        paddingHorizontal: Dimension.paddingSmall,
        paddingVertical: Dimension.paddingMedium
    },
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: Dimension.marginSmall,
        // marginVertical: Dimension.marginMedium
    },
    label: {
        // fontWeight: 'bold',
    },
    horizontalView: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: Dimension.marginSmall
    },
    horizontalPriceAndQuantity: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: Dimension.marginSmall
    },
    imageContainer: {
        width: Dimension.widthNormal,
        height: Dimension.heightNormal,
    },
    image: {
        width: '100%',
        height: '100%',
    },
    title: {
        fontWeight: FontStyle.bold,
    },
    infoContainer: {
        flex: 1,
        margin: Dimension.marginSmall,
    },
    price: {
        color: Colors.red,
        // fontWeight: 'bold'
    },
    totalPrice: {
        color: Colors.red,
        fontWeight: 'bold',
        fontSize: FontSize.xmedium
    },
    totalPriceContainer: {
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center'
    }
});

export default PaymentItem;
