import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Colors, Dimension, FontStyle, IconStyle} from '../../common/styles/common';
import CustomText from '../../components/text/customText';
import {CheckBox} from 'react-native-elements';
import {LinearGradient} from 'react-native-svg';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {updatePaymentMethod} from '../../redux/features/checkoutSlice';
import {useDispatch, useSelector} from 'react-redux';

const PaymentMethodPicker = (props) => {
    const method = useSelector(state => state.checkout.paymentUpdated)

    const [isSelectMomo, setSelectMomo] = useState(false);
    const [isSelectCash, setSelectCash] = useState(false);

    const dispatch = useDispatch();

    useEffect(() => {
        if (method === 'momo') {
            setSelectMomo(true);
            setSelectCash(false);
        } else {
            setSelectMomo(false);
            setSelectCash(true);
        }
    },[]);

    const selectMethod = (methodCheck) => {
        if (methodCheck === 'momo') {
            setSelectMomo(true);
            setSelectCash(false);
        } else {
            setSelectMomo(false);
            setSelectCash(true);
        }
        dispatch(updatePaymentMethod(methodCheck))
    }

    return (
        <View style={styles.container}>
            <CustomText style={{marginBottom: Dimension.marginMedium}}>
                Chọn phương thức thanh toán
            </CustomText>
            <TouchableOpacity style={styles.rowParent} onPress={() => {
                selectMethod('momo')
            }}>
                <View style={styles.row}>
                    <View>
                        <Image source={require('../../../assets/images/logoMomo.png')} style={IconStyle.momo}/>
                    </View>
                    <CustomText style={styles.label}>
                        Momo
                    </CustomText>
                </View>

                {
                    isSelectMomo ? <View>
                        <Icon name='check' color={Colors.orange} style={styles.icon}/>
                    </View> : null
                }
            </TouchableOpacity>
            <TouchableOpacity style={styles.rowParent} onPress={() => {
                selectMethod('cash')
            }}>
                <View style={styles.row}>
                    <View style={{
                        borderRadius: 25,
                        paddingHorizontal: Dimension.paddingMedium,
                        paddingVertical: Dimension.paddingSmall,
                        borderWidth: 1,
                        borderColor: Colors.red,
                    }}>
                        <CustomText
                            style={{fontWeight: FontStyle.bold, color: Colors.red}}>Cash</CustomText>
                    </View>
                    <CustomText style={styles.label}>
                        Cash
                    </CustomText>
                </View>

                {
                    isSelectCash ?
                        <View>
                            <Icon name='check' color={Colors.orange} style={styles.icon}/>
                        </View> : null
                }
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingVertical: Dimension.paddingMedium,
        paddingLeft: Dimension.paddingMedium,
    },
    rowParent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: Dimension.marginSmall,
        width: '100%',
        backgroundColor: Colors.white,
    },
    label: {
        marginLeft: Dimension.marginMedium,
    },
    icon: {
        marginRight: Dimension.marginMedium,
    },
});

export default PaymentMethodPicker;
