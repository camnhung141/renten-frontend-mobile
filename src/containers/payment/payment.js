import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView, RefreshControl, FlatList, TouchableOpacity, Image} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getCartsService} from '../../redux/features/cartSlice';
import {checkObjectEmpty, checkObjectValid, toConcurrency, wait} from '../../common/utils/utils';
import {doPayment, getPayments, updateMethodPaymentState} from '../../redux/features/checkoutSlice';
import {
    Colors,
    Dimension,
    FontSize,
    IconStyle,
    FontStyle,
    StatusOrderColor,
    StatusOrderMessage,
} from '../../common/styles/common';
import ProductCartItem from '../cart/productCartItem';
import PaymentItem from './paymentItem/paymentItem';
import {Divider} from 'react-native-elements';
import CustomButton from '../../components/buttons/customButton';
import CustomText from '../../components/text/customText';
import AddressItem from '../location/addressItem';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/core';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import MomoTest from '../../payment/momoTest';
import DeepLinkMomo from '../../payment/deepLinkMomo';
import Toast from 'react-native-toast-message';

const Payment = (props) => {
    const [products, setProducts] = useState([]);
    const [paymentData, setPaymentData] = useState([]);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [addressItem, setAddressItem] = useState({});
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const paymentMethod = useSelector(state => state.checkout.paymentUpdated);

    useEffect(() => {
        setIsRefreshing(true);
        getPayments().then(payments => {
            setPaymentData(payments);
            setProducts(payments.products);
            setAddressItem({
                ...payments.products[0].deliveryAddress,
                fullName: payments.products[0].fullName,
                phoneNumber: payments.products[0].phoneNumber,
                selected: true,
            });
            dispatch(updateMethodPaymentState(payments.products[0].payment.method));
        });
        wait(500).then(() => setIsRefreshing(false));
    }, []);

    const onRefresh = () => {
        setIsRefreshing(true);
        getPayments().then(payments => {
            setPaymentData(payments);
            setProducts(payments.products);
            setAddressItem({
                ...payments.products[0].deliveryAddress,
                fullName: payments.products[0].fullName,
                phoneNumber: payments.products[0].phoneNumber,
                selected: true,
            });
            dispatch(updateMethodPaymentState(payments.products[0].payment.method));
        });
        wait(500).then(() => setIsRefreshing(false));
    };

    const renderPaymentItem = (productItem) => {
        return <PaymentItem item={productItem.item}/>;
    };

    const renderSeparator = () => {
        return <Divider style={styles.divider}/>;
    };

    const onPayment = () => {
        doPayment().then(() => {
            Toast.show({
                type: 'success',
                text1: 'Đặt thuê thành công!!!',
            });
            navigation.navigate(StackRoute.Home);
        });
    };

    const onOpenAdress = () => {
        navigation.navigate(StackRoute.Location);
    };

    const renderPaymentMethod = () => {
        //Todo check momo method later
        if (paymentMethod === 'momo') {
            return <View>
                <Image source={require('../../../assets/images/logoMomo.png')} style={IconStyle.momo}/>
            </View>;
        } else if (paymentMethod === 'cash') {
            return <View>
                <View style={{
                    borderRadius: 25,
                    paddingHorizontal: Dimension.paddingMedium,
                    paddingVertical: Dimension.paddingSmall,
                    borderWidth: 1,
                    borderColor: Colors.red,
                    marginLeft: Dimension.marginMedium,
                }}>
                    <CustomText
                        style={{fontWeight: FontStyle.bold, color: Colors.red}}>{paymentMethod}</CustomText>
                </View>
            </View>;
        }
    };

    const renderPaymentButton = () => {
        // Todo check method momo

        if (checkObjectValid(products[0]) &&
            ((checkObjectEmpty(addressItem) || !products[0].hasOwnProperty('deliveryAddress')))) {
            return <CustomButton title='Cập nhật địa chỉ' backgroundColor={Colors.red}
                                 style={{width: '50%'}} isNotRound={true}
                                 onChooseOption={onOpenAdress}
            />;
        }
        else {
            if (paymentMethod === 'momo') {
                return <DeepLinkMomo/>;
            } else if (paymentMethod === 'cash') {
                return <CustomButton title='Thuê hàng' backgroundColor={Colors.orange}
                                     style={styles.button} isNotRound={true}
                                     onChooseOption={onPayment}
                />;
            }
        }
    };

    const changeMethod = () => {
        navigation.navigate(StackRoute.ChangeMethodPayment);
    };

    return (
        <View style={styles.container}>
            <ScrollView style={styles.contentContainer}
                        refreshControl={
                            <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh}/>
                        }>
                {
                    checkObjectValid(products[0]) ?
                        (checkObjectEmpty(addressItem) || !products[0].hasOwnProperty('deliveryAddress')) ?
                            null : <AddressItem item={addressItem} onPressAddressItem={onOpenAdress} showTitle={true}/>
                        : null
                }
                <FlatList
                    style={styles.flatListCart}
                    data={products}
                    renderItem={renderPaymentItem}
                    ItemSeparatorComponent={renderSeparator}
                />
                <View style={styles.payMethodAndPriceContainer}>
                    <View style={{...styles.paymentMethodContainer, ...styles.horizontalContainer}}>
                        <View style={{...styles.paymentMethodTitle, ...styles.horizontalContainer}}>
                            <Icon name='currency-usd' style={{
                                ...IconStyle.small,
                                marginHorizontal: 0,
                                paddingRight: Dimension.paddingSmall,
                            }}
                                  color={Colors.red}/>
                            <CustomText>Phương thức thanh toán</CustomText>
                        </View>
                        <TouchableOpacity onPress={changeMethod}>
                            {
                                renderPaymentMethod()
                            }
                        </TouchableOpacity>

                    </View>
                    <Divider style={styles.divider}/>
                    <View style={{...styles.horizontalContainer, ...styles.totalPriceContainer}}>
                        <CustomText style={styles.totalPriceLabel}>Tổng tiền thanh toán</CustomText>
                        {
                            paymentData ?
                                <CustomText
                                    style={styles.totalPrice}>{toConcurrency(paymentData.totalPrice)} ₫</CustomText> : null
                        }
                    </View>
                </View>
            </ScrollView>
            <View style={styles.bottomArea}>
                <View style={styles.horizon}>
                    <CustomText style={styles.label}>Tổng tiền: </CustomText>

                    {
                        paymentData ?
                            <CustomText
                                style={styles.totalPrice}>{toConcurrency(paymentData.totalPrice)} ₫</CustomText> : null}
                </View>
                {
                    renderPaymentButton()
                }
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
    },
    contentContainer: {
        backgroundColor: Colors.backgroundColorGray,
    },
    flatListCart: {
        // backgroundColor: Colors.white,
    },
    divider: {
        marginVertical: Dimension.marginSmall,
    },
    bottomArea: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        borderTopWidth: Dimension.borderSmall,
        borderColor: Colors.gray,
        paddingLeft: Dimension.paddingMedium,
    },
    button: {
        width: '40%',
    },
    horizon: {
        flexDirection: 'row',
    },
    price: {
        color: Colors.red,
    },
    label: {
        fontWeight: 'bold',
    },
    totalPrice: {
        color: Colors.red,
        fontWeight: 'bold',
        fontSize: FontSize.xmedium,
    },
    horizontalContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    paymentMethodContainer: {
        // justifyContent: 'space-between'
        marginBottom: Dimension.marginMedium,
    },
    paymentMethodTitle: {
        alignItems: 'center',
    },
    payMethodAndPriceContainer: {
        backgroundColor: Colors.white,
        padding: Dimension.paddingMedium,
        marginVertical: Dimension.marginMedium,
    },
    paymentMethod: {
        color: Colors.red,
        fontSize: FontSize.medium,
    },
    totalPriceContainer: {
        marginVertical: Dimension.marginSmall,
    },
    totalPriceLabel: {
        fontWeight: FontStyle.bold,
        fontSize: FontSize.medium,
    },
});

export default Payment;
