import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {CheckBox, Divider} from 'react-native-elements';
import CustomText from '../../../components/text/customText';
import CustomDatePicker from '../../../components/datePicker/customDatePicker';
import NumericInput from 'react-native-numeric-input';
import {Colors, Dimension, FontStyle, StatusOrder} from '../../../common/styles/common';
import {checkObjectValid, dateToStringFormat, dateToTimeFortmat, toConcurrency} from '../../../common/utils/utils';
import CustomButton from '../../../components/buttons/customButton';
import {useDispatch, useSelector} from 'react-redux';
import {StackRoute} from '../../../navigations/stackroute/stackRoute';
import {useNavigation} from '@react-navigation/core';
import {updateOrder} from '../../../redux/features/ordersSlice';

const OrderItem = ({item, disableDetailPress}) => {
    const navigation = useNavigation();
    const [itemData, setItemData] = useState({product: {}, deliveryAddress: {}, payment: {}, history: {}});
    const dispatch = useDispatch();

    useEffect(() => {
        if (checkObjectValid(item)) {
            setItemData(item);
        }
    }, []);

    const onReview = () => {
        navigation.navigate(StackRoute.Review, {
            data: itemData,
        });
    };

    const warningOrder = () => {
        return <View style={styles.warning}>
            <CustomText style={{fontWeight: 'bold', color: Colors.red}}>Sắp đến hạn</CustomText>
        </View>;
    };

    const warningLate = () => {
        return <View style={styles.warning}>
            <CustomText style={{fontWeight: 'bold', color: Colors.red}}>Trễ hạn</CustomText>
        </View>;
    };

    const warningNotReturn = () => {
        return <View style={styles.warning}>
            <CustomText style={{fontWeight: 'bold', color: Colors.red}}>Không hoàn trả</CustomText>
        </View>;
    };

    const onCancel = () => {
        dispatch(updateOrder(itemData._id, StatusOrder.cancel));
        navigation.navigate(StackRoute.OrderTabTopNav);
    };

    const onPressItem = () => {
        navigation.navigate(StackRoute.DetailOrder, {
            data: itemData,
        });
    };

    const checkWarning = () => {
        if (itemData.status !== StatusOrder.rented) {
            return false;
        }
        const returnDate = new Date(itemData.product.endTime);
        const returnDate1 = returnDate.addDays(-3);
        const now = new Date();
        return (returnDate1 <= now && returnDate >= now);
    };

    const checkLateOrNotReturn = () => {
        return (itemData.status === StatusOrder.late || itemData.status === StatusOrder.notReturned);
    };

    const renderButtonArea = () => {
        if (itemData.status === StatusOrder.pending || itemData.status === StatusOrder.processing) {
            return <View style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
            }}>
                <CustomButton title='Hủy đơn' backgroundColor={Colors.red} onChooseOption={onCancel}/>
            </View>;
        } else if (((itemData.status === StatusOrder.returned))) {
            if (itemData.reviewProduct) {
                return (<View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                }}>
                    <CustomButton title='Đã đánh giá' backgroundColor={Colors.gray} disabled={true}/>
                </View>);
            } else {
                return (<View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                }}>
                    <CustomButton title='Đánh giá' backgroundColor={Colors.orange} onChooseOption={onReview}/>
                </View>);
            }
        }
    };

    return (
        checkObjectValid(item) ?
            <TouchableOpacity style={styles.container} onPress={onPressItem} disabled={disableDetailPress}>
                <View style={styles.mainContainer}>
                    <View style={styles.imageContainer}>
                        <Image style={styles.image}
                               source={itemData.product.thumbnail ? {uri: itemData.product.thumbnail} : require('../../../../assets/images/no-img.png')}/>
                    </View>
                    <View style={styles.infoContainer}>
                        <CustomText numberOfLines={1} style={styles.title}>{itemData.product.name}</CustomText>
                        <CustomText style={styles.title}>Mã đơn hàng: {itemData.product.productCode}</CustomText>
                        <CustomText numberOfLines={1}>{itemData.product.appearance}</CustomText>
                    </View>
                </View>
                <Divider style={styles.divider}/>
                <View style={styles.priceContainer}>
                    <CustomText>Giá thuê:</CustomText>
                    <CustomText style={styles.price}>{toConcurrency(itemData.product.rentPrice)} đ</CustomText>
                </View>
                <Divider style={styles.divider}/>
                <View style={styles.horizontalView}>
                    <CustomText style={styles.dateLabel}>Ngày thuê: </CustomText>
                    <CustomText>{dateToStringFormat(itemData.product.beginTime)}</CustomText>

                    <CustomText style={styles.dateLabel}> Giờ thuê: </CustomText>
                    <CustomText>{dateToTimeFortmat(itemData.product.beginTime)}</CustomText>
                </View>
                <View style={{...styles.horizontalView, marginBottom: Dimension.marginMedium}}>
                    <CustomText style={styles.dateLabel}>Ngày trả: </CustomText>
                    <CustomText>{dateToStringFormat(itemData.product.endTime)}</CustomText>

                    <CustomText style={styles.dateLabel}> Giờ trả: </CustomText>
                    <CustomText>{dateToTimeFortmat(itemData.product.endTime)}</CustomText>
                </View>
                {
                    renderButtonArea()
                }
                {
                    checkWarning() ?
                        warningOrder() :
                        itemData.status === StatusOrder.late ?
                            warningLate() : itemData.status === StatusOrder.notReturned ?
                            warningNotReturn() : null

                }
            </TouchableOpacity> : null
    );
};

const styles = StyleSheet.create({
    imageContainer: {
        width: Dimension.widthXSmall,
        height: Dimension.heightXSmall,
        margin: Dimension.marginSmall,
    },
    container: {
        margin: Dimension.marginSmall,
        backgroundColor: Colors.white,
        padding: Dimension.paddingSmall,
    },
    mainContainer: {
        flexDirection: 'row',
        backgroundColor: Colors.white,
        alignItems: 'center',
        margin: Dimension.marginSmall,
    },
    infoContainer: {
        flex: 1,
        margin: Dimension.marginSmall,
    },
    image: {
        width: '100%',
        height: '100%',
    },
    checkBox: {
        margin: 0,
        padding: 0,
    },
    checkBoxStyle: {},
    title: {
        fontWeight: FontStyle.bold,
    },
    numericInput: {
        alignSelf: 'center',
    },
    pickDate: {
        marginBottom: 15,
    },
    dateLabel: {
        fontWeight: 'bold',
    },
    horizontalView: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    divider: {
        borderWidth: 0.2,
        marginVertical: Dimension.marginSmall,
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    price: {
        color: 'red',
        marginHorizontal: Dimension.marginSmall,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    warning: {
        position: 'absolute',
        top: 5,
        right: 0,
        borderWidth: 2,
        borderRadius: 10,
        paddingHorizontal: Dimension.paddingMedium,
        paddingVertical: Dimension.paddingSmall,
        borderColor: Colors.red,
        transform: [
            {rotateY: '20deg'},
            {rotateZ: '20deg'},
        ],
    },
});

export default OrderItem;
