import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList, RefreshControl} from 'react-native';
import {getOrders} from '../../redux/features/ordersSlice';
import {StatusOrder} from '../../common/styles/common';
import OrderItem from './orderItem/orderItem';
import {useSelector} from 'react-redux';
import {checkObjectValid, wait} from '../../common/utils/utils';

const OrderReturned = (props) => {
    const [data, setData] = useState([]);
    const justUpdateOrder = useSelector(state => state.order.justUpdateOrder)
    const [isRefreshing, setIsRefreshing] = useState(false);
    useEffect(() => {
        setIsRefreshing(true);
            getOrders().then(result => {
                let dataTemp = []

                if (result != checkObjectValid(result)) {
                    result.forEach(item => {
                        if (item.status === StatusOrder.returned || item.status === StatusOrder.late) {
                            dataTemp.push(item)
                        }
                    })
                }
                setData(dataTemp)
            })
        wait(500).then(() => setIsRefreshing(false));
    },[justUpdateOrder])

    const onRefresh = () => {
        setIsRefreshing(true);
            getOrders().then(result => {
                let dataTemp = []
                if (result != checkObjectValid(result)) {
                    result.forEach(item => {
                        if (item.status === StatusOrder.returned || item.status === StatusOrder.late) {
                            dataTemp.push(item)
                        }
                    })
                }
                setData(dataTemp)
            })
        wait(500).then(() => setIsRefreshing(false));
    };

    const renderOrderItem = (itemData) => {
        return <OrderItem item={itemData.item}/>
    }

    return (
        <View style={styles.container}>
            <FlatList
                refreshControl={
                    <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh}/>
                }
                data={data}
                keyExtractor={item => item._id}
                renderItem={renderOrderItem}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
    }
})

export default OrderReturned;
