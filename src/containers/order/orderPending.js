import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity, RefreshControl} from 'react-native';
import OrderItem from './orderItem/orderItem';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, IconStyle, StatusOrder} from '../../common/styles/common';
import {getOrders} from '../../redux/features/ordersSlice';
import CustomText from '../../components/text/customText';
import {useSelector} from 'react-redux';
import {getCartsService} from '../../redux/features/cartSlice';
import {checkObjectValid, wait} from '../../common/utils/utils';

const OrderPending = (props) => {
    const [data, setData] = useState([]);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const justUpdateOrder = useSelector(state => state.order.justUpdateOrder)

    useEffect(() => {
        setIsRefreshing(true);
        getOrders().then(result => {
            let dataTemp = []
            if (result != checkObjectValid(result)) {
                result.forEach(item => {
                    if (item.status === StatusOrder.pending || item.status === StatusOrder.processing) {
                        dataTemp.push(item)
                    }
                })
            }
            setData(dataTemp)
        })
        wait(500).then(() => setIsRefreshing(false));
    },[justUpdateOrder])

    const onRefresh = () => {
        setIsRefreshing(true);
        getOrders().then(result => {
            let dataTemp = []
            if (result != checkObjectValid(result)) {
                result.forEach(item => {
                    if (item.status === StatusOrder.pending || item.status === StatusOrder.processing) {
                        dataTemp.push(item)
                    }
                })
            }
            setData(dataTemp)
        })
        wait(500).then(() => setIsRefreshing(false));
    };

    const renderOrderItem = (itemData) => {
        return <OrderItem item={itemData.item}/>
    }

    return (
        <View style={styles.container}>
            <FlatList
                refreshControl={
                    <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh}/>
                }
                data={data}
                keyExtractor={item => item._id}
                renderItem={renderOrderItem}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {},
});

export default OrderPending;
