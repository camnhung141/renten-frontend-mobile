import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView, Image} from 'react-native';
import {
    checkObjectEmpty,
    checkObjectValid,
    dateToStringFormat,
    dateToTimeFortmat,
    toConcurrency,
} from '../../../common/utils/utils';
import AddressItem from '../../location/addressItem';
import {
    Colors,
    Container,
    Dimension, DividerStyle,
    FontSize,
    FontStyle,
    IconStyle,
    StatusOrder, StatusOrderColor,
    StatusOrderMessage,
} from '../../../common/styles/common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomText from '../../../components/text/customText';
import {Divider} from 'react-native-elements';
import {getAgency} from '../../../redux/features/agenciesSlice';
import ShopItem from '../../../components/shop/shopItem';
import {getOrderDetail, updateOrder} from '../../../redux/features/ordersSlice';
import Timeline from 'react-native-timeline-flatlist';
import CustomButton from '../../../components/buttons/customButton';
import {useNavigation} from '@react-navigation/core';
import {StackRoute} from '../../../navigations/stackroute/stackRoute';
import OrderItem from '../orderItem/orderItem';

const DetailOrder = (props) => {
    const data = props.route.params.data;
    const [addressItem, setAddressItem] = useState({});
    const [paymentItem, setPaymentItem] = useState({});
    const [agencyInfo, setAgencyInfo] = useState({});
    const [dataItem, setDataItem] = useState({});
    const [historyData, setHistoryData] = useState([]);
    const navigation = useNavigation()


    useEffect(() => {
        getOrderDetail(data._id).then((result) => {
            setDataItem(result);
            setAddressItem({
                ...result.deliveryAddress,
                fullName: result.fullName,
                phoneNumber: result.phoneNumber,
                selected: true,
            });
            setPaymentItem({
                ...result.payment,
            });
            setAgencyInfo(result.agency);
            let historyTempData = [];
            for (const item in result.history) {
                const time = result.history[item];
                if (time) {
                    historyTempData.push({
                        time: dateToStringFormat(time) + '\n' + dateToTimeFortmat(time),
                        title: StatusOrderMessage[item],
                        circleColor: StatusOrderColor[item],
                        lineColor: StatusOrderColor[item],
                        titleStyle: {
                            color: StatusOrderColor[item],
                            fontWeight: 'normal',
                        },
                    });
                }
            }
            setHistoryData(historyTempData.reverse());
        });

    }, []);

    const onCancel = () => {
        dispatch(updateOrder(dataItem._id, StatusOrder.cancel))
    }

    const renderPaymentMethod = () => {
        //Todo check momo method later
        if (true) {
            return <View>
                <Image source={require('../../../../assets/images/logoMomo.png')} style={IconStyle.momo}/>
            </View>;
        } else {
            return <View>
                <View style={{
                    borderRadius: 25,
                    paddingHorizontal: Dimension.paddingMedium,
                    paddingVertical: Dimension.paddingSmall,
                    borderWidth: 1,
                    borderColor: Colors.red,
                    marginLeft: Dimension.marginMedium,
                }}>
                    <CustomText
                        style={{fontWeight: FontStyle.bold, color: Colors.red}}>{paymentItem.method}</CustomText>
                </View>
            </View>;
        }
    };

    return (
        <ScrollView style={styles.container}>
            {
                checkObjectEmpty(addressItem) ?
                    null :
                    <View style={Container.blockContainer}>
                        <AddressItem item={addressItem} showTitle={true}/>
                    </View>
            }

            {
                checkObjectEmpty(agencyInfo) ? null :
                    <View style={Container.blockContainer}>
                        <ShopItem info={agencyInfo}/>
                    </View>
            }

            {
                checkObjectValid(dataItem) ?
                <View style={Container.blockContainer}>
                    <OrderItem item={dataItem} disableDetailPress={true}/>
                </View> : null
            }

            <View style={{...styles.timeLineContainer, ...Container.blockContainer}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View>
                        <Icon name='history'
                              style={{...IconStyle.small, marginHorizontal: 0, paddingRight: 0}}
                              color={Colors.orange}/>
                    </View>
                    <CustomText style={{
                        marginLeft: Dimension.marginSmall,
                        fontSize: FontSize.medium,
                    }}>Trạng thái đơn hàng</CustomText>
                    {
                        checkObjectValid(dataItem.status) ?
                            <View style={{
                                borderRadius: 25,
                                paddingHorizontal: Dimension.paddingMedium,
                                paddingVertical: Dimension.paddingSmall,
                                borderWidth: 1,
                                borderColor: StatusOrderColor[dataItem.status],
                                marginLeft: Dimension.marginMedium
                            }}>
                                <CustomText style={{fontWeight: FontStyle.bold, color: StatusOrderColor[dataItem.status]}}>{StatusOrderMessage[dataItem.status]}</CustomText>
                            </View> : null
                    }
                </View>
                <Divider style={{marginVertical: Dimension.marginMedium}}/>
                <Timeline
                    data={historyData}
                    innerCircle={'dot'}
                />
            </View>

            <View style={styles.payMethodAndPriceContainer}>
                <View style={{...styles.paymentMethodContainer, ...styles.horizontalContainer}}>
                    <View style={{...styles.paymentMethodTitle, ...styles.horizontalContainer}}>
                        <Icon name='currency-usd' style={{
                            ...IconStyle.small,
                            marginHorizontal: 0,
                            paddingRight: Dimension.paddingSmall,
                        }}
                              color={Colors.red}/>
                        <CustomText>Phương thức thanh toán</CustomText>
                    </View>
                    {
                        renderPaymentMethod()
                    }
                </View>
                <Divider style={styles.divider}/>
                <View style={{...styles.horizontalContainer, ...styles.totalPriceContainer}}>
                    <CustomText style={styles.totalPriceLabel}>Số tiền thanh toán</CustomText>
                    <CustomText style={styles.totalPrice}>{toConcurrency(dataItem.totalPrice)} ₫</CustomText>
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.backgroundColorGray,
        paddingVertical: Dimension.paddingMedium,
    },
    horizon: {
        flexDirection: 'row',
    },
    price: {
        color: Colors.red,
    },
    label: {
        fontWeight: 'bold',
    },
    totalPrice: {
        color: Colors.red,
        fontWeight: 'bold',
        fontSize: FontSize.xmedium,
    },
    horizontalContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    paymentMethodContainer: {
        // justifyContent: 'space-between'
        marginBottom: Dimension.marginMedium,
    },
    paymentMethodTitle: {
        alignItems: 'center',
    },
    payMethodAndPriceContainer: {
        backgroundColor: Colors.white,
        padding: Dimension.paddingMedium,
        marginVertical: Dimension.marginSmall,
    },
    paymentMethod: {
        color: Colors.red,
        fontSize: FontSize.medium,
    },
    totalPriceContainer: {
        marginVertical: Dimension.marginSmall,
    },
    totalPriceLabel: {
        fontWeight: FontStyle.bold,
        fontSize: FontSize.medium,
    },
    timeLineContainer: {
        backgroundColor: Colors.white,
        paddingVertical: Dimension.paddingMedium,
        padding: Dimension.paddingMedium,
    },
    historyTitle: {
        fontSize: FontSize.medium,
        marginVertical: Dimension.marginMedium,
    },
});

export default DetailOrder;
