import React from 'react';
import {View, StyleSheet} from 'react-native';
import CustomText from '../../components/text/customText';
import CustomButton from '../../components/buttons/customButton';
import {Colors, Dimension, FontSize} from '../../common/styles/common';
import {StackRoute} from '../../navigations/stackroute/stackRoute';

const NonLoginScreen = (props) => {
    const navigation = props.navigation;
    const onLogin = () => {
        navigation.navigate(StackRoute.Login);
    };

    const onRegister = () => {
        navigation.navigate(StackRoute.Signup);
    };
    return (
        <View style={styles.containerNonLogin}>
            <CustomText style={styles.titleNonLogin}>Bạn chưa đăng nhập</CustomText>
            <View style={styles.buttonContainer}
            >
                <CustomButton
                    onChooseOption={onLogin}
                    style={styles.button}
                    title={'Đăng nhập'}
                    backgroundColor={Colors.orange}
                />
            </View>
            <CustomText>Hoặc</CustomText>
            <View style={styles.buttonContainer}
            >
                <CustomButton
                    onChooseOption={onRegister}
                    style={styles.button}
                    title={'Đăng ký'}
                    backgroundColor={Colors.blue}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    containerNonLogin: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleNonLogin: {
        fontWeight: 'bold',
        fontSize: FontSize.medium,
    },
    buttonContainer: {
        marginTop: Dimension.marginMedium,
        marginBottom: Dimension.marginSmall,
        width: '90%',
    },
});

export default NonLoginScreen;
