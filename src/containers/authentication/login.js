import React, {useContext, useEffect, useRef, useState} from 'react';
import {
    TextInput,
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
} from 'react-native';
import {ButtonStyle, InputStyle, Colors, Dimension} from '../../common/styles/common';
import CustomButton from '../../components/buttons/customButton';
import OutlineButton from '../../components/buttons/outlineButton';
import CustomInput from '../../components/customInput/customInput';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import CustomText from '../../components/text/customText';
import {useDispatch, useSelector} from 'react-redux';
import {loginService as loginAction, signUpService} from '../../redux/features/authSlice';
import Toast from 'react-native-toast-message';
import {checkEmail, checkPassword} from '../../common/utils/utils';


const Login = (props) => {
    const navigation = props.navigation;
    const dispatch = useDispatch();
    const isLogged = useSelector(state => state.auth.isLogged);

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isValidEmail, setValidEmail] = useState(true);
    const [isValidPassword, setValidPassword] = useState(true);
    const login = () => {
        setValidEmail(checkEmail(username));
        setValidPassword(checkPassword(password));

        if (isValidPassword && isValidEmail) {
            const info = {
                email: username,
                password: password,
            };
            dispatch(loginAction(info));
        } else {
            Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập thông tin hợp lệ',
            });
        }
    };

    useEffect(() => {
        if (isLogged) {
            Toast.show({
                type: 'success',
                text1: 'Đăng nhập thành công',
            });
            navigation.goBack();
        }
    }, [isLogged]);

    const register = () => {
        navigation.navigate(StackRoute.Signup);
    };

    return (<View style={{...styles.container, backgroundColor: Colors.background}}>
        {/*<Image*/}
        {/*    style={styles.logo}*/}
        {/*    source={*/}
        {/*        require('../../assets/images/logo.png')*/}
        {/*    }*/}
        {/*/>*/}
        <View
            style={styles.infoContainer}
        >
            <CustomText
                style={{...styles.text, color: Colors.textColor}}
            >
                Email
            </CustomText>
            <CustomInput
                //value={this.state.username}
                defaultValue={username}
                errorMessage={'Vui lòng nhập địa chỉ email hợp lệ'}
                isValid={isValidEmail}
                onChangeText={(username) => {
                    setValidEmail(checkEmail(username))
                    setUsername(username);
                }}
                placeholder={'Email address'}
                placeholderTextColor={Colors.subTextColor}
                style={{...InputStyle.input}}
            />
            <CustomText
                style={{...styles.text, color: Colors.textColor}}
            >
                Mật khẩu
            </CustomText>
            <CustomInput
                defaultValue={password}
                isValid={isValidPassword}
                errorMessage='Mật khẩu phải từ 8 ký tự trở lên, bao gồm 1 chữ thường, 1 chữ hoa, 1 số và 1 ký tự đặt biệt'
                onChangeText={(password) => {
                    setValidPassword(checkPassword(password))
                    setPassword(password);
                }}
                placeholder={'Password'}
                placeholderTextColor={Colors.subTextColor}
                isSecure={true}
                style={{...InputStyle.input}}
            />
            <View style={styles.button}
            >
                <CustomButton
                    title={'Đăng nhập'}
                    backgroundColor={Colors.orange}
                    onChooseOption={login}
                />
            </View>
            <View style={styles.button}
            >
                <OutlineButton
                    title={'Quên mật khẩu'}
                    onChooseOption={() => {
                    }}
                />
            </View>
            <View style={styles.button}
            >
                <CustomButton
                    title={'Đăng ký'}
                    backgroundColor={Colors.blue}
                    onChooseOption={register}
                />
            </View>
            <TouchableOpacity
                // style={Style.subButton}
                onPress={() => {
                    // props.navigation.navigate(Screen.Register, {item: props.item});
                }}
            >
                {/*<Text style={{color: '#0084be', fontWeight: 'bold'}}>{lang.SIGNUPFREE}</Text>*/}
            </TouchableOpacity>
        </View>
    </View>);

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: Colors.textColor,
        marginBottom: 5,
        width: '100%',
        alignItems: 'flex-start',
    },
    textButton: {
        color: 'white', fontWeight: 'bold',
    },
    button: {
        marginTop: Dimension.marginMedium,
        width: '90%',
    },
    forgotPassButton: {
        backgroundColor: Colors.transparent,
    },
    logo: {
        flex: 1,
        width: '90%',
        resizeMode: 'contain',
    },
    infoContainer: {
        // flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: Dimension.paddingMedium,
        width: '100%',
    },
});

export default Login;
