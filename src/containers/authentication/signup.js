import React, {useEffect, useRef, useState} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity, ScrollView,
} from 'react-native';
import {InputStyle, Colors, Dimension} from '../../common/styles/common';
import CustomButton from '../../components/buttons/customButton';
import CustomInput from '../../components/customInput/customInput';
import CustomText from '../../components/text/customText';
import {signUpService} from '../../redux/features/authSlice';
import {useDispatch, useSelector} from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';
import {Divider} from 'react-native-elements';
import Toast from 'react-native-toast-message';
import DatePickerDisableAble from '../../components/datePicker/datePickerDisable';
import CustomDatePicker from '../../components/datePicker/customDatePicker';
import {
    checkEmail,
    checkObjectValid,
    checkPassword,
    checkPhone,
    checkValidNotEmptyInput,
    wait,
} from '../../common/utils/utils';
import {editAddressInfo, updateAddressInfo} from '../../redux/features/locationSlice';

const SignUp = (props) => {
    const dispatch = useDispatch();
    const isSuccessSignUp = useSelector(state => state.auth.signUpSuccessFlag);

    const [signUpInfo, setSignUpInfo] = useState({
        fullName: '',
        email: '',
        password: '',
        confirmPassword: '',
        phoneNumber: '',
        gender: 'Nam',
        dateOfBirth: new Date(),
    });

    const [isValidName, setIsValidName] = useState(true);
    const [isValidEmail, setValidEmail] = useState(true);
    const [isValidPassword, setValidPassword] = useState(true);
    const [isValidConfirmPassword, setValidConfirmPassword] = useState(true);
    const [isValidPhoneNumber, setValidPhoneNumber] = useState(true);

    const isFirstRun = useRef(true);
    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }
        Toast.show({
            type: 'success',
            text1: 'Đăng ký tài khoản thành công!',
        });
        props.navigation.goBack();
    }, [isSuccessSignUp]);

    const setDateOfBirth = (date) => {
        setSignUpInfo({...signUpInfo, dateOfBirth: date});
    };

    const signup = () => {

        setIsValidName(checkValidNotEmptyInput(signUpInfo.fullName));
        setValidPhoneNumber(checkPhone(signUpInfo.phoneNumber));
        setValidEmail(checkEmail(signUpInfo.email));
        setValidPassword(checkPassword(signUpInfo.password));
        setValidConfirmPassword(signUpInfo.confirmPassword === signUpInfo.password);

        if (isValidName && isValidPhoneNumber && isValidConfirmPassword && isValidPassword && isValidEmail) {
            dispatch(signUpService(signUpInfo));
        } else {
            Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập thông tin hợp lệ',
            });
        }

    };

    return (<ScrollView contentContainerStyle={{...styles.container, backgroundColor: Colors.background}}>
        <CustomText
            style={{...styles.text, color: Colors.textColor}}
        >
            Họ và tên
        </CustomText>
        <CustomInput
            isValid={isValidName}
            errorMessage='Nhập họ và tên hợp lệ'
            isUpperCaseName={true}
            defaultValue={signUpInfo.fullName}
            onChangeText={(input) => {
                setIsValidName(checkValidNotEmptyInput(input));
                setSignUpInfo({...signUpInfo, fullName: input});
            }}
            placeholder={'Họ và tên'}
            placeholderTextColor={Colors.subTextColor}
            style={{...InputStyle.input}}
        />
        <CustomText
            style={{...styles.text, color: Colors.textColor}}
        >
            Email
        </CustomText>
        <CustomInput
            isValid={isValidEmail}
            errorMessage='Nhập email hợp lệ'
            defaultValue={signUpInfo.email}
            onChangeText={(input) => {
                setValidEmail(checkEmail(input));
                setSignUpInfo({...signUpInfo, email: input});
            }}
            placeholder={'Email'}
            placeholderTextColor={Colors.subTextColor}
            style={{...InputStyle.input}}
        />
        <CustomText
            style={{...styles.text, color: Colors.textColor}}
        >
            Mật khẩu
        </CustomText>
        <CustomInput
            isValid={isValidPassword}
            errorMessage='Mật khẩu phải từ 8 ký tự trở lên, bao gồm 1 chữ thường, 1 chữ hoa, 1 số và 1 ký tự đặt biệt'
            defaultValue={signUpInfo.password}
            onChangeText={(input) => {
                setValidPassword(checkPassword(input));
                setSignUpInfo({...signUpInfo, password: input});
            }}
            placeholder={'Mật khẩu'}
            placeholderTextColor={Colors.subTextColor}
            isSecure={true}
            style={{...InputStyle.input}}
        />
        <CustomText
            style={{...styles.text, color: Colors.textColor}}
        >
            Nhập lại mật khẩu
        </CustomText>
        <CustomInput
            isValid={isValidConfirmPassword}
            errorMessage='Phải trùng khớp với mật khẩu'
            isSecure={true}
            defaultValue={''}
            onChangeText={(input) => {
                setValidConfirmPassword(input === signUpInfo.password);
                setSignUpInfo({...signUpInfo, confirmPassword: input});
            }}
            placeholder={'Nhập lại mật khẩu'}
            placeholderTextColor={Colors.subTextColor}
            secureTextEntry={true}
            style={{...InputStyle.input}}
        />
        <CustomText
            style={{...styles.text, color: Colors.textColor}}
        >
            Số điện thoại
        </CustomText>
        <CustomInput
            isValid={isValidPhoneNumber}
            errorMessage='Số điện thoại không hợp lệ'
            defaultValue={''}
            onChangeText={
                (input) => {
                    setValidPhoneNumber(checkPhone(input));
                    setSignUpInfo({...signUpInfo, phoneNumber: input});
                }
            }
            placeholder={'Số điện thoại'}
            placeholderTextColor={Colors.subTextColor}
            style={{...InputStyle.input}}
            keyboardType='numeric'
        />
        <CustomText
            style={{...styles.text, color: Colors.textColor}}
        >
            Giới tính
        </CustomText>
        <DropDownPicker
            placeholder='Chọn giới tính'
            items={[
                {label: 'Nam', value: 'Nam'},
                {label: 'Nữ', value: 'Nữ'},
            ]}
            defaultValue={'Nam'}
            containerStyle={{...styles.genderInput}}
            onChangeItem={item => {
                setSignUpInfo({...signUpInfo, gender: item.value});
            }}
        />
        <CustomText
            style={{...styles.text, color: Colors.textColor}}
        >
            Ngày sinh
        </CustomText>
        <CustomDatePicker date={signUpInfo.dateOfBirth}
                          setDate={setDateOfBirth}
                          isInit={true}
                          style={styles.pickDate}
                          mode='date'/>
        {/*<DatePickerDisableAble date={signUpInfo.dateOfBirth} setDate={setDateOfBirth}*/}
        {/*                       mode='date'*/}
        {/*                       maxDate={new Date()}*/}
        {/*                       style={styles.pickDate}*/}
        {/*/>*/}
        <Divider style={{height: 1}}/>
        <View style={styles.button}
        >
            <CustomButton
                title={'Đăng ký'}
                backgroundColor={Colors.orange}
                onChooseOption={signup}
            />
        </View>
        <TouchableOpacity
            // style={Style.subButton}
            onPress={() => {
                // props.navigation.navigate(Screen.Register, {item: props.item});
            }}
        >
            {/*<Text style={{color: '#0084be', fontWeight: 'bold'}}>{lang.SIGNUPFREE}</Text>*/}
        </TouchableOpacity>
    </ScrollView>);

};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 40,
        paddingHorizontal: Dimension.paddingMedium,
    },
    text: {
        color: Colors.textColor,
        marginBottom: 5,
        width: '100%',
        alignItems: 'flex-start',
    },
    textButton: {
        color: 'white', fontWeight: 'bold',
    },
    button: {
        marginTop: Dimension.marginMedium,
        width: '90%',
    },
    forgotPassButton: {
        backgroundColor: Colors.transparent,
    },
    genderInput: {
        width: '90%',
        height: 44,
        marginBottom: 15,
    },
    pickDate: {
        marginBottom: 15,
    },
});

export default SignUp;
