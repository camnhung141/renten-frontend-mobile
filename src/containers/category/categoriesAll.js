import React from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity, ScrollView, Image} from 'react-native';
import CustomText from '../../components/text/customText';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, Dimension, FontSize} from '../../common/styles/common';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {useNavigation} from '@react-navigation/core';
import {checkObjectValid} from '../../common/utils/utils';

const CategoriesAll = (props) => {
    const data = props.route.params.data;
    const navigation = useNavigation();

    const searchByCategories = (item) => {
        let criteria = {
            keyword: '',
            page: 1,
        }
        if (checkObjectValid(item.parentId)) {
            criteria.subCategoriesId = item._id
        } else {
            criteria.categoriesId = item._id
        }
        navigation.push(StackRoute.SearchDetail, {
            criteria,
            categoryItem: item,
            name: item.name,
        });
    };

    const renderCategoryItem = (dataItem) => {
        const itemData = dataItem.item;
        return <TouchableOpacity style={styles.containerItem} onPress={() => {
            searchByCategories(itemData);
        }}>
            <View>
                <View style={styles.iconContainer}>
                    <Image style={styles.image} source={{uri: itemData.icon}}/>
                </View>
                <CustomText style={styles.name} numberOfLines={1}>{itemData.name}</CustomText>
            </View>
            <View>
                <Icon name='chevron-right' style={styles.iconRight} color={Colors.textColor}/>
            </View>
        </TouchableOpacity>;
    };

    return (
        <FlatList
            style={styles.container}
            data={data} renderItem={renderCategoryItem}
            keyExtractor={item => item._id}
        />
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Dimension.marginMedium
    },
    containerItem: {
        paddingHorizontal: Dimension.paddingSmall,
        paddingVertical: Dimension.paddingSmall,
        marginVertical: Dimension.marginSmall,
        backgroundColor: Colors.white,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    icon: {
        fontSize: FontSize.xxxxxlarge,
        marginHorizontal: Dimension.marginSmall,
    },
    iconContainer: {
    },
    name: {
        marginLeft: Dimension.marginSmall,
        color: Colors.textColor,
        // fontWeight: FontStyle.bold,
        fontSize: FontSize.medium,
        // borderWidth: 1
    },
    iconRight: {
        fontSize: FontSize.xxxxxlarge,
        marginHorizontal: Dimension.marginSmall,
    },
    image: {
        width: 60,
        height: 60,
        marginLeft: Dimension.marginMedium
    },
});

export default CategoriesAll;
