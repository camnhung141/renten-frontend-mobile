import React from 'react';
import {View, StyleSheet} from 'react-native';
import CustomText from '../../components/text/customText';
import {Colors, Dimension, DividerStyle, FontSize} from '../../common/styles/common';
import {Divider} from 'react-native-elements';

const PointInfoHelp = (props) => {
    return (
        <View style={styles.container}>
            <CustomText style={styles.title}>Điểm cá nhân là gì?</CustomText>
            <CustomText style={styles.infoGraph}>Là số điểm mà bạn sẽ được cập nhật mỗi lần thuê một sản phẩm, nó giống như số điểm để tính độ
                "uy tín" của bạn, giúp bạn có thể thuê những sản phẩm nhất định.</CustomText>
            <Divider style={styles.divider}/>
            <CustomText style={styles.title}>Điểm cá nhân được tính như thế nào?</CustomText>
            <CustomText style={styles.infoGraph}>Điểm cá nhân được tính trung bình qua các đánh giá của cửa hàng mỗi khi bạn thuê một sản phẩm của họ.</CustomText>
            <Divider style={styles.divider}/>
            <CustomText style={styles.title}>Cơ chế thuê hàng dựa theo điểm như thế nào?</CustomText>
            <CustomText style={styles.infoGraph}>Mỗi một sản phẩm sẽ được cửa hàng định mức một mức điểm tối thiểu nhất định, muốn thue e sản phẩm đó thì điểm cá nhân của bạn phải cao hơn hoặc bằng số điểm đã có.</CustomText>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: Dimension.paddingMedium
    },
    title: {
        fontSize: FontSize.xmedium,
        color: Colors.orange,
        fontWeight: 'bold',
    },
    infoGraph: {
        fontStyle: 'italic'
    },
    divider: {
        marginVertical: Dimension.marginMedium,
        borderWidth: 0.4
    }
});

export default PointInfoHelp;
