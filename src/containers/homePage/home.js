import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView, TouchableOpacity, RefreshControl, SafeAreaView} from 'react-native';
import {Colors, Dimension, FontSize, IconStyle} from '../../common/styles/common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Categories from '../../components/category/categories';
import ListProduct from '../../components/product/ListProduct/list-product';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {useDispatch, useSelector} from 'react-redux';
import {getCategoriesService} from '../../redux/features/categorySlice';
import {wait} from '../../common/utils/utils';
import {Divider, SearchBar} from 'react-native-elements';
import {SliderBox} from 'react-native-image-slider-box';
import CustomText from '../../components/text/customText';
import {getCartsService} from '../../redux/features/cartSlice';
import {getRefreshToken} from '../../storage/async-storage';
import {getUserInfo} from '../../redux/features/customerSlice';
import {setStatusLoginService} from '../../redux/features/authSlice';

const Home = (props) => {
    const navigation = props.navigation;
    const [searchText, setSearchText] = useState('');
    const isLogged = useSelector(state => state.auth.isLogged);
    const dispatch = useDispatch();
    const [isRefreshing, setIsRefreshing] = useState(false);
    const categories = useSelector(state => state.category.categories);
    const carts = useSelector(state => state.cart.carts);

    const openCartScreen = () => {
        navigation.navigate(StackRoute.Cart);
    };

    useEffect(() => {
        dispatch(getCategoriesService());
        dispatch(getCartsService());
    },[])

    const onRefresh = () => {
        setIsRefreshing(true);
        dispatch(getCartsService());
        dispatch(getCategoriesService());
        wait(100).then(() => setIsRefreshing(false));
    };

    const doSearch = () => {
        navigation.navigate(StackRoute.SearchDetail, {
            criteria: {
                keyword: searchText,
                categoriesId: '',
                page: 1,
                sort: 'createdAt:-1',
            },
        });
    };
    const refreshControl = () => {
        return <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh}/>;
    };

    const renderSliderAds = () => {
        return <View style={{marginVertical: Dimension.marginSmall}}>
            <SliderBox
                images={[require('../../../assets/images/slider/1.jpg'), require('../../../assets/images/slider/2.jpg'), require('../../../assets/images/slider/3.jpg')]}
                resizeMode='cover'
                resizeMethod='resize'
                onCurrentImagePressed={() => {
                    navigation.push(StackRoute.DetailProduct, {
                        id: '60de96a7a79358169cf50a6a',
                    });
                }}
                autoplay={true}
                circleLoop={true}
            />
        </View>;
    };

    return <SafeAreaView style={styles.container}>
        <View style={styles.searchBarContainer}>
            <View style={{flex: 1}}>
                <SearchBar
                    round={5}
                    placeholder='Tìm sản phẩm, danh mục...'
                    onChangeText={setSearchText}
                    inputContainerStyle={{backgroundColor: Colors.white, height: 35, alignContent: 'center'}}
                    value={searchText}
                    containerStyle={{
                        borderRadius: 5,
                        borderBottomColor: 'transparent',
                        borderTopColor: 'transparent',
                        backgroundColor: 'transparent',
                        padding: 0,
                    }}
                    onSubmitEditing={doSearch}
                />
            </View>
            <TouchableOpacity style={styles.cartIconContainer} onPress={openCartScreen}>
                {carts.length > 0 ? <CustomText style={{
                        color: Colors.white,
                        borderRadius: 25,
                        backgroundColor: Colors.red,
                        paddingVertical: 2,
                        paddingHorizontal: 5,
                        position: 'absolute',
                        right: -3,
                        top: -7,
                        zIndex: 1,
                    fontSize: 10
                    }}>{carts.length}</CustomText>
                    : null}
                <Icon name='cart-outline'
                      style={{...IconStyle.small}}
                      color={Colors.white}/>
            </TouchableOpacity>
        </View>
        <Divider style={styles.divider}/>
        <View style={styles.mainContainer}>
            {/*<View>*/}
            {/*    <ListProduct title='Gợi ý cho bạn'*/}
            {/*                 navigation={props.navigation}*/}
            {/*    />*/}
            {/*</View>*/}

            <View style={styles.blockContainer}>
                <ListProduct title='Gợi ý cho bạn'
                             navigation={navigation}
                             criteria={
                                 {
                                     keyword: '',
                                     categoriesId: '',
                                     page: 1,
                                     sort: 'createdAt:-1',
                                 }
                             }
                             refreshing={isRefreshing}
                             header={
                                 <View>
                                     {renderSliderAds()}
                                     <View style={styles.categories}>
                                         <Categories data={categories} navigation={props.navigation}/>
                                     </View>
                                 </View>
                             }
                             refreshControl={refreshControl()}
                />
            </View>
        </View>
    </SafeAreaView>;
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    searchBar: {
        height: Dimension.heightSmall,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    searchBarContainer: {
        backgroundColor: Colors.orange,
        paddingHorizontal: Dimension.paddingMedium,
        paddingTop: Dimension.paddingSmall,
        paddingBottom: Dimension.paddingSmall,
        flexDirection: 'row',
        alignItems: 'center',
    },
    cartIcon: {
        fontSize: FontSize.xlarge,
        marginHorizontal: Dimension.marginSmall,
    },
    cartIconContainer: {
        alignSelf: 'center',
    },
    mainContainer: {
        // padding: Dimension.paddingMedium
    },
    divider: {
        borderColor: Colors.white,
        borderWidth: 0.5,
    },

    blockContainer: {
        marginVertical: Dimension.marginSmall,
        marginBottom: Dimension.marginLarge
    },
});

export default Home;
