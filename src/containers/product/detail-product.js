import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {SliderBox} from 'react-native-image-slider-box';
import CustomText from '../../components/text/customText';
import {Dimension, FontSize, FontStyle, Colors, DividerStyle, IconStyle} from '../../common/styles/common';
import {Divider, Overlay, Rating} from 'react-native-elements';
import ListProduct from '../../components/product/ListProduct/list-product';
import ProductDescription from '../../components/product/productDescription';
import CustomButton from '../../components/buttons/customButton';
import {getAllProductsByProductTempId, getProductDetail} from '../../redux/features/productSlice';
import CommentList from '../../components/comment/commentList';
import {useSelector} from 'react-redux';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {addCartService} from '../../redux/features/cartSlice';
import ShopItem from '../../components/shop/shopItem';
import {getAgency} from '../../redux/features/agenciesSlice';
import {checkObjectEmpty, checkObjectValid, toConcurrency} from '../../common/utils/utils';
import CategoryItem from '../../components/category/CategoryItem';
import {useNavigation} from '@react-navigation/core';
import Toast from 'react-native-toast-message';
import DatePickerDisableAble from '../../components/datePicker/datePickerDisable';
import ImgZoom from '../../components/image/testImg';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const DetailProduct = ({route}) => {
    const navigation = useNavigation();
    const [itemTemplate, setItem] = useState(null);
    const isLogged = useSelector(state => state.auth.isLogged);
    const [dateRent, setDateRent] = useState(new Date());
    const [dateReturn, setDateReturn] = useState(new Date());
    const [agencyInfo, setAgencyInfo] = useState({});
    const [categories, setCategories] = useState({});
    const [typeProductList, setTypeProductList] = useState([]);
    const [itemProductTypeData, setItemProductTypeData] = useState({images: []});
    const [productTypeSelected, setProductypeSelected] = useState(false);
    const [isVisibleOverLay, setIsVisibleOverLay] = useState(false);
    const [visibleZoomImg, setVisibleZoomImg] = useState(false);

    useEffect(() => {
        const id = route.params.id;
        getProductDetail(id).then(item => {
            setItem(item);

            const itemUpdateDataTemp = {images: [item.thumbnail]};
            setItemProductTypeData({...itemProductTypeData, ...itemUpdateDataTemp});

            setCategories(item.categories.concat(item.subCategories));

            getAgency(item.agencyId).then(info => {
                setAgencyInfo(info);
            });
        });

        getAllProductsByProductTempId(id).then(products => {
            setTypeProductList(products.map(item => {
                return {...item, isSelected: false};
            }));
        });
    }, []);

    const point = useSelector(state => state.customer.point);

    const showAddCartSlide = () => {
        if (!isLogged) {
            navigation.navigate(StackRoute.Nonlogin);
            return;
        }
        const currentPoint = !checkObjectEmpty(point) ?
            point.pointAverage ? point.pointAverage : point : 0;

        if (currentPoint < itemTemplate.rentDetail.requiredPoint) {
            Toast.show({
                type: 'error',
                text1: 'Bạn không đủ điểm để thuê sản phẩm này',
                text2: `Sản phẩm yêu cầu (${itemTemplate.rentDetail.requiredPoint}) điểm nhưng tài khoản của bạn chỉ có (${currentPoint}) điểm`,
            });
        } else {
            setDateRent(null);
            setDateReturn(null);
            setIsVisibleOverLay(true);
        }
    };

    const addCart = () => {
        if (checkObjectValid(dateRent) && checkObjectValid(dateReturn)) {
            const request = {
                productId: itemProductTypeData._id,
                beginTime: dateRent,
                endTime: dateReturn,
            };
            addCartService({request}).then(() => {
                setIsVisibleOverLay(false);
                navigation.navigate(StackRoute.Cart);
            });
        } else {
            Toast.show({
                type: 'error',
                text1: 'Vui lòng nhập đầy đủ thời gian',
            });
        }
    };


    const renderBottomSheetContent = () => (
        itemTemplate ? <View style={styles.containerSlidingPanel}>
            {
                checkObjectValid(itemProductTypeData) ?
                    <View>
                        <CustomText
                            style={{fontWeight: FontStyle.bold}}>Mã: {itemProductTypeData.productCode}</CustomText>
                        <CustomText numberOfLines={1}>Tình trạng: {itemProductTypeData.appearance}</CustomText>
                        <CustomText
                            style={styles.rentPriceProductType}>{toConcurrency(itemProductTypeData.rentPrice)} ₫</CustomText>
                    </View> : null
            }
            <View style={styles.inLinePickDate}>
                <CustomText>Ngày bắt đầu thuê: </CustomText>
                <DatePickerDisableAble date={dateRent} setDate={setDateRent}
                                       mode={itemTemplate.rentDetail.unitRent === 'date' ? 'date' : null}
                                       isDefaultMinDate={true}
                                       orderedTime={itemProductTypeData.orderedTime}/>
            </View>
            <View style={styles.inLinePickDate}>
                <CustomText>Ngày trả: </CustomText>
                <DatePickerDisableAble date={dateReturn} setDate={setDateReturn} disable={!checkObjectValid(dateRent)}
                                       minDate={dateRent}
                                       mode={itemTemplate.rentDetail.unitRent === 'date' ? 'date' : null}
                                       orderedTime={itemProductTypeData.orderedTime}
                />
            </View>
            <View style={{width: '100%'}}>
            </View>

            <CustomButton disabled={!productTypeSelected} style={styles.slidingButton} title='Thêm'
                          onChooseOption={addCart}
                          backgroundColor={Colors.orange}/>
        </View> : null
    );

    const selectTypeItem = (value) => {
        setItemProductTypeData(value);
        setProductypeSelected(true);
        if (isVisibleOverLay) {
            setIsVisibleOverLay(false);
        }
        setTypeProductList(typeProductList.map(item => {
            item.selected = item._id === value._id;
            return item;
        }));
    };

    const openShopPage = () => {
        navigation.navigate(StackRoute.Shop, {
            name: agencyInfo.agencyName,
            agencyInfo: agencyInfo,
        });
    };

    const renderCategoryItem = (item) => {
        return <CategoryItem item={item.item} navigation={navigation} containerStyle={styles.containerCategoriesItem}/>;
    };

    const renderCategories = () => {
        return <View style={styles.categories}>
            <CustomText style={styles.titleBlock}>Danh mục</CustomText>
            <Divider style={DividerStyle.horizontal}/>
            <FlatList data={categories}
                      renderItem={renderCategoryItem}
                      keyExtractor={item => item._id}
                      horizontal={true}
            />
        </View>;
    };

    const renderItemType = (item, key) => {
        return <TouchableOpacity key={key}
                                 style={item.selected ? styles.itemTypeSelected : item.rented ? styles.itemTypeRented : styles.itemType}
                                 onPress={() => selectTypeItem(item)}>
            <CustomText style={{fontWeight: FontStyle.bold}}>{item.productCode}</CustomText>
            {/*<CustomText numberOfLines={1}>{item.appearance}</CustomText>*/}
            <CustomText style={styles.rentPriceProductType}>{item.rentPrice} ₫</CustomText>
        </TouchableOpacity>;
    };

    const renderTypeList = () => {
        return <View style={styles.typeListContainer}>
            <CustomText style={styles.titleBlock}>Các loại sản phẩm</CustomText>
            <Divider style={DividerStyle.horizontal}/>
            <View style={styles.typeListScrollContainer}>
                <ScrollView nestedScrollEnabled={true}>
                    <View style={styles.typeListScroll}>
                        {
                            typeProductList.map(renderItemType)
                        }
                    </View>
                </ScrollView>
            </View>
        </View>;
    };

    return (
        <View style={styles.container}>
            {
                itemTemplate ?
                    <ScrollView style={styles.container}>
                        <View style={{backgroundColor: Colors.black}}>
                            <SliderBox images={itemProductTypeData.images}
                                       resizeMode='contain'
                                       resizeMethod='resize'
                                       onCurrentImagePressed={() => {
                                           setVisibleZoomImg(true);
                                       }}
                            />
                        </View>

                        <View style={{...styles.infoProduct, ...styles.blockContainer}}>
                            <View
                                style={styles.rating}
                            >
                                <Rating
                                    ratingBackgroundColor={Colors.transparent}
                                    readonly
                                    imageSize={25}
                                    ratingCount={5}
                                    startingValue={itemTemplate.rate.rateAverage}
                                />
                                <CustomText style={{fontSize: FontSize.xxsmall}}> ( {itemTemplate.rate.rateCount} đánh
                                    giá )</CustomText>
                            </View>
                            <CustomText style={styles.title}>{itemTemplate.name}</CustomText>
                            <View style={{flexDirection: 'row', alignItems: 'center', flex: 1, flexWrap: 'wrap'}}>
                                <CustomText style={{fontStyle: 'italic'}}>(Để thuê được sản phẩm này, bạn cần phải
                                    đạt </CustomText>
                                <CustomText style={{
                                    color: Colors.red,
                                    fontSize: FontSize.medium,
                                }}>({itemTemplate.rentDetail.requiredPoint})</CustomText>
                                <CustomText style={{fontStyle: 'italic'}}>điểm trở lên)</CustomText>
                            </View>
                            {
                                productTypeSelected ?
                                    <View>
                                        <CustomText
                                            style={styles.title}>Mã: {itemProductTypeData.productCode}</CustomText>
                                        <View style={{marginVertical: Dimension.marginSmall}}>
                                            <CustomText>Trạng thái: {itemProductTypeData.appearance}</CustomText>
                                            <TouchableOpacity style={{
                                                borderWidth: 1, borderColor: Colors.orange,
                                                flexDirection: 'row', width: '50%', alignItems: 'center',
                                                justifyContent: 'center', paddingVertical: Dimension.paddingSmall,
                                                marginVertical: Dimension.marginSmall,
                                            }}
                                            onPress={() => {
                                                navigation.navigate(StackRoute.Appearances, {id: itemProductTypeData._id})
                                            }}>
                                                <Icon name='history'
                                                      style={{...IconStyle.small, marginHorizontal: 0, paddingRight: 0}}
                                                      color={Colors.orange}/>
                                                <CustomText style={{color: Colors.orange}}> Xem lịch sử trạng thái</CustomText>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    : null

                            }
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                {
                                    productTypeSelected ?
                                        <CustomText style={styles.rentPrice}>Giá
                                            thuê: {toConcurrency(itemProductTypeData.rentPrice)} ₫</CustomText> :
                                        <CustomText style={styles.rentPrice}>Giá
                                            thuê: {toConcurrency(itemTemplate.rentPrice)} ₫</CustomText>
                                }
                                <View style={{
                                    borderWidth: 1,
                                    padding: Dimension.paddingSmall,
                                    marginLeft: Dimension.marginMedium,
                                    borderColor: Colors.orange,
                                    borderStyle: 'dashed',
                                    borderRadius: 3,
                                }}>
                                    <CustomText style={{color: Colors.orange}}>Thuê
                                        theo {itemTemplate.rentDetail.unitRent === 'date' ? 'ngày' : 'giờ'}</CustomText>
                                </View>
                            </View>
                            <CustomText style={styles.price}>Giá
                                gốc: {toConcurrency(itemTemplate.originalPrice)} ₫</CustomText>
                            <CustomButton title={productTypeSelected ? 'Thêm vào giỏ hàng' : 'Vui lòng chọn 1 sản phẩm'}
                                          onChooseOption={showAddCartSlide}
                                          icon='cart-outline'
                                          iconColor={Colors.white}
                                          backgroundColor={Colors.orange} disabled={!productTypeSelected}/>
                        </View>
                        <View style={styles.blockContainer}>
                            {
                                renderTypeList()
                            }
                        </View>
                        <View style={styles.blockContainer}>
                            {
                                checkObjectEmpty(agencyInfo) ? null :
                                    <ShopItem onPress={openShopPage} info={agencyInfo}/>
                            }
                        </View>
                        <View style={styles.blockContainer}>
                            {
                                checkObjectEmpty(categories) ? null :
                                    renderCategories()
                            }
                        </View>

                        <View style={styles.blockContainer}>
                            <ProductDescription data={itemTemplate.description}/>
                        </View>
                        <CommentList productTemplateId={itemTemplate._id} productId={itemProductTypeData._id}
                                     style={styles.blockContainer}/>
                        <View style={styles.blockContainer}>
                            <ListProduct title='Sản phẩm tương tự' horizontal={true}
                                         criteria={
                                             {
                                                 keyword: '',
                                                 categoriesId: itemTemplate.categoriesId[0],
                                                 page: 1,
                                             }
                                         }
                            />
                        </View>

                        {agencyInfo ? <View style={styles.blockContainer}>
                            <ListProduct title='Các sản phẩm cùng cửa hàng' horizontal={true}
                                         criteria={
                                             {
                                                 keyword: '',
                                                 categoriesId: '',
                                                 page: 1,
                                                 agencyId: itemTemplate.agencyId,
                                             }
                                         }
                            />
                        </View> : null}

                        {/*<View style={styles.blockContainer}>*/}
                        {/*    <ListProduct title='Có thể bạn cũng thích' horizontal={false}*/}
                        {/*                 navigation={navigation}*/}
                        {/*                 data={dataTest}*/}
                        {/*    />*/}
                        {/*</View>*/}
                        {itemTemplate ?
                            <ImgZoom data={itemProductTypeData.images} setIsVisible={setVisibleZoomImg}
                                     visible={visibleZoomImg}/> : null}
                    </ScrollView>
                    : null
            }
            <Overlay isVisible={isVisibleOverLay} onBackdropPress={() => {
                setIsVisibleOverLay(false);
            }}>
                {renderBottomSheetContent()}
            </Overlay>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColorGray,
    },
    containerSlidingPanel: {
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    infoProduct: {
        padding: Dimension.paddingMedium,
        backgroundColor: Colors.white,
    },
    title: {
        fontSize: FontSize.large,
        fontWeight: FontStyle.bold,
        marginVertical: Dimension.marginSmall,
    },
    price: {
        color: Colors.yellow,
        fontSize: FontSize.xmedium,
        marginVertical: Dimension.marginSmall,
        // textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
    },
    rentPrice: {
        color: Colors.red,
        fontSize: FontSize.large,
        marginVertical: Dimension.marginSmall,
    },
    rentPriceProductType: {
        color: Colors.red,
        marginVertical: Dimension.marginSmall,
    },
    rating: {
        alignSelf: 'flex-start',
        marginVertical: Dimension.marginSmall,
        marginBottom: Dimension.marginMedium,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        width: '100%', alignItems: 'center',
    },
    blockContainer: {
        marginVertical: Dimension.marginSmall,
    },
    inLinePickDate: {
        flexDirection: 'row',
        marginTop: Dimension.marginMedium,
        marginBottom: Dimension.marginMedium,
        alignItems: 'center',
    },
    slidingButton: {
        marginBottom: Dimension.marginSmall,
        width: '95%',
    },
    numericInput: {
        marginBottom: Dimension.marginLarge,
    },
    categories: {
        // padding: Dimension.paddingMedium,
        backgroundColor: Colors.white,
        paddingHorizontal: Dimension.paddingMedium,
    },
    containerCategoriesItem: {
        borderColor: Colors.lighterOrange,
        borderWidth: 1,
        marginRight: Dimension.marginMedium,
    },
    typeListContainer: {
        backgroundColor: Colors.white,
        paddingHorizontal: Dimension.paddingMedium,
        paddingBottom: Dimension.paddingMedium,
    },
    titleBlock: {
        fontSize: FontSize.xmedium,
        marginVertical: Dimension.marginMedium,
    },
    typeListScroll: {
        // flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
        padding: Dimension.paddingSmall,
    },
    typeListScrollContainer: {
        height: 150,
        borderWidth: 0.2,
        backgroundColor: Colors.white,
        marginTop: Dimension.marginMedium,
    },
    itemType: {
        // backgroundColor: Colors.lighterGray,
        // backgroundColor: Colors.lighterGray,
        borderRadius: 5,
        padding: Dimension.paddingSmall,
        margin: Dimension.marginSmall,
        borderWidth: 1,
        borderColor: Colors.gray,
    },
    itemTypeRented: {
        // backgroundColor: Colors.lighterGray,
        borderRadius: 5,
        padding: Dimension.paddingSmall,
        margin: Dimension.marginSmall,
        borderWidth: 1,
        borderColor: Colors.orange,
    },
    itemTypeSelected: {
        backgroundColor: Colors.lighterGreen,
        borderRadius: 5,
        padding: Dimension.paddingSmall,
        margin: Dimension.marginSmall,
        borderWidth: 1.5,
        borderColor: Colors.green,
    },
});

export default DetailProduct;
