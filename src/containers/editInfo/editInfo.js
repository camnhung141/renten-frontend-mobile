import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import CustomInput from '../../components/customInput/customInput';
import CustomButton from '../../components/buttons/customButton';
import {Colors, Dimension, FontSize} from '../../common/styles/common';
import DropDownPicker from 'react-native-dropdown-picker';
import {useDispatch} from 'react-redux';
import {updatePropertyUserInfo} from '../../redux/features/customerSlice';
import DatePickerDisableAble from '../../components/datePicker/datePickerDisable';
import CustomDatePicker from '../../components/datePicker/customDatePicker';

const EditInfo = (props) => {
    const oldValue = props.route.params.oldValue;
    const onSave = props.route.params.onSave;
    const navigation = props.navigation;
    const typeEdit = props.route.params.typeEdit;
    const userUpdateProperty = props.route.params.userUpdateProperty;

    const dispatch = useDispatch();

    const [val, setVal] = useState(oldValue);

    const renderInput = () => {
        switch (typeEdit) {
            case TypeEdit.TEXT:
                return <CustomInput defaultValue={oldValue} onChangeText={setVal}
                                    style={styles.input}/>;
            case TypeEdit.NUMBER:
                return <CustomInput defaultValue={oldValue} onChangeText={setVal}
                                    style={styles.input}
                                    keyboardType='numeric'
                />;
            case TypeEdit.GENDER:
                return <DropDownPicker
                    placeholder='Chọn giới tính'
                    items={[
                        {label: 'Nam', value: 'Nam'},
                        {label: 'Nữ', value: 'Nữ'},
                    ]}
                    defaultValue={oldValue}
                    containerStyle={{...styles.genderInput}}
                    onChangeItem={item => {
                        setVal(item.value);
                    }}
                />;
            case TypeEdit.DATE:
                return <CustomDatePicker date={val} setDate={setVal} pickDateTextStyle={styles.pickDateTextStyle} mode='date'/>;

        }
    };

    return (
        <View style={styles.container}>
            {
                renderInput()
            }
            <CustomButton onChooseOption={() => {
                let updateObj = {};
                if (typeEdit === TypeEdit.DATE)
                    updateObj[userUpdateProperty] = val.toString();
                else updateObj[userUpdateProperty] = val
                dispatch(updatePropertyUserInfo(updateObj))
                onSave(val);
                navigation.goBack();
            }}
                          title={'Lưu'}
                          backgroundColor={Colors.blue}
                          style={styles.styleBtn}/>
            <CustomButton onChooseOption={() => {
                navigation.goBack();
            }}
                          title={'Hủy'}
                          backgroundColor={Colors.orange}
                          style={styles.styleBtn}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: Dimension.paddingMedium
    },
    input: {
    },
    styleBtn: {
        marginTop: Dimension.marginMedium,
        width: '50%',
    },
    genderInput: {
        width: '90%',
        height: 44,
        marginBottom: 15,
    },
    pickDateTextStyle: {
        fontSize: FontSize.large
    }
});


export default EditInfo;

export const TypeEdit = {
    TEXT: 'text',
    DATE: 'date',
    GENDER: 'gender',
    NUMBER: 'number'
};
