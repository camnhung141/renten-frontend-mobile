import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import CustomButton from '../../components/buttons/customButton';
import {updatePropertyUserInfo} from '../../redux/features/customerSlice';
import {Colors, Dimension, FontSize, FontStyle} from '../../common/styles/common';
import {TypeEdit} from './editInfo';
import {useNavigation} from '@react-navigation/core';
import CustomInput from '../../components/customInput/customInput';
import CustomText from '../../components/text/customText';
import {checkValidNotEmptyInput} from '../../common/utils/utils';
import {useDispatch} from 'react-redux';
import {updatePassword} from '../../redux/features/authSlice';
import Toast from 'react-native-toast-message';


const ChangePassword = () => {
    const [oldPass, setOldPass] = useState();
    const [newPass, setNewPass] = useState();
    const [retypeNewPass, setRetypeNewPass] = useState();
    const navigation = useNavigation();

    const [isValidOldPass, setIsValidOldPass] = useState(true);
    const [isValidNewPass, setIsValidNewPass] = useState(true);
    const [isValidRetypeNewPass, setIsValidRetypeNewPass] = useState(true);

    const onSubmit = () => {
        setIsValidOldPass(checkValidNotEmptyInput(oldPass));
        setIsValidNewPass(checkValidNotEmptyInput(newPass));
        setIsValidRetypeNewPass(checkValidNotEmptyInput(retypeNewPass));

        if (newPass !== retypeNewPass) {
            setIsValidNewPass(false);
            setIsValidRetypeNewPass(false);
            Toast.show({
                type: 'error',
                text1: 'Mật khẩu không khớp',
            })
        } else if (isValidRetypeNewPass && isValidNewPass && isValidOldPass) {
            updatePassword(oldPass, newPass, retypeNewPass).then(() => {
                    navigation.goBack();
                    Toast.show({
                        type: 'success',
                        text1: 'Đổi mật khẩu thành công',
                    })
                },
            );
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.labelContainer}>
                <CustomText style={styles.label} isRequired={true}>Nhập mật khẩu hiện tại:</CustomText>
            </View>
            <CustomInput defaultValue={oldPass} onChangeText={setOldPass} isSecure={true} isValid={isValidOldPass}
                         style={styles.input}/>
            <View style={styles.labelContainer}>
                <CustomText style={styles.label} isRequired={true}>Nhập mật khẩu mới:</CustomText>
            </View>
            <CustomInput defaultValue={newPass} onChangeText={setNewPass} isSecure={true} isValid={isValidNewPass}
                         style={styles.input}/>
            <View style={styles.labelContainer}>
                <CustomText style={styles.label} isRequired={true}>Nhập lại mật khẩu mới:</CustomText>
            </View>
            <CustomInput defaultValue={retypeNewPass} onChangeText={setRetypeNewPass} isSecure={true}
                         isValid={isValidRetypeNewPass}
                         style={styles.input}/>
            <CustomButton onChooseOption={() => {
                onSubmit();
            }}
                          title={'Thay đổi mật khẩu'}
                          backgroundColor={Colors.blue}
                          style={styles.styleBtn}/>
            <CustomButton onChooseOption={() => {
                navigation.goBack();
            }}
                          title={'Hủy'}
                          backgroundColor={Colors.orange}
                          style={styles.styleBtn}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: Dimension.paddingMedium
    },
    input: {
    },
    styleBtn: {
        marginTop: Dimension.marginMedium,
        width: '50%',
    },
    label: {
        marginBottom: Dimension.marginSmall,
        fontSize: FontSize.medium,
    },
    labelContainer: {
        justifyContent: 'flex-start',
        width: '100%',
    },
});

export default ChangePassword;
