import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView, FlatList, RefreshControl, Image} from 'react-native';
import ProductCartItem from './productCartItem';
import {CheckBox, Divider} from 'react-native-elements';
import CustomText from '../../components/text/customText';
import CustomButton from '../../components/buttons/customButton';
import {Colors, Dimension, FontSize, ImageStyle, PaymentStatus} from '../../common/styles/common';
import {useDispatch, useSelector} from 'react-redux';
import {getCartsService} from '../../redux/features/cartSlice';
import {wait} from '../../common/utils/utils';
import {StackRoute} from '../../navigations/stackroute/stackRoute';

const ProductCart = (props) => {
    const cartData = useSelector(state => state.cart.carts);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const isFetchedData = useSelector(state => state.cart.isUpdatedCart);
    const dispatch = useDispatch();

    useEffect(() => {
        setIsRefreshing(true);
        dispatch(getCartsService());
        wait(500).then(() => setIsRefreshing(false));
    }, [isFetchedData]);


    const renderCartItem = (item) => {
        return <ProductCartItem item={item.item}/>;
    };

    const onRefresh = () => {
        setIsRefreshing(true);
        dispatch(getCartsService());
        wait(500).then(() => setIsRefreshing(false));
    };

    const renderSeparator = () => {
        return <Divider style={styles.divider}/>
    }

    const onRent = () => {
        props.navigation.navigate(StackRoute.Payment)
    }

    return <View style={styles.container}>
        {
            cartData.length === 0 ?
                <View style={styles.empty}>
                                <View>
                                    <Image
                                           source={require('../../../assets/images/empty-cart.png')}/>
                                </View>
                                <View>
                                    <CustomText
                                        style={{fontSize: FontSize.large,
                                            color: Colors.gray,
                                            marginLeft: Dimension.marginMedium
                                        }}>Giỏ hàng trống</CustomText>
                                </View>
                </View>
                : <View>
                    <ScrollView style={styles.contentContainer}
                                refreshControl={
                                    <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh}/>
                                }>
                        <FlatList
                            style={styles.flatListCart}
                            data={cartData}
                            renderItem={renderCartItem}
                            extraData={isFetchedData}
                            ItemSeparatorComponent={renderSeparator}
                        />
                    </ScrollView>
                    <View style={styles.bottomArea}>
                        <CustomButton title='Thuê hàng' onChooseOption={onRent} backgroundColor={Colors.orange} style={styles.button} isNotRound={true} disabled={cartData.length === 0}/>
                    </View>
                </View>

        }
    </View>;
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
    },
    empty: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentContainer: {
        backgroundColor: Colors.backgroundColorGray,
    },
    checkAllContainer: {
        flexDirection: 'row',
    },
    checkBox: {
        margin: 0,
        padding: 0,
    },
    bottomArea: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        borderTopWidth: Dimension.borderSmall,
        borderColor: Colors.gray,
    },
    button: {
        padding: Dimension.paddingMedium,
    },
    flatListCart: {
        backgroundColor: Colors.white,
    },
    divider: {
        marginVertical: Dimension.marginSmall
    }
});

export default ProductCart;
