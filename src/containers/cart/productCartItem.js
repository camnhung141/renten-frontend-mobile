import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import {Image} from 'react-native';
import {CheckBox, Rating} from 'react-native-elements';
import {Colors, Dimension, FontSize, FontStyle, IconStyle} from '../../common/styles/common';
import CustomText from '../../components/text/customText';
import NumericInput from 'react-native-numeric-input';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import CustomDatePicker from '../../components/datePicker/customDatePicker';
import {deleteCarts, updateCartItem} from '../../redux/features/cartSlice';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {checkObjectEmpty, checkObjectValid, toConcurrency} from '../../common/utils/utils';
import DatePickerDisableAble from '../../components/datePicker/datePickerDisable';

const ProductCartItem = ({item}) => {

    const navigation = useNavigation();

    const openProduct = () => {
        navigation.push(StackRoute.DetailProduct, {
            id: item.templateId,
            name: item.template.name,
            productSelectedId: item.productId
        });
    };

    const [beginTime, setBeginTime] = useState(new Date(Date.parse(item.beginTime)));
    const [endTime, setEndTime] = useState(new Date(Date.parse(item.endTime)));

    const dispatch = useDispatch();

    useEffect(() => {
        setBeginTime(new Date(Date.parse(item.beginTime)));
        setEndTime(new Date(Date.parse(item.endTime)));
    }, [item]);

    const updateBeginTime = (beginTimeUpdate) => {
        setBeginTime(beginTimeUpdate);
        dispatch(
            updateCartItem({
                productId: item.productId,
                beginTime: beginTimeUpdate,
                endTime: endTime,
                quantity: 1
            }),
        );
    };

    const updateEndTime = (endTimeUpdate) => {
        setEndTime(endTimeUpdate);
        dispatch(
            updateCartItem({
                productId: item.productId,
                beginTime: beginTime,
                endTime: endTimeUpdate,
                quantity: 1
            }),
        );
    };

    const deleteCart = () => {
        dispatch(
            updateCartItem({
                productId: item.productId,
                beginTime: beginTime,
                endTime: endTime,
                quantity: 0
            }),
        );
    }

    return <View style={styles.container}>
        <View style={styles.mainContainer}>
            <TouchableOpacity style={styles.imageContainer} onPress={openProduct}>
                <Image style={styles.image}
                       source={item.product.thumbnail ? {uri: item.product.thumbnail} : require('../../../assets/images/no-img.png')}/>
            </TouchableOpacity>
            <View style={styles.infoContainer}>
                <CustomText numberOfLines={2} style={styles.title}>{item.template.name}</CustomText>
                <CustomText numberOfLines={2} style={styles.title}>{item.product.productCode}</CustomText>
                <CustomText numberOfLines={2}>{item.product.appearance}</CustomText>
                <View style={styles.horizontalPrice}>
                    <CustomText>Giá thuê: </CustomText>
                    <CustomText
                        style={styles.price}>{toConcurrency(item.product.rentPrice)} đ</CustomText>
                </View>
            </View>
            <TouchableOpacity onPress={deleteCart}>
                <Icon name='delete-forever' color={Colors.orange} style={IconStyle.small}/>
            </TouchableOpacity>
        </View>
        <View style={styles.horizontalView}>
            <CustomText style={styles.dateLabel}>Ngày thuê: </CustomText>
            <DatePickerDisableAble date={beginTime}
                                   setDate={updateBeginTime}
                                   style={styles.pickDate}
                                   mode={item.template.rentDetail.unitRent === 'date' ? 'date' : null}
                                   minDate={new Date()}
                                   orderedTime={item.product.orderedTime}
                                   isDefaultMinDate={true}
            />

        </View>
        <View style={styles.horizontalView}>
            <CustomText style={styles.dateLabel}>Ngày trả: </CustomText>
            <DatePickerDisableAble date={endTime}
                                   setDate={updateEndTime}
                                   style={styles.pickDate}
                                   minDate={beginTime}
                                   orderedTime={item.product.orderedTime}
                                   mode={item.template.rentDetail.unitRent === 'date' ? 'date' : null}
            />
        </View>
    </View>;
};

const styles = StyleSheet.create({
    imageContainer: {
        width: Dimension.widthNormal,
        height: Dimension.heightNormal,
    },
    container: {
        margin: Dimension.marginSmall,
    },
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: Dimension.marginSmall,
    },
    infoContainer: {
        flex: 1,
        margin: Dimension.marginSmall,
    },
    image: {
        width: '100%',
        height: '100%',
    },
    checkBox: {
        margin: 0,
        padding: 0,
    },
    checkBoxStyle: {},
    title: {
        fontWeight: FontStyle.bold,
    },
    numericInput: {
        alignSelf: 'center',
    },
    pickDate: {
        // marginBottom: 15,
    },
    dateLabel: {
        fontWeight: 'bold',
    },
    horizontalView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: Dimension.marginSmall
    },
    horizontalPrice: {
        flexDirection: 'row',
        // justifyContent: 'flex-end',
    },
    price: {
        color: Colors.orange,
        fontWeight: 'bold',
    },
    discountAndReview: {
        marginTop: Dimension.marginMedium,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    discount: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    iconTicket: {
        fontSize: FontSize.xlarge,
        marginRight: Dimension.marginSmall,
    },
});

export default ProductCartItem;
