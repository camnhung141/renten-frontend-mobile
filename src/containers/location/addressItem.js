import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import CustomText from '../../components/text/customText';
import {Colors, Dimension, FontSize, IconStyle} from '../../common/styles/common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {checkObjectValid} from '../../common/utils/utils';
import {Divider} from 'react-native-elements';
import {useNavigation} from '@react-navigation/core';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {deleteAddressInfo} from '../../redux/features/locationSlice';
import {useDispatch} from 'react-redux';

const AddressItem = ({item, onPressAddressItem, deleteAble, editAble}) => {

    const navigation = useNavigation()
    const dispatch = useDispatch()

    const openEditAddress = () => {
        navigation.navigate(StackRoute.AddLocation,  {
            isUpdate: true,
            data: item
        })
    }

    const deleteAddress = (item) => {
        dispatch(deleteAddressInfo(item._id, item.accountId))
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.containerHorizontal} onPress={onPressAddressItem} disabled={!onPressAddressItem}>
                <View>
                    {
                        (checkObjectValid(item.selected) && item.selected) ?
                            <View>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <View>
                                        <Icon name='truck-fast-outline'
                                              style={{...IconStyle.small, marginHorizontal: 0, paddingRight: 0}}
                                              color={Colors.orange}/>
                                    </View>
                                    <CustomText style={styles.title}>Địa chỉ nhận hàng</CustomText>
                                </View>
                                <Divider style={{marginVertical: Dimension.marginSmall}}/>
                            </View>
                         : null
                    }
                    <CustomText style={styles.name}>{item.fullName}</CustomText>
                    <CustomText>Số điện thoại: {item.phoneNumber}</CustomText>
                    <CustomText>{item.streetAndNumber}, {item.wardName}</CustomText>
                    <CustomText>{item.districtName}, {item.cityName}</CustomText>
                </View>
                {/*{*/}
                {/*    onPressAddressItem ?*/}
                {/*        <Icon name='chevron-right' style={{...IconStyle.normal, marginHorizontal: 0, paddingRight: 0}}*/}
                {/*              color={Colors.gray}/> : null*/}
                {/*}*/}
                <View style={{position: 'absolute', right: 0, top: 0, flexDirection: 'row'}} >
                    {
                        ! deleteAble ? null :
                            <TouchableOpacity onPress={() => deleteAddress(item)}>
                                <Icon name='delete-forever' color={Colors.orange} style={IconStyle.small}/>
                            </TouchableOpacity>
                    }
                    {
                        ! editAble ? null :
                            <TouchableOpacity onPress={openEditAddress}>
                                <Icon name='circle-edit-outline'
                                      style={{...IconStyle.small, marginHorizontal: 0, paddingRight: 0}}
                                      color={Colors.orange}/>
                            </TouchableOpacity>
                    }
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        padding: Dimension.paddingMedium,
        backgroundColor: Colors.white,
    },
    name: {
        fontWeight: 'bold',
        marginBottom: Dimension.marginSmall,
    },
    icon: {
        alignSelf: 'flex-start',
    },
    containerHorizontal: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    title: {
        marginLeft: Dimension.marginSmall,
        fontSize: FontSize.medium,
        color: Colors.textColor
    }
});

export default AddressItem;
