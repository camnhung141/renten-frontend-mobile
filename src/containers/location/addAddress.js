import React, {useEffect, useRef, useState} from 'react';
import {View, StyleSheet, ScrollView, Button} from 'react-native';
import CustomInput from '../../components/customInput/customInput';
import CustomText from '../../components/text/customText';
import {Colors, Dimension, Input} from '../../common/styles/common';
import {useDispatch, useSelector} from 'react-redux';
import {
    editAddressInfo,
    loadCitiesService,
    loadDistrictsService,
    loadWardService,
    updateAddressInfo,
} from '../../redux/features/locationSlice';
import DropDownPicker from 'react-native-dropdown-picker';
import {checkObjectEmpty, setValidDropdownPicker, checkValidNotEmptyInput} from '../../common/utils/utils';
import {Switch} from 'react-native-elements';
import CustomButton from '../../components/buttons/customButton';
import {useNavigation} from '@react-navigation/core';
import {setSelectedLog} from 'react-native/Libraries/LogBox/Data/LogBoxData';
import Toast from 'react-native-toast-message';

const AddAddress = (props) => {
    let isUpdateMode = false;
    let data = {};
    let isInitDefault = false;
    if (props.route.params) {
        isUpdateMode = props.route.params.isUpdate;
        data = props.route.params.data;
        isInitDefault = props.route.params.isDefault;
    }
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const citiesData = useSelector(state => state.location.cities);
    const districtsData = useSelector(state => state.location.districts);
    const wardsData = useSelector(state => state.location.wards);
    const addressList = useSelector(state => state.location.addressList);


    const [city, setCity] = useState({});
    const [district, setDistrict] = useState({});
    const [ward, setWard] = useState({});
    const [fullName, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [streetAndNumber, setStreetAndNumber] = useState('');
    const [isDefault, setIsDefault] = useState(false);
    const [request, setRequest] = useState({
        phoneNumber: '',
        selected: false,
        streetAndNumber: '',
        wardName: '',
        wardCode: '',
        wardPrefix: '',
        districtName: '',
        districtCode: '',
        districtPrefix: '',
        cityName: '',
        cityCode: '',
        cityPrefix: '',
        fullName: '',
    });

    useEffect(() => {
        dispatch(loadCitiesService());
        if (isUpdateMode) {
            updateName(data.fullName);
            updateNumber(data.phoneNumber);
            updateIsDefault(data.selected);
            updateStreetAndNumber(data.streetAndNumber);
            setRequest({
                ...request,
                fullName: data.fullName,
                phoneNumber: data.phoneNumber,
                selected: data.selected,
                streetAndNumber: data.streetAndNumber,
            });
        }
    }, []);

    const isFirstRun = useRef(true)
    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }
        Toast.show({
            type: 'success',
            text1: 'Cập nhật địa chỉ thành công',
        });
        navigation.goBack()
    }, [addressList])

    const mapCitiesDropdown = () => {
        const result = citiesData.map(item => {
            return {label: item.cityName, value: item};
        });
        result.unshift({label: 'Chọn Tỉnh/ Thành phố', value: {}});
        return result;
    };

    const mapDistrictsDropdown = () => {
        const result = districtsData.map(item => {
            return {label: item.districtName, value: item};
        });
        result.unshift({label: 'Chọn Quận/ Huyện', value: {}});
        return result;
    };

    const mapWardsDropdown = () => {
        const result = wardsData.map(item => {
            return {label: item.wardName, value: item};
        });
        result.unshift({label: 'Chọn Phường/ Xã', value: {}});
        return result;
    };

    const onSelectCity = (cityItem) => {
        setIsValidCity(checkValidNotEmptyInput(cityItem.value));
        setCity(cityItem.value);
        setRequest({
            ...request,
            cityCode: cityItem.value.cityCode,
            cityName: cityItem.value.cityName,
            cityPrefix: cityItem.value.cityPrefix,
        });
        dispatch(loadDistrictsService(cityItem.value.cityCode));
        setDistrict({});
        setWard({});
    };

    const onSelectDistrict = (districtItem) => {
        setIsValidDistrict(checkValidNotEmptyInput(districtItem.value));
        setDistrict(districtItem.value);
        setRequest({
            ...request,
            districtCode: districtItem.value.districtCode,
            districtName: districtItem.value.districtName,
            districtPrefix: districtItem.value.districtPrefix,
        });
        dispatch(loadWardService(districtItem.value.districtCode));
        setWard({});
    };

    const onSelectWard = (wardItem) => {
        setIsValidWard(checkValidNotEmptyInput(wardItem.value));
        setWard(wardItem.value);
        setRequest({
            ...request,
            wardCode: wardItem.value.wardCode,
            wardName: wardItem.value.wardName,
            wardPrefix: wardItem.value.wardPrefix,
        });
    };

    const [isValidName, setIsValidName] = useState(true);
    const [isValidStreetAndNumber, setIsValidStreetAndNumber] = useState(true);
    const [isValidCity, setIsValidCity] = useState(true);
    const [isValidDistrict, setIsValidDistrict] = useState(true);
    const [isValidWard, setIsValidWard] = useState(true);
    const [isValidPhone, setIsValidPhone] = useState(true);

    const updateName = (text) => {
        setIsValidName(checkValidNotEmptyInput(text));
        setName(text);
        setRequest({...request, fullName: text});
    };

    const updateNumber = (text) => {
        setIsValidPhone(checkValidNotEmptyInput(text));
        setRequest({...request, phoneNumber: text});
        setPhone(text);
    };

    const updateStreetAndNumber = (text) => {
        setIsValidStreetAndNumber(checkValidNotEmptyInput(text));
        setRequest({...request, streetAndNumber: text});
        setStreetAndNumber(text);
    };

    const updateIsDefault = (value) => {
        setIsDefault(value);
        setRequest({...request, selected: value});
    };

    const onSaveAddress = () => {
        setIsValidName(checkValidNotEmptyInput(fullName));
        setIsValidPhone(checkValidNotEmptyInput(phone));
        setIsValidDistrict(checkValidNotEmptyInput(district));
        setIsValidCity(checkValidNotEmptyInput(city));
        setIsValidWard(checkValidNotEmptyInput(ward));
        setIsValidStreetAndNumber(checkValidNotEmptyInput(streetAndNumber));
        if (isValidCity && isValidDistrict && isValidPhone && isValidStreetAndNumber && isValidWard) {
            if (isUpdateMode) {
                dispatch(editAddressInfo(request, data._id));
            } else {
                dispatch(updateAddressInfo(request));
            }
        }
    };

    return (
        <ScrollView>
            <View style={styles.container}>
                <CustomText style={styles.text} isRequired={true}>Họ và tên</CustomText>
                <CustomInput placeholder='Họ và tên' isValid={isValidName} onChangeText={updateName}
                             isUpperCaseName={true}
                             errorMessage='Nhập họ và tên hợp lệ' defaultValue={fullName}/>
                <CustomText style={styles.text} isRequired={true}>Số điện thoại</CustomText>
                <CustomInput placeholder='Số điện thoại' isValid={isValidPhone} onChangeText={updateNumber}
                             errorMessage='Nhập sđt hợp lệ' defaultValue={phone}/>
                <CustomText style={styles.text} isRequired={true}>Tỉnh/ Thành phố</CustomText>
                <DropDownPicker
                    placeholder='Chọn Tỉnh/ Thành phố'
                    items={mapCitiesDropdown()}
                    containerStyle={{...styles.dropdownInput}}
                    onChangeItem={onSelectCity}
                    style={{...isValidCity ? Input.validInput : Input.invalidInput}}
                />
                <CustomText style={styles.text} isRequired={true}>Quận/ Huyện</CustomText>
                <DropDownPicker
                    onChangeItem={onSelectDistrict}
                    placeholder='Chọn Quận/ Huyện'
                    items={mapDistrictsDropdown()}
                    containerStyle={{...styles.dropdownInput}}
                    style={{...isValidDistrict ? Input.validInput : Input.invalidInput}}
                />
                <CustomText style={styles.text} isRequired={true}>Phường/ Xã</CustomText>
                <DropDownPicker
                    placeholder='Chọn Phường/ Xã'
                    items={mapWardsDropdown()}
                    containerStyle={{...styles.dropdownInput}}
                    onChangeItem={onSelectWard}
                    style={{...isValidWard ? Input.validInput : Input.invalidInput}}
                />
                <CustomText style={styles.text} isRequired={true}>Số địa chỉ</CustomText>
                <CustomInput placeholder='Số địa chỉ' isValid={isValidStreetAndNumber}
                             onChangeText={updateStreetAndNumber} errorMessage='Nhập số địa chỉ hợp lệ'
                             defaultValue={streetAndNumber}
                />
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginTop: Dimension.marginMedium,
                }}>
                    <CustomText style={{
                        marginRight: Dimension.marginLarge,
                    }}>Đặt làm địa chỉ mặc định</CustomText>
                    <Switch
                        trackColor={{false: '#767577', true: '#81b0ff'}}
                        thumbColor={isDefault ? Colors.orange : '#f4f3f4'}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={updateIsDefault}
                        value={isInitDefault ? true : isDefault}
                        disabled={isInitDefault}
                    />
                </View>

                <CustomButton title={'Lưu địa chỉ'} onChooseOption={onSaveAddress} backgroundColor={Colors.orange}
                              style={styles.saveBtn}/>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 40,
        paddingHorizontal: Dimension.paddingMedium,
    },
    text: {
        marginBottom: 5,
        width: '100%',
        alignItems: 'flex-start',
    },
    dropdownInput: {
        width: '90%',
        height: 44,
        marginBottom: 15,
    },
    saveBtn: {
        marginTop: Dimension.marginLarge,
        width: '90%',
    },
});

export default AddAddress;
