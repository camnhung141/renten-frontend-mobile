import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList, RefreshControl} from 'react-native';
import AddressItem from './addressItem';
import {Colors, Dimension} from '../../common/styles/common';
import CustomButton from '../../components/buttons/customButton';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {
    editAddressInfo,
    getAddressInfoList,
} from '../../redux/features/locationSlice';
import {checkObjectEmpty, wait} from '../../common/utils/utils';
import {useDispatch, useSelector} from 'react-redux';

const Location = (props) => {

    const addressList = useSelector(state => state.location.addressList);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAddressInfoList())
    }, [])

    const onRefresh = () => {
        setIsRefreshing(true);
        dispatch(getAddressInfoList())
        wait(500).then(() => setIsRefreshing(false));
    };

    const openAddLocation = () => {
        props.navigation.navigate(StackRoute.AddLocation, {
            isDefault: checkObjectEmpty(addressList)
        })
    }

    const onSelected = (item) => {
        const requestItem = {...item.item}
        requestItem.selected = true;
        delete requestItem._id
        delete requestItem.__v
        delete requestItem.accountId
        dispatch(editAddressInfo(requestItem, item.item._id))
    }

    const renderLocationItem = (item) => {
        return <View style={styles.locationItem}>
            <AddressItem item={item.item} onPressAddressItem={() => {
                onSelected(item)
            }}
                         deleteAble={true}
                         editAble={true}
            />
        </View>
    }

    const renderAddButton = () => {
        return <CustomButton style={styles.addButton} title={'Thêm địa chỉ mới'} backgroundColor={Colors.orange} onChooseOption={openAddLocation}/>
    }

    return (
        <View style={styles.container}>
            <FlatList
                refreshControl={
                    <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh}/>
                }
                data={addressList} renderItem={renderLocationItem}
                keyExtractor={item => item._id}
                ListFooterComponent={renderAddButton}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.lightGray
    },
    locationItem: {
        backgroundColor: Colors.white,
        marginVertical: Dimension.marginSmall
    },
    addButton: {
        marginTop: Dimension.marginMedium
    }
});

export default Location;
