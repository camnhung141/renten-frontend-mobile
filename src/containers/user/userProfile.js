import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity, ImageBackground, ScrollView} from 'react-native';
import {CameraSetting, Colors, Dimension, IconStyle, TypeImage} from '../../common/styles/common';
import SettingButton from '../../components/buttons/settingButton';
import {useDispatch, useSelector} from 'react-redux';
import {Divider} from 'react-native-elements';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {getUserInfo, sendUpdateUserInfo, updatePropertyUserInfo} from '../../redux/features/customerSlice';
import {checkObjectEmpty, dateToStringFormat} from '../../common/utils/utils';
import {TypeEdit} from '../editInfo/editInfo';
import TopRightButton from '../../components/topButton/topRightButton';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {launchImageLibrary} from 'react-native-image-picker';
import {postFormDataImages} from '../../api/api';
import CustomButton from '../../components/buttons/customButton';

const UserProfile = (props) => {

    const userInfo = useSelector(state => state.customer.userInfo);
    const updateInfo = useSelector(state => state.customer.updateInfo);
    const justChangeInfo = useSelector(state => state.customer.justChangeInfo);
    const dispatch = useDispatch();

    const [fullName, setFullName] = useState(userInfo.fullName);
    const [email, setEmail] = useState(userInfo.email);
    const [phoneNumber, setPhoneNumber] = useState(userInfo.phoneNumber);
    const [gender, setGender] = useState(userInfo.gender);
    const [dateOfBirth, setDateOfBirth] = useState(new Date(Date.parse(userInfo.dateOfBirth)));
    const [updateInf, setUpdateInf] = useState({});
    const [avtTempUri, setAvtTempUri] = useState('../../assets/images/no-img.png')
    const [isAvatarUpdated, setIsAvatarUpdated] = useState(false);

    const openGallery = () => {
        launchImageLibrary(CameraSetting.image, (response) => {
            if (!response.didCancel) {
                const form = new FormData();
                const newImg = {
                    uri: response.uri,
                    type: response.type,
                    name: response.fileName,
                };
                setAvtTempUri(response.uri)
                form.append('images', newImg);
                postFormDataImages(form, TypeImage.AVATAR).then((response) => {
                    const avatar = response.data.payload[0];
                    dispatch(updatePropertyUserInfo({avatar: avatar}))
                    setIsAvatarUpdated(true)
                }).catch((err) => {
                });
            }
        });
    };

    useEffect(() => {
        setFullName(userInfo.fullName);
        setPhoneNumber(userInfo.phoneNumber);
        setEmail(userInfo.email);
        setGender(userInfo.gender);
        setDateOfBirth(new Date(Date.parse(userInfo.dateOfBirth)));
    }, [justChangeInfo]);

    useEffect(() => {
        if (checkObjectEmpty(userInfo)) {
            dispatch(getUserInfo());
        }
    }, []);

    const editPhoneNumber = () => {
        setUpdateInf(true);
        props.navigation.navigate(StackRoute.EditInfo, {
            onSave: setPhoneNumber,
            oldValue: phoneNumber,
            typeEdit: TypeEdit.NUMBER,
            userUpdateProperty: 'phoneNumber',
        });
    };

    const editName = () => {
        setUpdateInf(true);
        props.navigation.navigate(StackRoute.EditInfo, {
            onSave: setFullName,
            oldValue: fullName,
            typeEdit: TypeEdit.TEXT,
            userUpdateProperty: 'fullName',
        });
    };

    const editGender = () => {
        setUpdateInf(true);
        props.navigation.navigate(StackRoute.EditInfo, {
            onSave: setGender,
            oldValue: gender,
            typeEdit: TypeEdit.GENDER,
            userUpdateProperty: 'gender',
        });
    };

    const editBirthDate = () => {
        setUpdateInf(true);
        props.navigation.navigate(StackRoute.EditInfo, {
            onSave: setDateOfBirth,
            oldValue: dateOfBirth,
            typeEdit: TypeEdit.DATE,
            userUpdateProperty: 'dateOfBirth',
        });
    };

    const sendUpdateInfo = () => {
        const dataUpdate = {...userInfo, ...updateInfo}
        dispatch(sendUpdateUserInfo(dataUpdate));
    };

    // React.useLayoutEffect(() => {
    //     props.navigation.setOptions({
    //         headerRight: () => (
    //             <TopRightButton icon='check' onPress={sendUpdateInfo}/>
    //         ),
    //     });
    // }, [props.navigation]);

    return (
        <View style={styles.container}>
            {
                userInfo ?
                    <View style={styles.blockContainer}>
                        <ImageBackground style={styles.avatarProfileEdit}
                                         imageStyle={{borderRadius: 25}}
                                         source={isAvatarUpdated ? {uri: avtTempUri} : userInfo.avatar ? {uri: userInfo.avatar} : require('../../../assets/images/no-img.png')}>
                            <TouchableOpacity onPress={openGallery}>
                                <Icon name='pencil-outline'
                                      style={{...IconStyle.small, ...styles.icon}}/>
                            </TouchableOpacity>
                        </ImageBackground>
                        <SettingButton title='Họ và tên' rightTitle={fullName} onChooseOption={editName}/>
                        <Divider/>
                        <SettingButton title='Email' rightTitle={email} disabled={true}/>
                        <Divider/>
                        <SettingButton title='Số điện thoại' rightTitle={phoneNumber} onChooseOption={editPhoneNumber}/>
                        <Divider/>
                        <SettingButton title='Giới tính' rightTitle={gender} onChooseOption={editGender}/>
                        <Divider/>
                        <SettingButton title='Ngày sinh' rightTitle={dateToStringFormat(dateOfBirth)}
                                       onChooseOption={editBirthDate}/>
                    </View> : null
            }

            <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
                <CustomButton
                    onChooseOption={sendUpdateInfo}
                    disabled={checkObjectEmpty(updateInfo)}
                    title={'Lưu'}
                    backgroundColor={Colors.blue}
                    style={styles.styleBtn}/>
            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    blockContainer: {
        marginVertical: Dimension.marginMedium,
    },
    avatarContainer: {
        alignContent: 'center',
    },
    icon: {
        color: Colors.orange,
        backgroundColor: Colors.white,
        borderRadius: 50,
        padding: 2,
        alignSelf: 'flex-end',
    },
    avatarProfileEdit: {
        width: 65,
        height: 65,
        borderRadius: 25,
        margin: Dimension.marginSmall,
        justifyContent: 'flex-end',
        alignSelf: 'center',
    },
});

export default UserProfile;
