import React, {useEffect, useState} from 'react';
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    SafeAreaView,
    RefreshControl,
    Dimensions,
} from 'react-native';
import {
    Colors,
    Dimension,
    FontSize,
    IconStyle,
    ImageStyle, VerifyStatus,
    VerifyStatusColor,
    VerifyStatusMessage,
} from '../../common/styles/common';
import CustomButton from '../../components/buttons/customButton';
import OutlineButton from '../../components/buttons/outlineButton';
import {useDispatch, useSelector} from 'react-redux';
import CustomText from '../../components/text/customText';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SearchBar from 'react-native-search-bar';
import SettingButton from '../../components/buttons/settingButton';
import {checkObjectEmpty, wait} from '../../common/utils/utils';
import {getUserInfo} from '../../redux/features/customerSlice';
import {logOutService} from '../../redux/features/authSlice';
import {Divider} from 'react-native-elements';
import {getCategoriesService} from '../../redux/features/categorySlice';
import {getCartsService} from '../../redux/features/cartSlice';

const UserMainPage = (props) => {

    const navigation = props.navigation;
    const [isInfoLoaded, setIsInfoLoaded] = useState(false);
    const dispatch = useDispatch();
    const [isRefreshing, setIsRefreshing] = useState(false);
    const carts = useSelector(state => state.cart.carts);

    useEffect(() => {
        dispatch(getCartsService());
        dispatch(getUserInfo())
    },[])

    const onRefresh = () => {
        setIsRefreshing(true);
        setIsInfoLoaded(true);
        dispatch(getCartsService());
        dispatch(getUserInfo())
        wait(100).then(() => setIsRefreshing(false));
    };

    const openCartScreen = () => {
        navigation.navigate(StackRoute.Cart);
    };

    const openProfileScreen = () => {
        navigation.navigate(StackRoute.UserProfile);
    };

    const openLocationScreen = () => {
        navigation.navigate(StackRoute.Location);
    };

    const openOrderScreen = () => {
        navigation.navigate(StackRoute.OrderTabTopNav);
    };

    const openChangePassScreen = () => {
        navigation.navigate(StackRoute.ChangePassword);
    };

    const onLogout = () => {
        dispatch(logOutService());
    };

    const openVerifyScreen = () => {
        navigation.navigate(StackRoute.Verify);
    }

    const isLogged = useSelector(state => state.auth.isLogged);
    const userInfo = useSelector(state => state.customer.userInfo);
    const accountStatus = useSelector(state => state.customer.accountStatus)
    const point = useSelector(state => state.customer.point)
    const justChangeInfo = useSelector(state => state.customer.justChangeInfo);

    useEffect(() => {
        setIsInfoLoaded(true);
        if (checkObjectEmpty(userInfo)) {
            dispatch(getUserInfo())
        }
    }, []);

    const checkDisableVerify = () => {
        return ((accountStatus.status === VerifyStatus.verified) || (accountStatus.status === VerifyStatus.locked) || (accountStatus.status === VerifyStatus.blocked))
    }

    const openPointHelp = () => {
        navigation.navigate(StackRoute.PointInfoHelp);
    }

    const contentHeight = Dimensions.get('window').height

    return (
        <View style={{...styles.container}}>
            <ScrollView
                        refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={onRefresh}/>}
            >
                    <View style={styles.headerContainer}>
                        <View style={styles.settingArea}>
                            <TouchableOpacity style={styles.IconContainer} onPress={openCartScreen}>
                                {carts.length > 0 ? <CustomText style={{
                                        color: Colors.white,
                                        borderRadius: 25,
                                        backgroundColor: Colors.red,
                                        paddingVertical: 2,
                                        paddingHorizontal: 5,
                                        position: 'absolute',
                                        right: -3,
                                        top: -7,
                                        zIndex: 1,
                                        fontSize: 10
                                    }}>{carts.length}</CustomText>
                                    : null}
                                <Icon name='cart-outline'
                                      style={{...IconStyle.small}}
                                      color={Colors.white}/>
                            </TouchableOpacity>
                            {/*<TouchableOpacity style={styles.IconContainer}*/}
                            {/*>*/}
                            {/*    <Icon name='chat-processing-outline'*/}
                            {/*          style={{...IconStyle.small}}*/}
                            {/*          color={Colors.white}/>*/}
                            {/*</TouchableOpacity>*/}
                        </View>
                    </View>
                    <View style={styles.infoContainer}>
                        <View>
                            {/*<Image style={styles.} source={userInfo.avatar ? {uri: userInfo.avatar} : require('../../assets/images/no-img.png')}/>*/}
                            <Image style={ImageStyle.avatar}
                                   source={userInfo.avatar ? {uri: userInfo.avatar} : require('../../../assets/images/no-img.png')}/>
                        </View>
                        {isInfoLoaded ?
                            <View>
                                <CustomText style={styles.name}>{userInfo.fullName}</CustomText>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <CustomText style={{color: Colors.textColor}} >Số điểm của bạn: </CustomText>
                                    {
                                        !checkObjectEmpty(point) ?
                                            <CustomText>{point.pointAverage ? point.pointAverage : point}</CustomText> : null
                                    }
                                    <TouchableOpacity style={styles.IconContainer} onPress={openPointHelp}>
                                        <Icon name='help-circle-outline'
                                              style={{...IconStyle.small}}
                                              color={Colors.orange}/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            : null}
                    </View>
                    <View style={styles.blockContainer}>
                        <SettingButton title='Chi tiết thông tin cá nhân' icon='account-cog-outline'
                                       onChooseOption={openProfileScreen}/>
                        <Divider style={styles.divider}/>
                        <SettingButton title='Địa chỉ' icon='square-edit-outline'
                                       onChooseOption={openLocationScreen}/>
                        <Divider style={styles.divider}/>
                        <SettingButton title={VerifyStatusMessage[accountStatus.status]} icon='card-account-details-outline'
                                       onChooseOption={openVerifyScreen} colorIcon={ userInfo.accountId ? VerifyStatusColor[accountStatus.status] : null}
                                       disabled={checkDisableVerify()}
                        />
                        <Divider style={styles.divider}/>
                        <SettingButton title='Quản lý đơn thuê' icon='shopping-outline'
                                       onChooseOption={openOrderScreen}/>
                        <Divider style={styles.divider}/>
                        <SettingButton title='Thay đổi mật khẩu' icon='lock-outline'
                                       onChooseOption={openChangePassScreen}/>
                    </View>
            </ScrollView>
            <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
                <CustomButton title='Đăng xuất' backgroundColor={Colors.red} isNotRound={true}
                              style={styles.logOutBtn} onChooseOption={onLogout}/>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.lightGray,
    },
    buttonContainer: {
        marginTop: Dimension.marginMedium,
        marginBottom: Dimension.marginSmall,
        width: '90%',
    },
    containerNonLogin: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleNonLogin: {
        fontWeight: 'bold',
        fontSize: FontSize.medium,
    },
    headerContainer: {
        backgroundColor: Colors.orange,
        height: Dimension.navigationBarHeight,
    },
    settingArea: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // paddingTop: Dimension.paddingMedium,
        paddingRight: Dimension.paddingMedium,
        // paddingVertical: Dimension.paddingMedium,
        alignItems: 'center',
        height: Dimension.navigationBarHeight,
    },
    cartIcon: {
        fontSize: FontSize.xlarge,
        marginHorizontal: Dimension.marginSmall,
    },
    IconContainer: {
        alignSelf: 'center',
    },
    infoContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        // marginHorizontal: Dimension.marginMedium,
        // marginVertical: Dimension.marginXSmall,
        width: '100%',
        backgroundColor: Colors.white,
        paddingVertical: Dimension.paddingMedium,
        paddingLeft: Dimension.paddingMedium,
    },
    name: {
        // fontWeight: 'bold',
        fontSize: FontSize.medium,
        // color: Colors.black,
    },
    blockContainer: {
        marginVertical: Dimension.marginMedium,
    },
    logOutBtn: {
        marginTop: 50,
        // position: 'absolute',
        // bottom: 0
    },
    divider: {
        // borderWidth: 0.3
    },
    buttonsContainer: {},
});

export default UserMainPage;
