import React, {useState} from 'react';
import {View, StyleSheet, ImageBackground, TouchableOpacity, Dimensions, ScrollView, Linking} from 'react-native';
import CustomText from '../../components/text/customText';
import CustomInput from '../../components/customInput/customInput';
import DropDownPicker from 'react-native-dropdown-picker';
import {
    CameraSetting,
    Colors,
    Dimension,
    DividerStyle,
    FontSize,
    FontStyle,
    IconStyle,
    Input,
} from '../../common/styles/common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomButton from '../../components/buttons/customButton';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {postFormDataImages} from '../../api/api';
import {updatePropertyUserInfo} from '../../redux/features/customerSlice';
import ImagePicker from 'react-native-image-crop-picker';
import CustomDatePicker from '../../components/datePicker/customDatePicker';
import {Divider} from 'react-native-elements';
import {checkObjectValid} from '../../common/utils/utils';
import {reviewProduct} from '../../redux/features/productSlice';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {getImageLink} from '../../redux/features/imageService';
import {verify} from '../../redux/features/authSlice';
import {useNavigation} from '@react-navigation/core';
import DatePickerDisableAble from '../../components/datePicker/datePickerDisable';
import Toast from 'react-native-toast-message';


const VerifyAccount = (props) => {

    const [cmndFront, setCmndFront] = useState('');
    const [cmndBack, setCmndBack] = useState('');
    const [isFrontUpdated, setIsFrontUpdated] = useState(false);
    const [isBackUpdated, setIsBackUpdated] = useState(false);
    const [date, setDate] = useState(null);
    const [faceImg, setFaceImg] = useState({});
    const [fullName, setFullName] = useState();
    const [identityNumber, setIdentityNumber] = useState();
    const [placeOfIssue, setPlaceOfIssue] = useState();
    const navigation = useNavigation()

    const openGalleryCrop = (setUri, isUpdated) => {
        launchImageLibrary(CameraSetting.image, (response) => {
            if (!response.didCancel) {
                // const form = new FormData();
                const newImg = {
                    uri: response.uri,
                    type: response.type,
                    name: response.fileName,
                };
            }
        });
    };

    const openImgAndCrop = (setImg, isUpdated) => {
        ImagePicker.openPicker({
            width: cmndWidth,
            height: cmndHeight,
            cropping: true,
        }).then(image => {
            setImg(image);
            isUpdated(true);
        });
    };

    const takeImage = () => {
        launchCamera(CameraSetting.image, (response) => {
            if (!response.didCancel) {
                setFaceImg(response);
            }
        });
    };

    const cmndWidth = Dimensions.get('window').width - Dimension.paddingMedium * 3;
    const cmndHeight = cmndWidth * Dimension.percentHeightItemCmnd / Dimension.percentWidthItemCmnd;

    const onSendInfo = async () => {
        const jsonData = {};
        jsonData.fullName = fullName;
        jsonData.identityNumber = identityNumber;
        jsonData.placeOfIssue = placeOfIssue;
        jsonData.dateOfIssue = date;

        let pathFrontFile = cmndFront.path
        jsonData.frontSideImage = await getImageLink(pathFrontFile, pathFrontFile.substr(pathFrontFile.lastIndexOf('/') + 1, pathFrontFile.length), cmndFront.mime)

        let pathBackFile = cmndBack.path
        jsonData.backSideImage = await getImageLink(pathBackFile, pathBackFile.substr(pathBackFile.lastIndexOf('/') + 1, pathBackFile.length), cmndBack.mime)

        jsonData.faceImage = await getImageLink(faceImg.uri, faceImg.fileName, faceImg.type)

        verify(jsonData).then((rs) => {
            Toast.show({
                type: 'success',
                text1: 'Đã gửi thông tin xác thực thành công!',
            });
            navigation.goBack();
        })
    };

    return (
        <ScrollView style={styles.container}>
            <CustomText style={styles.text} isRequired={true}>Họ và tên</CustomText>
            <CustomInput placeholder='Họ và tên' onChangeText={setFullName}/>

            <CustomText style={styles.text} isRequired={true}>Số cmnd</CustomText>
            <CustomInput placeholder='Số cmnd' onChangeText={setIdentityNumber} keyboardType='numeric'/>

            <CustomText style={styles.text} isRequired={true}>Địa điểm làm cmnd</CustomText>
            <CustomInput placeholder='Địa điểm làm cmnd' onChangeText={setPlaceOfIssue}/>

            <CustomText style={styles.text} isRequired={true}>Thời điểm làm cmnd</CustomText>
            <DatePickerDisableAble date={date} setDate={setDate}
                                   style={styles.datePick}
                                   mode='date'
            />

            <Divider style={styles.divider}/>

            <TouchableOpacity style={{...styles.cmndContainer, width: cmndWidth, height: cmndHeight}}
                              onPress={() => takeImage()}>
                {checkObjectValid(faceImg) ?
                    (<ImageBackground style={styles.cmndPlaceHolder}
                                      imageStyle={{borderRadius: 25}}
                                      source={{uri: faceImg.uri}}>
                        <TouchableOpacity onPress={takeImage}>
                            <Icon name='pencil-outline'
                                  style={{...IconStyle.small, ...styles.icon}}/>
                        </TouchableOpacity>
                    </ImageBackground>)
                    :
                    (<View style={styles.cmndPlaceHolder}>
                        <Icon name='face-recognition'
                              style={{fontSize: 70, ...styles.icon}}
                              color={Colors.orange}/>
                        <CustomText>Chụp ảnh mặt bạn</CustomText>
                    </View>)
                }
            </TouchableOpacity>

            <TouchableOpacity style={{...styles.cmndContainer, width: cmndWidth, height: cmndHeight}}
                              onPress={() => openImgAndCrop(setCmndFront, setIsFrontUpdated)}>
                {
                    isFrontUpdated ?
                        <ImageBackground style={styles.cmndPlaceHolder}
                                         imageStyle={{borderRadius: 25}}
                                         source={{uri: cmndFront.path}}>
                            <TouchableOpacity onPress={openGalleryCrop}>
                                <Icon name='pencil-outline'
                                      style={{...IconStyle.small, ...styles.icon}}/>
                            </TouchableOpacity>
                        </ImageBackground>
                        :
                        <View style={styles.cmndPlaceHolder}>
                            <Icon name='card-account-details-outline'
                                  style={{fontSize: 70, ...styles.icon}}
                                  color={Colors.orange}/>
                            <CustomText>Cập nhật ảnh mặt trước cmnd</CustomText>
                        </View>}
            </TouchableOpacity>
            <TouchableOpacity style={{...styles.cmndContainer, width: cmndWidth, height: cmndHeight}}
                              onPress={() => openImgAndCrop(setCmndBack, setIsBackUpdated)}>
                {
                    isBackUpdated ?
                        <ImageBackground style={styles.cmndPlaceHolder}
                                         imageStyle={{borderRadius: 25}}
                                         source={{uri: cmndBack.path}}>
                            <TouchableOpacity onPress={openGalleryCrop}>
                                <Icon name='pencil-outline'
                                      style={{...IconStyle.small, ...styles.icon}}/>
                            </TouchableOpacity>
                        </ImageBackground>
                        :
                        <View style={styles.cmndPlaceHolder}>
                            <Icon name='card-account-details'
                                  style={{fontSize: 70, ...styles.icon}}
                                  color={Colors.orange}/>
                            <CustomText>Cập nhật ảnh mặt sau cmnd</CustomText>
                        </View>}
            </TouchableOpacity>
            <CustomButton title={'Gửi thông tin'} backgroundColor={Colors.orange} style={styles.saveBtn}
                          onChooseOption={onSendInfo}/>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        // flexDirection: 'row'
        padding: Dimension.paddingMedium,
    },
    label: {
        fontSize: FontSize.xmedium,
    },
    cmndContainer: {
        borderWidth: 2,
        borderColor: Colors.orange,
        borderStyle: 'dashed',
        borderRadius: 3,
        margin: Dimension.marginSmall,
        backgroundColor: Colors.white,
        marginBottom: Dimension.marginMedium,
    },
    cmndPlaceHolder: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    saveBtn: {
        marginTop: Dimension.marginMedium,
        marginBottom: Dimension.marginLarge,
    },
    icon: {
        color: Colors.orange,
        backgroundColor: Colors.white,
        borderRadius: 50,
        padding: 2,
        // alignSelf: 'flex-end',
    },
    text: {
        marginBottom: 5,
        width: '90%',
        alignItems: 'flex-start',
    },
    datePick: {
        justifyContent: 'center',
    },
    divider: {
        marginVertical: Dimension.marginMedium,
    },

});

export default VerifyAccount;
