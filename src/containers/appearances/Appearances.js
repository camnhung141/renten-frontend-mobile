import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {getProductAppearancesService} from '../../redux/features/productSlice';
import CommentItem from '../../components/comment/commentItem';
import {Divider} from 'react-native-elements';
import AppearanceItem from './appearanceItem';
import {Colors, Dimension} from '../../common/styles/common';

const Appearances = (props) => {

    const [data, setData] = useState();
    useEffect(() => {
        getProductAppearancesService(props.route.params.id).then((appearances) => {
            setData(appearances);
        })
    },[])

    const renderItem = ({item}) => {
        return (<View style={{flexDirection: 'row'}}>
                <View style={{marginLeft: Dimension.marginSmall, borderLeftWidth: 3, borderColor: Colors.orange}}/>
                <AppearanceItem data={item}/>
        </View>
        );
    };

  return (
      <View style={styles.container}>
          <FlatList data={data}
                    // contentContainerStyle={{alignSelf: 'flex-start'}}
                    renderItem={renderItem}
                    keyExtractor={item => item._id}
          />
      </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})

export default Appearances;
