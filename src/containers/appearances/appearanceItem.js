import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import CustomText from '../../components/text/customText';
import {Divider, Rating} from 'react-native-elements';
import {Colors, Dimension, FontSize} from '../../common/styles/common';
import CommentContent from '../../components/comment/commentContent';
import ImageFlatlist from '../../components/image/imageFlatlist';
import {dateToStringFormat, dateToTimeFortmat} from '../../common/utils/utils';

const AppearanceItem = (props) => {

    const data = props.data;
    return (
        <View style={styles.container}>
                <View style={styles.content}>
                    <CustomText style={{fontWeight: 'bold', color: Colors.orange}}>Hình ảnh:</CustomText>
                    <View style={{marginRight: Dimension.marginMedium, paddingRight: Dimension.paddingSmall}}>
                        <ImageFlatlist data={data.images}/>
                    </View>
                    <CustomText style={{fontWeight: 'bold', color: Colors.orange}}>Mô tả:</CustomText>
                    <CustomText style={styles.name}>{data.note}</CustomText>
                    <CustomText style={{fontWeight: 'bold', color: Colors.orange}}>Trạng thái:</CustomText>
                    <CustomText>{data.appearance}</CustomText>
                    <CustomText style={{fontWeight: 'bold', color: Colors.orange}}>Thời điểm:</CustomText>
                    <CustomText style={styles.time}>{dateToStringFormat(data.updatedAt)} : {dateToTimeFortmat(data.updatedAt)}</CustomText>
                </View>
            <Divider/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        // marginRight: Dimension.marginMedium
        backgroundColor: Colors.white,
        width: '100%'
    },
    image: {
        width: 44,
        height: 44,
        borderRadius: 44 / 2,
        marginRight: Dimension.marginSmall,
    },
    headerComment: {
        flexDirection: 'row',
    },
    name: {
        // fontWeight: '500'
        // color: Colors.gray,
    },
    content: {
        // flexDirection: 'column',
        marginTop: Dimension.marginSmall,
        marginBottom: Dimension.marginSmall,
        // paddingRight: Dimension.paddingSmall
        marginHorizontal: Dimension.marginSmall,
        width: '100%'
    },
    rating: {
        alignSelf: 'flex-start',
        marginBottom: Dimension.marginMedium,
    },
    time: {
        color: Colors.gray,
    }
});

export default AppearanceItem;
