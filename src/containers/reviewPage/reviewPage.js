import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Image, ScrollView} from 'react-native';
import CustomText from '../../components/text/customText';
import {CameraSetting, Colors, Dimension, FontStyle, TypeImage} from '../../common/styles/common';
import {Divider, Rating} from 'react-native-elements';
import CustomInput from '../../components/customInput/customInput';
import CustomButton from '../../components/buttons/customButton';
import OutlineButton from '../../components/buttons/outlineButton';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImageCollector from '../../components/imageCollector/imageCollector';
import DocumentPicker from 'react-native-document-picker';
import api, {postFormDataImages} from '../../api/api';
import {reviewProduct} from '../../redux/features/productSlice';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {useNavigation} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import {checkObjectValid} from '../../common/utils/utils';

const ReviewPage = (props) => {
    const [dataImg, setDataImg] = useState([]);
    const [content, setContent] = useState('');
    const [rating, setRating] = useState(5)
    const dataItem = props.route.params.data
    const navigation = useNavigation();
    const userInfo = useSelector(state => state.customer.userInfo);

    const onSubmit = () => {
        const orderId = dataItem._id;
        const avatar = userInfo.avatar
        const name = userInfo.fullName
        if (dataImg.length !== 0) {
            const form = new FormData();

            dataImg.forEach((item) => {
                const newImg = {
                    uri: item.uri,
                    type: item.type,
                    name: item.fileName
                }
                form.append('images', newImg);
            })
            postFormDataImages(form, TypeImage.REVIEW).then((response) => {
                let images = [];
                if (response.data.payload)
                    images = response.data.payload;
                const data = {images: images, rating, content, orderId, name};
                if (checkObjectValid(avatar)) {
                    data.avatar = avatar;
                }
                reviewProduct(data, dataItem.product.productId).then(() => {
                    navigation.navigate(StackRoute.UserMainPage)
                })
            }).catch((err) => {
            })
        } else {
            const data = {rating, content, images: [], orderId , name};
            if (checkObjectValid(avatar)) {
                data.avatar = avatar;
            }
            reviewProduct(data, dataItem.product.productId).then(() => {
                navigation.navigate(StackRoute.UserMainPage)
            })
        }
    }

    const finishRating = (value) => {
        setRating(value)
    }

    return (
        <ScrollView style={styles.container}>
            <View style={styles.mainContainer}>
                <TouchableOpacity style={styles.imageContainer}>
                    <Image style={styles.image}
                           source={dataItem.product.thumbnail ? {uri: dataItem.product.thumbnail} : require('../../../assets/images/no-img.png')}/>
                </TouchableOpacity>
                <View style={styles.infoContainer}>
                    <CustomText numberOfLines={1} style={styles.title}>{dataItem.product.name}</CustomText>
                    <CustomText numberOfLines={1} style={styles.title}>{dataItem.product.productCode}</CustomText>
                    <CustomText numberOfLines={1}>{dataItem.product.appearance}</CustomText>
                </View>
            </View>
            <Divider style={styles.divider}/>
            <View>
                <Rating
                    ratingBackgroundColor={Colors.transparent}
                    imageSize={35}
                    ratingCount={5}
                    startingValue={5}
                    type='custom'
                    onFinishRating={finishRating}
                />
            </View>
            <Divider style={styles.divider}/>
            <ImageCollector data={dataImg} setData={setDataImg}/>
            <Divider style={styles.divider}/>
            <View>
                <CustomInput multiline={true}
                             style={styles.reviewComment}
                             textAlignVertical='top'
                             placeholder='Nhập bình luận của bạn'
                             onChangeText={setContent}
                />
            </View>
            <Divider style={styles.divider}/>
            <CustomButton title='Đánh giá' backgroundColor={Colors.orange} onChooseOption={onSubmit}/>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    imageContainer: {
        width: Dimension.widthNormal,
        height: Dimension.heightNormal,
    },
    container: {
        margin: Dimension.marginSmall,
        backgroundColor: Colors.white,
    },
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: Dimension.marginSmall,
    },
    infoContainer: {
        flex: 1,
        margin: Dimension.marginSmall,
    },
    image: {
        width: '100%',
        height: '100%',
    },
    divider: {
        borderWidth: 0.2,
        marginVertical: Dimension.marginSmall,
    },
    reviewComment: {
        height: 200,
        width: '100%',
        backgroundColor: Colors.lighterGray,
    },
    imageBtnArea: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    btnImage: {
        marginVertical: Dimension.marginSmall,
    },
    title: {
        fontWeight: FontStyle.bold
    }
});

export default ReviewPage;
