import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView, useWindowDimensions} from 'react-native';
import ShopItem from '../../components/shop/shopItem';
import ListProduct from '../../components/product/ListProduct/list-product';
import {Colors, Container, Dimension} from '../../common/styles/common';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import ListProductVertical from '../../components/product/ListProduct/list-product-vertical';
import {searchProducts} from '../../redux/features/productSlice';
import {SearchBar} from 'react-native-elements';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import {useNavigation} from '@react-navigation/core';
import CommentList from '../../components/comment/commentList';

const Shop = (props) => {

    const navigation = useNavigation();
    const [searchText, setSearchText] = useState('');

    const agencyInfo = props.route.params.agencyInfo;

    const doSearch = () => {
        navigation.navigate(StackRoute.SearchDetail, {
            criteria: {
                keyword: searchText,
                categoriesId: '',
                page: 1,
                sort: 'createdAt:-1',
                agencyId: agencyInfo.accountId._id
            },
        });
    };

    const criteriaTopRent = {
        keyword: '',
        categoriesId: '',
        page: 1,
        agencyId: agencyInfo.accountId._id,
        sort: 'rentCount:-1',
    };
    const criteriaTopReview = {
        keyword: '',
        categoriesId: '',
        page: 1,
        agencyId: agencyInfo.accountId._id,
        sort: 'rate.rateAverage:-1',
    };
    const criteriaTopNew = {
        keyword: '',
        categoriesId: '',
        page: 1,
        agencyId: agencyInfo.accountId._id,
        sort: 'updatedAt:-1',
    };

    return (
        <ScrollView style={styles.container}>
            <View style={styles.searchContainer}>
                <SearchBar
                    round={5}
                    placeholder={`Tìm kiếm trong ${agencyInfo.agencyName}`}
                    onChangeText={setSearchText}
                    inputContainerStyle={{backgroundColor: Colors.white, height: 35, alignContent: 'center'}}
                    value={searchText}
                    containerStyle={{borderRadius: 5, borderBottomColor: 'transparent', borderTopColor: 'transparent', backgroundColor: 'transparent', padding: 0}}
                    onSubmitEditing={doSearch}
                />
            </View>
            <View  style={Container.blockContainer}>
                <ShopItem info={agencyInfo}/>
            </View>

            <View style={Container.blockContainer}>
                <ListProduct title='Sản phẩm được thuê nhiều nhất' horizontal={true}
                             criteria={criteriaTopRent}
                />
            </View>

            <View style={Container.blockContainer}>
                <ListProduct title='Sản phẩm được đánh giá cao' horizontal={true}
                             criteria={criteriaTopReview}
                />
            </View>

            <View style={Container.blockContainer}>
                <ListProduct title='Sản phẩm mới nhất' horizontal={true}
                             criteria={criteriaTopNew}
                />
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    searchContainer: {
        backgroundColor: Colors.orange,
        padding: Dimension.paddingSmall,
    }
});

export default Shop;
