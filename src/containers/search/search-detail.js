import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import ListProductVertical from '../../components/product/ListProduct/list-product-vertical';
import {searchProducts} from '../../redux/features/productSlice';
import Categories from '../../components/category/categories';
import {StackRoute} from '../../navigations/stackroute/stackRoute';
import LoaderProductComponent from '../../components/contentLoader/contentLoader';
import {Colors, Dimension, IconStyle} from '../../common/styles/common';
// import SearchBar from 'react-native-search-bar';
import {Divider, SearchBar} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SearchDetail = (props) => {
    const [searchText, setSearchText] = useState('');
    const [category, setCategory] = useState({});
    const [placeHolderSearchText, setPlaceHolderSearchText] = useState('Tìm sản phẩm, danh mục hay thương hiệu,...');

    const doSearch = () => {
        props.navigation.push(StackRoute.SearchDetail, {
            criteria: {
                ...props.route.params.criteria,
                keyword: searchText,
            },
        });
    };
    useEffect(() => {
        const category = props.route.params.categoryItem;
        if (category) {
            setCategory(category);
            setPlaceHolderSearchText('Tìm kiếm trong ' + category.name);
        } else {
            setCategory(null);
        }
    }, []);


    const openCartScreen = () => {
        props.navigation.navigate(StackRoute.Cart);
    };

    return (
        <ListProductVertical navigation={props.navigation} criteria={props.route.params.criteria}
                             header={<View style={styles.container}>
                                 <View style={styles.searchContainer}>
                                     <View style={{flex: 1}}>
                                         <SearchBar
                                             round={5}
                                             placeholder={placeHolderSearchText}
                                             onChangeText={setSearchText}
                                             value={searchText}
                                             inputContainerStyle={{backgroundColor: 'white', height: 40,}}
                                             containerStyle={{borderRadius: 5, borderBottomColor: 'transparent', borderTopColor: 'transparent', backgroundColor: 'transparent', padding: 0}}
                                             onSubmitEditing={doSearch}
                                         />
                                     </View>

                                     <TouchableOpacity style={styles.cartIconContainer} onPress={openCartScreen}>
                                         <Icon name='cart-outline'
                                               style={{...IconStyle.small}}
                                               color={Colors.white}/>
                                     </TouchableOpacity>
                                 </View>
                                 <Divider style={styles.divider}/>
                                 {
                                     category && category.hasOwnProperty('subCategories') && category.subCategories.length > 0 ?
                                         <Categories data={category.subCategories}
                                                     navigation={props.navigation}/> : null
                                 }
                             </View>}/>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    searchBar: {
        height: Dimension.heightSmall,
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: Colors.orange
    },
    searchContainer: {
        backgroundColor: Colors.orange,
        paddingHorizontal: Dimension.paddingMedium,
        paddingTop: Dimension.paddingSmall,
        paddingBottom: Dimension.paddingMedium,
        alignItems: 'center',
        flexDirection: 'row'
    },
    divider: {
        borderColor: Colors.white,
        borderWidth: 0.5
    },
});

export default SearchDetail;
