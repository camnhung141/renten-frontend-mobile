import {getAccessToken, getRefreshToken} from '../../storage/async-storage';

export function nFormatter(num, digits) {
    let si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "k" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "G" },
        { value: 1E12, symbol: "T" },
        { value: 1E15, symbol: "P" },
        { value: 1E18, symbol: "E" }
    ];
    let rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    let i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
            break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

export const getHeaderAccessTokenConfig = async () => {
    const accessToken = await getAccessToken()
    const authenToken = 'Bearer ' + accessToken;
    return {headers: {Authorization: authenToken}}
}

export const getLocalRefreshToken = async () => {
    return await getRefreshToken();
}

export const dateToStringFormat = (date) => {
    let dateData = date;
    if (!dateData)
        return ''
    if (typeof dateData === 'string')
    {
        dateData = new Date(Date.parse(date))
    }
    let month = dateData.getUTCMonth() + 1; //months from 1-12
    let day = dateData.getUTCDate();
    let year = dateData.getUTCFullYear();
    return day + '/' + month + '/' + year
}

export const dateToTimeFortmat = (date) => {
    let dateData = date;
    if (!dateData)
        return ''
    if (typeof dateData === 'string')
    {
        dateData = new Date(Date.parse(date))
    }
    let hour = dateData.getUTCHours();
    const formattedHour = ("0" + hour).slice(-2)

    let minute = dateData.getUTCMinutes();
    const formattedMinute = ("0" + minute).slice(-2)
    return formattedHour + ':' + formattedMinute

}

export const checkObjectEmpty = (obj) => {
    return (JSON.stringify(obj) === '{}' || JSON.stringify(obj) === '[]');
}

export const wait = (timeout) => {
    return new Promise((resolve) => {
        setTimeout(resolve, timeout);
    });
};

export const checkValidNotEmptyInput = (obj) => {
    return !(checkObjectEmpty(obj) || obj === '' || obj === null || obj === undefined);
}

export const checkEmail = (obj) => {
    return !(checkObjectEmpty(obj) || obj === '' || obj === null || obj === undefined || !validateEmail(obj));
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export const checkPassword = (obj) => {
    return !(checkObjectEmpty(obj) || obj === '' || obj === null || obj === undefined || !validatePassword(obj));
}

function validatePassword(password) {
    const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    return re.test(String(password));
}

export const checkPhone = (obj) => {
    return !(checkObjectEmpty(obj) || obj === '' || obj === null || obj === undefined || !validatePhoneVn(obj));
}

function validatePhoneVn(phone) {
    const re = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;
    return re.test(String(phone));
}

export const checkObjectValid = (obj) => {
    return !(checkObjectEmpty(obj) || obj === '' || obj === null || obj === undefined)
}

export const toConcurrency = value => {
    return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}

export const getParams = ( name, url ) => {
    if (!url) url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
}
