// Colors define
export const Colors = {
    transparent: 'rgba(52,52,52,0.0)',
    backgroundColorGray: '#d0d0d0',
    secondaryColor: '#0F4764',
    red: '#e92f2f',
    black: '#000',
    white: '#ffffff',
    gray: '#808080',
    disabledBtn: '#c4c4c4',
    greyWhite: '#bdbdbd',
    yellow: '#ffd78b',
    blue: '#0084bc',
    dblue: '#58b3ef',
    lighterGray: '#f3f3f3',
    lightGray: '#e7e7e7',
    darkGray: '#404040',
    mblue: 'rgba(35,121,255,0.98)',
    green: '#00cc1f',
    lighterGreen: '#aeffb9',
    orange: '#ed5a36',
    lighterOrange: '#ff9275',
    darkerOrange: '#a73e25',
    background: '#ffffff',
    textColor: '#000a12',
    subTextColor: '#0F4764',
    itemColor: '#e0e0e0',
    detailBlockColor: '#f5f5f5',
    textHeader: 'blue',
    headerColor: '#ffffff',
    iconColor: '#000',
    block: '#ffffff',
    reverseColor: '#606060',
};

// Input define
export const InputStyle = {
    input: {
        borderRadius: 7,
        // width: '90%',
        height: 44,
        padding: 10,
        marginBottom: 15,
        borderWidth: 1,
        borderColor: '#0084be',
    },
};

//  Buttons define
export const ButtonStyle = {
    buttonRadius: {
        alignItems: 'center',
        borderRadius: 7,
        width: '90%',
        height: 44,
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#0084be',
    },
};

// Text define
export const FontStyle = {
    bold: 'bold',
};

// String define
export const StringStyle = {};

// FontSize define
export const FontSize = {
    small: 12,
    xsmall: 13,
    xxsmall: 14,
    medium: 15,
    xmedium: 18,
    large: 20,
    xlarge: 24,
    xxlarge: 28,
    xxxxxlarge: 40,
};

// Dimension define
export const Dimension = {
    marginSmall: 5,
    marginXSmall: 10,
    marginMedium: 15,
    marginLarge: 50,

    paddingSmall: 5,
    paddingMedium: 10,
    paddingXMedium: 15,

    borderSmall: 0.5,

    widthNormal: 125,
    widthSmall: 35,
    widthXSmall: 70,
    heightXSmall: 70,

    heightNormal: 125,
    heightSmall: 35,
    heightProductItemCard: 300,

    navigationBarHeight: 45,

    widthItemCategory: 170,
    heightItemCategory: 90,

    percentWidthItemCmnd: 85,
    percentHeightItemCmnd: 53,
};

export const IconStyle = {
    normal: {
        fontSize: FontSize.xxxxxlarge,
        alignSelf: 'center',
        marginHorizontal: Dimension.marginSmall,
    },
    xsmall: {
        fontSize: FontSize.xxlarge,
        alignSelf: 'center',
        marginHorizontal: Dimension.marginSmall,
    },
    small: {
        fontSize: FontSize.xlarge,
        alignSelf: 'center',
        marginHorizontal: Dimension.marginSmall,
    },
    tiny: {
        fontSize: FontSize.medium,
        alignSelf: 'center',
        marginHorizontal: Dimension.marginSmall,
    },
    momo: {
        height: 30,
        width: 30,
    },
};

export const ImageStyle = {
    avatar: {
        width: 44,
        height: 44,
        borderRadius: 25,
        marginRight: Dimension.marginMedium,
    },
};

export const CameraSetting = {
    image: {
        mediaType: 'photo',
        saveToPhotos: true,
    },
};

export const Input = {
    invalidInput: {
        borderColor: Colors.red,
    },
    validInput: {
        borderColor: Colors.blue,
    },
};

export const DividerStyle = {
    vertical: {
        borderLeftWidth: 1,
        borderLeftColor: Colors.orange,
        marginHorizontal: Dimension.marginMedium,
    },
    horizontal: {
        borderWidth: 0.15,
        borderColor: Colors.gray,
    },
};

export const Container = {
    blockContainer: {
        marginVertical: Dimension.marginSmall,
    },
};

export const StatusOrder = {
    pending: 'waitingToConfirm',
    processing: 'processing',
    transporting: 'transporting',
    waitingToReceive: 'waitingToReceive',
    rented: 'rented',
    returned: 'returned',
    cancel: 'cancel',
    late: 'late',
    notReturned: 'notReturned'
};

export const StatusOrderMessage = {
    waitingToConfirm: 'Đợi xác nhận',
    processing: 'Đã xác nhận',
    transporting: 'Đang vận chuyển',
    waitingToReceive: 'Đợi nhận',
    rented: 'Đã thuê',
    returned: 'Đã hoàn trả',
    cancel: 'Đã hủy',
    late: 'Trả hàng trễ',
    notReturned: 'Không trả hàng'
};

export const VerifyStatus = {
    unverified: 'unverified', verifying: 'verifying', verified: 'verified', locked: 'locked', blocked: 'blocked',
};

export const VerifyStatusColor = {
    unverified: Colors.gray, verifying: Colors.yellow, verified: Colors.green, locked: Colors.red, blocked: Colors.gray,
};

export const VerifyStatusMessage = {
    unverified: 'Chưa đươc xác thực', verifying: 'Đang đợi xác nhận', verified: 'Đã xác nhận tài khoản', locked: 'Đã khóa', blocked: 'Đã bị chặn',
};

export const StatusOrderColor = {
    waitingToConfirm: '#B78103',
    processing: '#229A16',
    transporting: '#B78103',
    waitingToReceive: '#229A16',
    rented: '#229A16',
    returned: '#229A16',
    cancel: '#B72136',
};

export const PaymentStatus = {
    success: 'Success',
    failed: 'Failed'
}

export const TypeImage = {
    ICON: 1,
    BANNER: 2,
    IDENTITY_CARD: 3,
    AVATAR: 4,
    REVIEW: 5,
    PRODUCT: 6
}
