import AsyncStorage from '@react-native-community/async-storage';

const dataType = {
    ACCOUNT_DATA: 'authData',
    ACCESS_TOKEN: 'accessToken',
    REFRESH_TOKEN: 'refreshToken',
}

export const storeAuthInfo = (value) => {
    try {
        // const jsonValue = JSON.stringify(value);
        // await AsyncStorage.setItem(dataType.AUTH_DATA, jsonValue);
        const jsonRefresh = JSON.stringify(value.refreshToken)
        const jsonAccess = JSON.stringify(value.accessToken)
        const jsonAccountInf = JSON.stringify(value.account)

        AsyncStorage.setItem(dataType.ACCOUNT_DATA, jsonAccountInf);
        AsyncStorage.setItem(dataType.ACCESS_TOKEN, jsonAccess);
        AsyncStorage.setItem(dataType.REFRESH_TOKEN, jsonRefresh);
    } catch (e) {
    }
};

export const storeAccessToken = async (value) => {
    try {
        const jsonAccess = JSON.stringify(value)
        await AsyncStorage.setItem(dataType.ACCESS_TOKEN, jsonAccess);
    } catch (e) {
    }
};

export const storeRefreshToken = async (value) => {
    try {
        const jsonRefresh = JSON.stringify(value)
        await AsyncStorage.setItem(dataType.REFRESH_TOKEN, jsonRefresh);
    } catch (e) {
    }
};

export const storeAccountData = async (value) => {
    try {
        const jsonAccountData = JSON.stringify(value)
        await AsyncStorage.setItem(dataType.ACCOUNT_DATA, jsonAccountData);
    } catch (e) {
        console.log(e);
    }
};


export const getAccessToken = async () => {
    try {
        const value = await AsyncStorage.getItem(dataType.ACCESS_TOKEN);
        return JSON.parse(value);
    } catch (e) {
        return null;
    }
};

export const getAccountData = async () => {
    try {
        const value = await AsyncStorage.getItem(dataType.ACCOUNT_DATA);
        return JSON.parse(value);
    } catch (e) {
        return null;
    }
};

export const getRefreshToken = async () => {
    try {
        const value = await AsyncStorage.getItem(dataType.REFRESH_TOKEN);
        return JSON.parse(value);
    } catch (e) {
        return null;
    }
};

export const getAuthInfo = async () => {
    try {
        const value = await AsyncStorage.getItem(dataType.ACCOUNT_DATA);
        const jsonValue = value !== null ? JSON.parse(value) : null;
        return jsonValue;
    } catch (e) {
        return null;
    }
};

export const removeAuthInfo = async () => {
    try {
        await AsyncStorage.removeItem(dataType.ACCOUNT_DATA);
        await AsyncStorage.removeItem(dataType.REFRESH_TOKEN);
        await AsyncStorage.removeItem(dataType.ACCESS_TOKEN);
        return true;
    } catch (exception) {
        return false;
    }
};




