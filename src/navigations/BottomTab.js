import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {StackRoute} from './stackroute/stackRoute';
import createBottomTabNavigator from '@react-navigation/bottom-tabs/src/navigators/createBottomTabNavigator';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Home from '../containers/homePage/home';
import HomeNavigationStack from './Home';
import UserNavigationStack from './User';
import {Colors} from '../common/styles/common';
import NotificationNavigationStack from './Notification';
import UserMainPage from '../containers/user/userMainPage';
import NonLoginScreen from '../containers/authentication/nonLoginScreen';
import {useSelector} from 'react-redux';

const Stack = createBottomTabNavigator();

const BottomTabNavigationStack = () => {
    const isLogged = useSelector(state => state.auth.isLogged);

    return (
        <Stack.Navigator
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;
                    if (route.name === StackRoute.HomeNav) {
                        iconName = 'home';
                    } else if (route.name === StackRoute.UserNav) {
                        iconName = 'account';
                    } else if (route.name === StackRoute.NotificationNav) {
                        iconName = 'bell';
                    }
                    return <Icon name={iconName} size={size} color={color}/>;
                },
            })}

            tabBarOptions={{
                activeBackgroundColor: Colors.white,
                activeTintColor: Colors.orange
            }}
        >
            <Stack.Screen
                name={StackRoute.HomeNav}
                component={HomeNavigationStack}
                options={
                    {}
                }
            />
            {/*<Stack.Screen*/}
            {/*    name={StackRoute.NotificationNav}*/}
            {/*    component={NotificationNavigationStack}*/}
            {/*    options={*/}
            {/*        {}*/}
            {/*    }*/}
            {/*/>*/}
            <Stack.Screen
                name={StackRoute.UserNav}
                component={isLogged ? UserNavigationStack : NonLoginScreen}
                options={
                    {}
                }
            />

        </Stack.Navigator>
    );
};

export default BottomTabNavigationStack;
