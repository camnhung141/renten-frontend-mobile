import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {StackRoute} from './stackroute/stackRoute';
import Notification from '../containers/notification/notification'

const Stack = createStackNavigator();

const NotificationNavigationStack = () => {
  return (
      <Stack.Navigator initialRouteName={StackRoute.NotificationNav}>
          <Stack.Screen
            name={StackRoute.Notification}
            component={Notification}
            options={
                {
                    headerShown: false
                }
            }
          />

      </Stack.Navigator>
  );
}

export default NotificationNavigationStack;
