import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {StackRoute} from './stackroute/stackRoute';
import MainNavigationStack from './Main';
import {useDispatch} from 'react-redux';
import {getRefreshToken} from '../storage/async-storage';
import {setStatusLoginService} from '../redux/features/authSlice';
import {getUserInfo} from '../redux/features/customerSlice';

const Stack = createStackNavigator();

const RootNavigationStack = () => {

    return (
        <Stack.Navigator initialRouteName={StackRoute.RootNav}>
            {/*<Stack.Screen*/}
            {/*  name={StackRoute.AuthNav}*/}
            {/*  component={AuthenticationNavigationStack}*/}
            {/*  options={*/}
            {/*      {*/}
            {/*          headerShown: false,*/}
            {/*      }*/}
            {/*  }*/}
            {/*/>*/}
            <Stack.Screen
                name={StackRoute.MainNav}
                component={MainNavigationStack}
                options={
                    {
                        headerShown: false,
                    }
                }
            />

        </Stack.Navigator>
    );
};

export default RootNavigationStack;
