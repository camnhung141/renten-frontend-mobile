import Login from '../containers/authentication/login';
import SignUp from '../containers/authentication/signup';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import {StackRoute} from './stackroute/stackRoute';
import {Colors} from '../common/styles/common';

const Stack = createStackNavigator();

const AuthenticationNavigationStack = () => {
  return (
      <Stack.Navigator initialRouteName={StackRoute.Login}>
          <Stack.Screen
            name={StackRoute.Login}
            component={Login}
            options={
                {
                    headerShown: false
                }
            }
          />
          <Stack.Screen
              name={StackRoute.Signup}
              component={SignUp}
              options={
                  {
                      headerStyle: {
                          backgroundColor: Colors.orange,
                          height: 40
                      },
                      headerTintColor: Colors.white,
                  }
              }
          />
      </Stack.Navigator>
  );
}

export default AuthenticationNavigationStack;
