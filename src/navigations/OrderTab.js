import React, {useState} from 'react';
import {StackRoute} from './stackroute/stackRoute';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import OrderPending from '../containers/order/orderPending';
import OrderReceived from '../containers/order/orderReceived';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, Dimension, FontStyle} from '../common/styles/common';
import OrderTransporting from '../containers/order/orderTransporting';
import OrderReturned from '../containers/order/orderReturned';
import OrderCanceled from '../containers/order/orderCanceled';

const Stack = createMaterialTopTabNavigator();

const OrderTabNavigationStack = () => {

  return (
      <Stack.Navigator initialRouteName={StackRoute.OrderTabTopNav}
                       tabBarOptions={{
                           activeTintColor: Colors.orange,
                           scrollEnabled: true,
                           labelStyle: {
                                fontWeight: 'normal'
                           },
                       }}
      >
          <Stack.Screen
            name={StackRoute.OrderPending}
            component={OrderPending}
          />
          <Stack.Screen
              name={StackRoute.OrderTransporting}
              component={OrderTransporting}
          />
          <Stack.Screen
            name={StackRoute.OrderReceived}
            component={OrderReceived}
          />
          <Stack.Screen
              name={StackRoute.OrderReturned}
              component={OrderReturned}
          />
          <Stack.Screen
              name={StackRoute.OrderCanceled}
              component={OrderCanceled}
          />
      </Stack.Navigator>
  );
}

export default OrderTabNavigationStack;
