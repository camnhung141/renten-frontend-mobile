import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {StackRoute} from './stackroute/stackRoute';
import Home from '../containers/homePage/home';

const Stack = createStackNavigator();

const HomeNavigationStack = () => {
  return (
      <Stack.Navigator initialRouteName={StackRoute.Home}>
          <Stack.Screen
            name={StackRoute.Home}
            component={Home}
            options={
                {
                    headerShown: false
                }
            }
          />
      </Stack.Navigator>
  );
}

export default HomeNavigationStack;
