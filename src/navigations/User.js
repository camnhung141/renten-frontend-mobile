import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {StackRoute} from './stackroute/stackRoute';
import UserMainPage from '../containers/user/userMainPage';
import UserProfile from '../containers/user/userProfile';
import EditInfo from '../containers/editInfo/editInfo';
import Location from '../containers/location/location';
import AddAddress from '../containers/location/addAddress';
import OrderTabNavigationStack from './OrderTab';
import ReviewPage from '../containers/reviewPage/reviewPage';
import {useSelector} from 'react-redux';
import NonLoginScreen from '../containers/authentication/nonLoginScreen';
import {Colors, Dimension} from '../common/styles/common';
import ChangePassword from '../containers/editInfo/changePassword';
import VerifyAccount from '../containers/verifyAccount/verifyAccount';
import OrderItem from '../containers/order/orderItem/orderItem';
import DetailOrder from '../containers/order/detailOrder/detailOrder';


const Stack = createStackNavigator();

const UserNavigationStack = () => {
    const isLogged = useSelector(state => state.auth.isLogged);

    return (
        <Stack.Navigator initialRouteName={StackRoute.UserMainPage}>
            <Stack.Screen
                name={StackRoute.UserMainPage}
                component={isLogged ? UserMainPage : NonLoginScreen}
                options={
                    {
                        headerShown: false,
                    }
                }
            />
            <Stack.Screen name={StackRoute.UserProfile}
                          component={isLogged ? UserProfile : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
            <Stack.Screen name={StackRoute.EditInfo}
                          component={isLogged ? EditInfo : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
            <Stack.Screen name={StackRoute.ChangePassword}
                          component={isLogged ? ChangePassword : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
            <Stack.Screen name={StackRoute.Verify}
                          component={isLogged ? VerifyAccount : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
            <Stack.Screen name={StackRoute.OrderTabTopNav}
                          component={isLogged ? OrderTabNavigationStack : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
            <Stack.Screen name={StackRoute.DetailOrder}
                          component={isLogged ? DetailOrder : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
            <Stack.Screen name={StackRoute.Review}
                          component={isLogged ? ReviewPage : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
        </Stack.Navigator>
    );
};

export default UserNavigationStack;
