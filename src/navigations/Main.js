import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {StackRoute} from './stackroute/stackRoute';
import AuthenticationNavigationStack from './Authentication';
import ProductCart from '../containers/cart/productCart';
import HomeNavigationStack from './Home';
import DetailProduct from '../containers/product/detail-product';
import SearchDetail from '../containers/search/search-detail';
import {Colors, Dimension} from '../common/styles/common';
import BottomTabNavigationStack from './BottomTab';
import Login from '../containers/authentication/login';
import SignUp from '../containers/authentication/signup';
import {useSelector} from 'react-redux';
import NonLoginScreen from '../containers/authentication/nonLoginScreen';
import Payment from '../containers/payment/payment';
import Shop from '../containers/shop/shop';
import CategoriesAll from '../containers/category/categoriesAll';
import Location from '../containers/location/location';
import AddAddress from '../containers/location/addAddress';
import PaymentMethodPicker from '../containers/payment/paymentMethodPicker';
import PaymentResult from '../payment/paymentResult';
import PointInfoHelp from '../containers/pointDetail/pointInfoHelp';
import Appearances from '../containers/appearances/Appearances';

const Stack = createStackNavigator();

const MainNavigationStack = () => {
    const isLogged = useSelector(state => state.auth.isLogged);

    return (
        <Stack.Navigator initialRouteName={StackRoute.BottomNav}>
            <Stack.Screen
                name={StackRoute.BottomNav}
                component={BottomTabNavigationStack}
                options={
                    {
                        headerShown: false,
                    }
                }
            />
            <Stack.Screen
                name={StackRoute.Cart}
                component={isLogged ? ProductCart : NonLoginScreen}
                options={
                    {
                        headerStyle: {
                            backgroundColor: Colors.orange,
                            height: Dimension.navigationBarHeight,
                        },
                        headerTintColor: Colors.white,
                    }
                }
            />
            <Stack.Screen
                name={StackRoute.Payment}
                component={isLogged ? Payment : NonLoginScreen}
            />
            <Stack.Screen
                name={StackRoute.Nonlogin}
                component={NonLoginScreen}
            />
            <Stack.Screen
                name={StackRoute.PointInfoHelp}
                component={isLogged ? PointInfoHelp : NonLoginScreen}
            />
            <Stack.Screen
                name={StackRoute.ChangeMethodPayment}
                component={isLogged ? PaymentMethodPicker : NonLoginScreen}
            />
            <Stack.Screen
                name={StackRoute.PaymentResult}
                component={isLogged ? PaymentResult : NonLoginScreen}
            />
            <Stack.Screen name={StackRoute.Location}
                          component={isLogged ? Location : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
            <Stack.Screen name={StackRoute.AddLocation}
                          component={isLogged ? AddAddress : NonLoginScreen}
                          options={
                              {
                                  headerStyle: {
                                      backgroundColor: Colors.orange,
                                      height: Dimension.navigationBarHeight
                                  },
                                  headerTintColor: Colors.white,
                              }
                          }
            />
            <Stack.Screen
                name={StackRoute.DetailProduct}
                component={DetailProduct}
                options={
                    ({route}) => ({
                        title: route.params.name,
                        headerStyle: {
                            backgroundColor: Colors.orange,
                            height: Dimension.navigationBarHeight,
                        },
                        headerTintColor: Colors.white,
                    })
                }
            />
            <Stack.Screen
                name={StackRoute.Appearances}
                component={Appearances}
            />
            <Stack.Screen
                name={StackRoute.SearchDetail}
                component={SearchDetail}
                options={
                    ({route}) => ({
                        title: route.params.name ? route.params.name : 'Danh sách sản phẩm',
                        headerStyle: {
                            backgroundColor: Colors.orange,
                            height: Dimension.navigationBarHeight,
                        },
                        headerTintColor: Colors.white,
                    })
                }
            />
            <Stack.Screen
                name={StackRoute.Shop}
                component={isLogged ? Shop : NonLoginScreen}
                options={
                    ({route}) => ({
                        title: route.params.name ? route.params.name : 'Cửa hàng',
                        headerStyle: {
                            backgroundColor: Colors.orange,
                            height: Dimension.navigationBarHeight,
                        },
                        headerTintColor: Colors.white,
                    })
                }
            />
            <Stack.Screen
                name={StackRoute.Categories}
                component={CategoriesAll}
                options={
                    {
                        headerStyle: {
                            backgroundColor: Colors.orange,
                            height: Dimension.navigationBarHeight,
                        },
                        headerTintColor: Colors.white,
                    }
                }
            />
            {
                !isLogged ?
                    (<>
                        <Stack.Screen
                            name={StackRoute.Login}
                            component={Login}
                            options={
                                {
                                    headerShown: false,
                                }
                            }
                        />
                        <Stack.Screen
                            name={StackRoute.Signup}
                            component={SignUp}
                            options={
                                {
                                    headerShown: false,
                                }
                            }
                        />
                    </>) : null}
        </Stack.Navigator>
    );
};

export default MainNavigationStack;
